import PubNubReact from "pubnub-react";
var PushNotification = require('react-native-push-notification');
import AsyncStorage from '@react-native-community/async-storage';

export default class ChatEngineProvider {
    
    static _username = null;
    static _uuid = null;
    static _name = null;
    static _connected = false;
    static _chatEngine = ChatEngineProvider._createChatEngine();
    static _pubnub = null;
    
    static _createChatEngine() {
        
    }
    
    static get() {
        return ChatEngineProvider._chatEngine;
    }
    
    static async connect(username, name) {
        return new Promise((resolve, reject) => {
            console.log("called ,,,,,,,,,,,,,,,,,")
            ChatEngineProvider._username = username;
            ChatEngineProvider._name = name || username;
            ChatEngineProvider._uuid = username;
            
            ChatEngineProvider._pubnub = new PubNubReact({
                //rajeev.ranjan@headerlabs.com account
                // publishKey: "pub-c-a600c37b-07b0-4ffd-b804-33eeb8e053ca",
                // subscribeKey: "sub-c-3fc27190-d84c-11e9-8d6d-8621f881bfdb",
                //aepiphany1@gmail.com account 
                publishKey: "pub-c-fd213554-7b85-472c-9014-90a1e61429f6",
                subscribeKey: "sub-c-748881b6-f652-11e9-a301-7a3b1591b90a",
                uuid: ChatEngineProvider._uuid,
                presenceTimeout: 200,
                // logVerbosity: true
            });
            // ChatEngineProvider._pubnub.getPresence('group_6666666666_1569925604547', presence => {

            //     console.log("presendce action ......")
            //     console.log(presence)
            //   })

            ChatEngineProvider._pubnub.addListener({
                status: (s) => {
                    console.log("category", s);
                    if(s.category === "PNConnectedCategory") {
                        ChatEngineProvider._connected = true;
                        PushNotification.configure({
                            // Called when Token is generated.
                            onRegister: function(token) {
                                console.log( 'TOKEN:', token );
                                AsyncStorage.setItem('token', token.token);
                                // PushNotification.getApplicationIconBadgeNumber((badgeNumber)=>{
                                //     var newBadgeNumber = badgeNumber + 1
                                //     PushNotification.setApplicationIconBadgeNumber(newBadgeNumber);
                                // })
                                PushNotification.setApplicationIconBadgeNumber(0);
                                // const ShortcutBadger = NativeModules.ShortcutBadger;
                               
                                // ShortcutBadger.applyCount(0);
                                // ShortcutBadger.removeCount();

                                
                                // if (token.os == "ios") {
                                //   this.pubnub.push.addChannels(
                                //   {
                                //     channels: ['notifications'],
                                //     device: token.token,
                                //     pushGateway: 'apns'
                                //   });
                                //   // Send iOS Notification from debug console: {"pn_apns":{"aps":{"alert":"Hello World."}}}
                                // } else if (token.os == "android"){
                                //   this.pubnub.push.addChannels(
                                //   {
                                //     channels: ['notifications'],
                                //     device: token.token,
                                //     pushGateway: 'gcm' // apns, gcm, mpns
                                //   });
                                //   // Send Android Notification from debug console: {"pn_gcm":{"data":{"message":"Hello World."}}}
                                // }  
                            }.bind(this),
                            // Something not working?
                            // See: https://support.pubnub.com/support/solutions/articles/14000043605-how-can-i-troubleshoot-my-push-notification-issues-
                            // Called when a remote or local notification is opened or received.
                            onNotification: function(notification) {
                              console.log( 'NOTIFICATION:', notification );

                            //   PushNotification.getApplicationIconBadgeNumber((badgeNumber)=>{
                            //     var newBadgeNumber = badgeNumber + 1
                            //     PushNotification.setApplicationIconBadgeNumber(newBadgeNumber);
                            // })
                              // Do something with the notification.
                              // Required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
                              // notification.finish(PushNotificationIOS.FetchResult.NoData);
                            },
                            // ANDROID: GCM or FCM Sender ID
                            senderID: "418151762322",

                            permissions: {
                                alert: true,
                                badge: true,
                                sound: true
                              },
                            popInitialNotification: true,
 
                            /**
                             * (optional) default: true
                             * - Specified if permissions (ios) and token (android and ios) will requested or not,
                             * - if not, you must call PushNotificationsHandler.requestPermissions() later
                             */
                            requestPermissions: true
                            
                        });
                        resolve();
                    }
                }
            });
            this.state = {};
            ChatEngineProvider._pubnub.init(this);
        });
    }
    
    static subscibeChannel(channelName) {
        console.log(" phone nsfjkf", channelName)
        ChatEngineProvider._pubnub.subscribe({
            channels: [channelName],
            withPresence: true,
            triggerEvents: true,
            autoload: 100
        });
    }
    
    // static getMessageFromChannel(channelName) {
    //     console.log("get message channel name ", channelName)
    //     // console.log("get status ",ChatEngineProvider._pubnub.getStatus())
    //         // ChatEngineProvider._pubnub.getMessage('hkjdsas', m => {
    //         //     console.log("same user message ....", m)
    //         // });        
    //         let messages = ChatEngineProvider._pubnub.getMessage(channelName, () => {
    //             console.log(messages);
    //           });
    // }
    
    static publishMessage(message, channelName, otherUserName){

        // console.log("send message lkfds")
        // console.log(channelName)
        // console.log(message)
        // var newBadgeNumber = 0
        // PushNotification.getApplicationIconBadgeNumber((badgeNumber)=>{
        //     newBadgeNumber = badgeNumber + 1
        //     // PushNotification.setApplicationIconBadgeNumber(newBadgeNumber);
        // })

        // const ShortcutBadger = NativeModules.ShortcutBadger;
        // ShortcutBadger.applyCount(newBadgeNumber);
        // let title = otherUserName;
        let title = '';
        // let body = message[0].text;
        let body = 'You have a new message'

            // console.log("check message ", message[0].text)
        const messagePayload = {
           
            pn_apns: {
              aps: {
                alert: {title, body},
                sound: 'melody',
                badge:1
              },
            },
            pn_gcm: {
            //   data:  {contentTitle: title, contentText: body},
            data:  {title: title, message: body},
            },
            text: message,
           
          };
        ChatEngineProvider._pubnub.publish({
            message: messagePayload,
            channel: channelName
        },
        function (status, response) {
            // handle status, response
            if (status.error) {
                // handle error
                console.log(status)
            } else {
                console.log("message Published w/ timetoken", response)
            }
        });
    }

    static unsubscribeAllChannel(){
        ChatEngineProvider._pubnub.unsubscribeAll();
    }

    static removeAllChannelsFromPush(token, deviceType){
        console.log("token value ", token)
        ChatEngineProvider._pubnub.push.deleteDevice(
            {
                device: token, 
                pushGateway: deviceType // apns, gcm, mpns
            }, 
            function (status) {
                if (status.error) {
                    console.log("remove all channels for push operation failed w/ error:", status);
                    this.removeAllChannelsFromPush(token, deviceType)
                } else {
                    console.log("operation done!- remove all channels for push");
                }
            }
        );
    }
    
}
