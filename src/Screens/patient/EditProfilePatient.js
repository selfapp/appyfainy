

import React,{Component} from 'react';
import {View, Text,TouchableOpacity, ScrollView,Image,Keyboard, KeyboardAvoidingView,
      Dimensions, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import BottomBorderView from '../../Components/BottomBorderView';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet'
import MyActivityIndicator from '../../Components/activity_indicator';
import firebase from 'react-native-firebase';
import EditProfileTextField from '../../Components/EditProfileTextField';
import Header from '../../Components/Header';

const chatlistRef = firebase.firestore().collection('Chatlist');
const userRef = firebase.firestore().collection('Users');

const DEVICE_WIDTH = Dimensions.get("window").width;

export default class EditProfilePatient extends Component {

    constructor(){
        super();
        this.state = {
            loader:false,
            patientDetail:[],
            name:'',
            profileImage:'',
            profileImageUri:'',
            location:'',
            email:'',
            phone:'',
            patientKey:'',
            documents:[],
            patientDetails:[],
            imageName:'',
            imageBase64:''
        }
    }

    componentDidMount(){

        this.setState({patientDetail:this.props.navigation.state.params.patientDetail,
        name:this.props.navigation.state.params.patientDetail.name,
        
        email:this.props.navigation.state.params.patientDetail.email,
        phone:this.props.navigation.state.params.patientDetail.phone,
        patientKey:this.props.navigation.state.params.patientDetail.user
    })
        if(this.props.navigation.state.params.patientDetail.image){
            this.setState({profileImageUri:this.props.navigation.state.params.patientDetail.image,
                profileImage:this.props.navigation.state.params.patientDetail.image})
        }
        if(this.props.navigation.state.params.patientDetail.location){
            this.setState({location:this.props.navigation.state.params.patientDetail.location})
        }
    }

    backButtonAction(){
        this.props.navigation.navigate('ProfilePatient')
    }

    showActionSheet() {
        this.ActionSheet.show()
    }

    randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
      }

    takeImageAction(index){

        if(index === 0){
            ImagePicker.openCamera({
                cropping: false,
                includeBase64: true,
                mediaType: 'photo',
                compressImageQuality:0.1
              }).then(image => {
                let data = `data:${image.mime};base64,${image.data}`
                var rString = this.randomString(6, '0123456789');
                rString = "IMG_" + rString + '.JPG'
                this.setState({profileImage:data, imageBase64:image.data,
                imageName:rString})
              });
        }else if(index === 1){
            ImagePicker.openPicker({
                cropping: false,
                includeBase64: true,
                mediaType: 'photo',
                compressImageQuality:0.1
              }).then(image => {
                let data = `data:${image.mime};base64,${image.data}`
                if(Platform.OS === 'ios'){
                    this.setState({imageName:image.filename})
                }else{
                    var rString = this.randomString(6, '0123456789');
                    rString = "IMG_" + rString + '.JPG'
                    this.setState({imageName:rString})
                }
                this.setState({profileImage:data, imageBase64:image.data})
              });
        }
    }

    async fileUploadToFirebase(){

        this.setState({loader: true});
      
        let body = {
          "file": this.state.imageBase64,
          "user_id": this.state.phone,
          "file_name": this.state.imageName
        };
        try {
            fetch('https://us-central1-aepiphany-bdba7.cloudfunctions.net/charge/uploadProfilePicture', {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }) .then(response => response.json())
            .then(responseJson => {
               this.setState({loader: false});
                let phoneNumber = parseInt(this.state.phone)

                userRef.doc(this.state.patientKey).set({
                    name:this.state.name,
                    email:this.state.email,
                    phone:phoneNumber,
                    location:this.state.location,
                    image:responseJson.data.mediaLink,
                    user:this.state.patientKey,
                    subscription:true
                 }).then(async()=>{
                    this.setState({loader:false})
                    this.changeInChatlistForPateintImage(responseJson.data.mediaLink)
                    this.props.navigation.navigate('ProfilePatient')
                })
                if(responseJson.statusCode === 200){
                }
            
            })
            .catch(e => {
               this.setState({loader: false});
               console.log("error ...",e)
            }); 
           }catch (error) {
            // Error retrieving data
            console.log("error kjjl", error)
           }
       }

       async  changeInChatlistForPateintImage(imageLink){
       
        var _this = this;

        AsyncStorage.getItem('doctornumber').then((phoneNum)=>{

            let doc = firebase.firestore().collection('Chatlist').doc(phoneNum);

            doc.get().then(docSnapshot => {
            if(docSnapshot.data() === undefined){
               console.log("jflks jj empty")
            }else{
               
                var updatedChannels = [...docSnapshot.data().channels]
                docSnapshot.data().channels.map((item, index)=>{
                    if(item.type === 'patient'){
                        if(item.channelname.includes(_this.state.phone)){
                            var channelToChange = updatedChannels[index]
                            channelToChange['image'] = imageLink
                            updatedChannels[index] = channelToChange
                            chatlistRef.doc(phoneNum).set({
                                channels:updatedChannels
                              }).then(async()=>{
                            })
                        }
                    }
                })
            }
            }, err => {
            console.log(`Encountered error: ${err}`);
            });
        })
       }

    saveProfileAction(){

        if(this.state.name.length === 0){
            alert("Please enter name.")
        }else if(this.state.email.length === 0){
            alert("Please enter email.")
        }else if(this.state.phone.length === 0){
            alert("Please enter phone number.")
        }else if(this.state.location.length === 0){
            alert("Please enter location.")
        }else {
            this.setState({loader:true})
            if(this.state.imageName.length > 0){
                this.fileUploadToFirebase()
            }else{
                let phoneNumber = parseInt(this.state.phone)

                userRef.doc(this.state.patientKey).set({
                    name:this.state.name,
                    email:this.state.email,
                    phone:phoneNumber,
                    location:this.state.location,
                    image:this.state.profileImageUri,
                    user:this.state.patientKey,
                    subscription:true
                }).then(async()=>{
                    this.setState({loader:false})
                    this.props.navigation.navigate('ProfilePatient')
                })
            }
        }
    }

    render(){
        const navigate = this.props.navigation.navigate;

        return(
            <View style={{flex:1, backgroundColor:'lightgray'}}>
                {this.state.loader ? <MyActivityIndicator /> : null}


                <Header title={'Edit Profile'} withBackButton=
                    {() => this.backButtonAction()} type={'patient'} 
                        navigation={navigate}
                    SAVE={() => this.saveProfileAction()}/>
                <BottomBorderView 
                height={1}
                bkgColor='lightgray'
                width='100%'
                top={0}
                />
        <KeyboardAvoidingView style={{ flex: 1, backgroundColor:'white'}}
         behavior="padding" enabled keyboardVerticalOffset=
                          {Platform.OS === 'ios'? 64 : -150} >

                <ScrollView 
                        style={{ flex: 1, backgroundColor:'white'}} 
                        >
                <ActionSheet
                        ref={o => this.ActionSheet = o}
                        title={'Choose option for image'}
                        options={['Camera', 'Gallery', 'Cancel']}
                        cancelButtonIndex={2}
                        destructiveButtonIndex={2}
                        onPress={(index) => { this.takeImageAction(index)}}
                    />
                    <View style={{
                        width:'100%',
                        height:210,
                        backgroundColor:'#1D808D'
                    }}>
                    
                    <Image style={{
                    marginTop:20,
                    height:80,
                    width:80,
                    borderRadius:40,
                    borderColor:'white',
                    borderWidth:2,
                    alignSelf:'center'
                   }}
                   source = {
                        this.state.profileImage ? {uri:this.state.profileImage} : 
                        require('../../assets/avatar.png')}>
                  </Image>
                   <TouchableOpacity style={{marginTop:15}} onPress={()=>this.showActionSheet()}>
                    <Text style={{color:'white', textAlign:'center'}}>
                            Upload your photo
                    </Text>
                   </TouchableOpacity>

                        <Text style={{
                            marginLeft:20,
                            marginTop:15,
                            width:'100%',
                            textAlign:'left',
                            color:'white'
                        }}>
                            Personal Details
                        </Text>
                    </View>
                <View style={{
                    marginHorizontal:10,
                    marginTop:-30,
                    width:DEVICE_WIDTH - 20,
                    backgroundColor:'white',
                    borderRadius:5,
                    alignItems:'center',
                    borderRadius:5,
                    alignItems:'center',
                    borderColor:'lightgray', 
                    borderWidth:0.5,elevation: 5,
                    shadowColor: '#999666', 
                    shadowOffset: {width: 5, height: 5},
                    shadowOpacity: 0.7, shadowRadius: 5,
                }}>
            

            <EditProfileTextField
                title={'Name'}
                value={this.state.name}
                image ={require('../../assets/user-green.png')}
                onChangeText={(value)=>this.setState({name:value})}
            />
            <EditProfileTextField
                title={'E-mail address'}
                value={this.state.email}
                image ={require('../../assets/email.png')}
                onChangeText={(value)=>this.setState({email:value})}
            />
            <EditProfileTextField
                title={'Location'}
                value={this.state.location}
                image ={require('../../assets/map-green.png')}
                onChangeText={(value)=>this.setState({location:value})}
            />
                </View>
                </ScrollView>
                </KeyboardAvoidingView>
            </View>
        )
    }
}