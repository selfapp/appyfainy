


import React,{Component} from 'react';
import {View, Text, Platform, Dimensions, TouchableOpacity, Image,
     StyleSheet
     } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import MyActivityIndicator from '../../Components/activity_indicator';
import BottomBorderView from '../../Components/BottomBorderView';
import ChatEngineProvider from "../../lib/pubnub-chatengine";
import Moment from 'moment';
import { SwipeListView } from 'react-native-swipe-list-view';
import Header from '../../Components/Header';

const doctorRef = firebase.firestore().collection('Doctors');
const chatlistRef = firebase.firestore().collection('Chatlist');

const DEVICE_WIDTH = Dimensions.get("window").width;

export default class ChatScreenPatient extends Component {

    constructor(){
        super();
        this.state = {
            loader:false,
            patientPhoneNo:'',
            arrChannelMes:[],
            arrMessageNo:[],
            doctorName:'',
            doctorPhoneNum:'',
            arrTimeMessage:[],
            arrNewMessIndex:[],
            chatList:[]
        }
    }

    componentWillMount(){

        var _this = this;

        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            this.addListener();
            AsyncStorage.getItem('newchatindex').then((value)=>{
                if(value){
                    if(_this.props.navigation.state.params){
                        if(_this.props.navigation.state.params.channelName){
                            var toRemove = _this.props.navigation.state.params.channelName;
                            var arrIndex = [...JSON.parse(value)]
                           
                            var index = arrIndex.indexOf(toRemove);
                            if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
                                arrIndex.splice(index, 1);
                            }
                            AsyncStorage.setItem('newchatindex', JSON.stringify(arrIndex))
                            _this.setState({arrNewMessIndex:arrIndex})
                        }
                    }else{
                        _this.setState({arrNewMessIndex:JSON.parse(value)})
                    }
                }
            })
            AsyncStorage.getItem('patientphonenum').then((phoneNum)=>{

                _this.findListOfChannels(parseInt(phoneNum))
                AsyncStorage.getItem('docdetail').then((docdetail)=>{
    
                  const docValue = JSON.parse(docdetail)
                  this.setState({doctorName:docValue[0].name, 
                        doctorPhoneNum:docValue[0].phonenum,
                        patientPhoneNo:phoneNum,
                  })
                 AsyncStorage.setItem('doctornumber',docValue[0].phonenum.toString())
                })
            })
        })
    }

    findListOfChannels(phoneNumber){

        var _this = this;

        let doc = firebase.firestore().collection('Chatlist').doc(phoneNumber.toString());

        doc.onSnapshot(docSnapshot => {
       if (docSnapshot .empty) {
          _this.setState({loader:false})                                         
       }  else{
            if(docSnapshot.data().channels.length > 0){
                _this.setState({ chatList:docSnapshot.data().channels})
                _this.findLastMessage(docSnapshot.data().channels)
            }
        }
        if(docSnapshot.data() === undefined){
           _this.setState({loader:false})
        }else{
            if(docSnapshot.data().channels.length > 0){
                _this.setState({ chatList:docSnapshot.data().channels})
                _this.findLastMessage(docSnapshot.data().channels)
            }
        }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
    }

    findLastMessage(arrChat) {

        var arrChannel = []
        arrChat.map((item, index)=>{
            if(!item.block){
                arrChannel.push(item.channelname)
            }
            if(index === arrChat.length -1){
                this.fetchMessage(arrChannel)
            }  
        })
    }

    fetchMessage(arrChannel){
        var _this = this;
        ChatEngineProvider._pubnub.fetchMessages({
            channels: arrChannel,
            count: 1,
          }, (status, response) => {
            _this.setState({loader:false})
            if(response != undefined){
                if(response.channels){
                  _this.createLastMessageForChannels(response.channels)
                  _this.subscribeALlChannel(arrChannel)
                  AsyncStorage.getItem('token').then((token)=>{
                    console.log("token iss s s", token)
                    if(token){
                        _this.setState({token:token})
                        _this.addAllChannelForPush(arrChannel, token)
                    }else{
                        setTimeout(function(){
                            _this.checkTokenGenerated(arrChannel)
                          }, 2000);
                    }
                })                  
                }
            }
          });
    }

    checkTokenGenerated(arrChannel){
        AsyncStorage.getItem('token').then((token)=>{
            console.log("token called jkjk", token)
            if(token !== null){
                this.setState({token:token})
                this.addAllChannelForPush(arrChannel, token)
            }
        })
    }

    addListener(){
        var _this = this;
        ChatEngineProvider._pubnub.addListener({
          message: function(m) {
              // handle message
              console.log("received meassage in chat history patient........ ", m)
              var msg = m.message; // The Payload
              var publisher = m.publisher; //The Publisher
              var subscribedChannel = m.subscribedChannel;
            _this.updateChatHistory(subscribedChannel, msg.text[0].text, msg.text[0].createdAt, publisher)
          },
        });
      }

      updateChatHistory(publisherId, newMessage, newCreatedDate, publisher){

        if(this.state.arrNewMessIndex.includes(publisherId)){
        }else{
            if(publisher !== this.state.patientPhoneNo){
                var arrIndex = [...this.state.arrNewMessIndex]
                arrIndex.push(publisherId)
               this.setState({arrNewMessIndex:arrIndex})
               AsyncStorage.setItem('newchatindex', JSON.stringify(arrIndex))
            }
        }
        
        const newMessArr = [...this.state.arrChannelMes]
        const newTimeArr = [...this.state.arrTimeMessage]
        const newChatList = [...this.state.chatList]
      
        this.state.chatList.map((item, index)=>{
            if(item.channelname === publisherId){
                newMessArr[0] = newMessage
                newTimeArr[0] = newCreatedDate
                newChatList[0]= newChatList[index]
                for(let i = 1; i<=index; i++){
                    newChatList[i]= this.state.chatList[i-1]
                    newMessArr[i]= this.state.arrChannelMes[i-1]
                    newTimeArr[i]= this.state.arrTimeMessage[i-1]
                }
            }
        })
        this.setState({arrChannelMes:newMessArr})
        this.setState({arrTimeMessage:newTimeArr})
        this.setState({chatList:newChatList})
        this.updateChatHistoryInFirebase()
    }

    updateChatHistoryInFirebase(){
        chatlistRef.doc(this.state.patientPhoneNo).set({
                        channels:this.state.chatList
                      }).then(async()=>{
            })
    }

    createLastMessageForChannels(channels){
       var arrMess = []
       var arrMessNum = []
       var arrTime = []
       var index = 0
      
       const keys = Object.keys(channels)
       for (var key in channels) {

       index = index + 1
       if(channels[key][0].message.text){
           var message = channels[key][0].message.text[0].text
           var time = channels[key][0].message.text[0].createdAt
           arrMess.push(message)
           arrMessNum.push(key)
           arrTime.push(time)
       }
       this.setState({ arrMessageNo:arrMessNum})
       if(index === keys.length){
           this.mappingMessageTOPatient(arrMess, arrTime)
       }
    }
}
    subscribeALlChannel(arrChannel){
        ChatEngineProvider._pubnub.subscribe({
            channels: arrChannel,
            withPresence: true,
            triggerEvents: true,
            autoload: 100
        });
    }

    addAllChannelForPush(arrChannel, token){
        var platformType = ''
        if(Platform.OS === 'ios'){
            platformType='apns'
        }else{
            platformType='gcm'
        }
        ChatEngineProvider._pubnub.push.addChannels({
            channels: arrChannel,
            device: token,
            pushGateway: platformType, // apns or gcm
          }, (status) => {
            if (status.error) {
              console.log('operation failed w/ status: ', status);
            } else {
              console.log('all channels for push are done!');
            }
          });
    }

    mappingMessageTOPatient(arrMess, arrTime){
        const newMessArr = [...arrMess]
        
        const newTimeArr = [...arrTime]
        this.state.chatList.map((item, index)=>{
           var newIndex = ''
            newIndex = this.state.arrMessageNo.indexOf(item.channelname)
            newMessArr[index] = arrMess[newIndex]
            newTimeArr[index] = arrTime[newIndex]
        })
        this.setState({arrChannelMes:newMessArr})
        this.setState({arrTimeMessage:newTimeArr})
    }

    openChatScreen(item, tapindex){
       
        if(item.block){
            if(item.type === 'group' && item.blockpatient){
                this.goToChatScreen(item, tapindex)
            }else if(item.blockdoctor){
                this.goToChatScreen(item, tapindex)
            }
            else{
                this.goToChatScreen(item, tapindex)
            }
        }else{
            this.goToChatScreen(item, tapindex)
    }
}

    goToChatScreen(item, tapindex) {
        
        var toRemove = item.channelname;
        var arrIndex = [...this.state.arrNewMessIndex]
       
       var index = arrIndex.indexOf(toRemove);
        if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
            arrIndex.splice(index, 1);
        }
        AsyncStorage.setItem('newchatindex', JSON.stringify(arrIndex))
       this.setState({arrNewMessIndex:arrIndex})

        AsyncStorage.getItem('patientdetail').then((patientdetail)=>{
    
       const patientValue = JSON.parse(patientdetail)
        var otherUserName = ''
        if(item.type === 'group'){
            otherUserName = item.name
        }else{
            otherUserName = this.state.doctorName
        }
        this.props.navigation.navigate('ChatPage', 
        {doctorId:this.state.patientPhoneNo, 
            item:item,
            patientId:this.state.doctorPhoneNum,
            otherUsername:otherUserName,
            username:patientValue[0].name,
            userImage:patientValue[0].image,
            type:item.type,
            ownerType:'patient',
            tapindex:tapindex})
    })
    }

    setTime(itemDate){

        var sendMessageTime = ''
          var currentDate  = Moment().format('YYYY-MM-DD');
          var messageDate  = Moment(itemDate).format('YYYY-MM-DD');
          if(currentDate === messageDate){
              sendMessageTime =  Moment(itemDate).format('hh:mm A')
          }else{
              sendMessageTime =  Moment(itemDate).utc().format('MM/DD/YY')
          }
          return(
              <View style={{}}>
                  <Text style={{fontSize:10}}>
                      {sendMessageTime}
                  </Text>
              </View>
          )
      }
  
      blockUser = async(rowMap, index, item)=> {
        
        var arrTotalParticiptant = []
        item.admin.map((phonenum, index)=>{
            arrTotalParticiptant.push(phonenum)
        if(index === item.admin.length -1){
            item.nonadmin.map((phonenum)=>{
                arrTotalParticiptant.push(phonenum)
                arrTotalParticiptant.map((phonenum)=>{
                    this.blockUnblockOtherDoctorInChannel(phonenum, item.channelname)
                })
            })
        }
   })
}

blockUnblockOtherDoctorInChannel(phoneNumber, channelName){

    var _this = this;
    let doc = firebase.firestore().collection('Chatlist').doc(phoneNumber.toString());

        doc.get().then(docSnapshot => {
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
           
            if(docSnapshot.data().channels.length > 0){
                var arrData = [...docSnapshot.data().channels]
                docSnapshot.data().channels.map((item, index)=>{

                    if(item.channelname === channelName){
                        var channelDetail = arrData[index]
                        if(channelDetail.block){
                            channelDetail['block'] = false
                            channelDetail['blockpatient'] = false
                            channelDetail['blockdoctor'] = false
                        }else{
                            channelDetail['block'] = true
                            channelDetail['blockpatient'] = true
                            channelDetail['blockdoctor'] = false
                        }
                        arrData[index]= channelDetail
                    }
                })
                _this.addChannelInFirebase(phoneNumber, arrData) 
            }
        }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
}

addChannelInFirebase(key, arrData){

    var _this = this;
    chatlistRef.doc(key.toString()).set({
            channels:arrData
       }).then(async()=>{
        if(key === parseInt(this.state.doctorPhoneNum)){
            _this.setState({loader:false})
        }else{
            _this.setState({loader:false})
        }
    })
}

openDoctorProfile() {
    var _this = this;
    this.setState({loader:true})
    doctorRef.where('phone', '==', this.state.doctorPhoneNum).get()
            .then(snapshot => {
        _this.setState({loader:false})
        if (snapshot.empty) {
            console.log('No matching documents.');
        } else{
             snapshot.forEach(doc => {
                _this.props.navigation.navigate('ProfileDoctor', {otherDoctorProfile:doc.data()})
            });
        }
    });
}

    onRowDidOpen = (rowKey, rowMap) => {
        setTimeout(() => {
            this.closeRow(rowMap, rowKey);
        }, 800);
    }

    closeRow(rowMap, rowKey) {
        if (rowMap[rowKey]) {
            rowMap[rowKey].closeRow();
        }
    }
      
    _renderItem = ({item, index}) => {

        return(
            <View style={{ 
                justifyContent:'center',
                width:DEVICE_WIDTH,
                backgroundColor:'white'
             }}>
                <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}}
                    onPress={()=>this.openChatScreen(item, index)} >

                     {
                        item.type === 'group' ? (
                            <Image style={{
                                height:40,
                                width:40,
                                borderRadius:20,
                                marginHorizontal:5
                            }} source={require('../../assets/group-icon.png')}>
                            </Image>
                        ) : (
                            <TouchableOpacity onPress={()=>this.openDoctorProfile()}>
                                <Image style={{
                                    height:40,
                                    width:40,
                                    borderRadius:20,
                                    marginHorizontal:5
                                }} source={item.image ? {uri:item.image} : 
                                    (require('../../assets/avatar.png')
                                ) }>
                                </Image>
                            </TouchableOpacity>
                        )
                    }
                    <View style={{width:DEVICE_WIDTH-100, marginVertical:20}}>
                            <Text style={{
                                fontSize:15
                            }}
                                >
                                    {item.type === 'group' ? (item.groupchannelname) : 
                                    (this.state.doctorName)}
                               
                            </Text>
                            {
                                item.block ? (
                                        <Text style={{color:'red',
                                        fontSize:15}}>
                                     Unsubscribed
                                     </Text>
                                ) : (
                                    this.state.arrNewMessIndex.includes(item.channelname)? (
                                        <Text style={{color:'red',
                                    fontSize:12}}>
                                 {this.state.arrChannelMes[index]}
                                 </Text>
                                    ) : (
                                        <Text style={{color:'gray',
                                    fontSize:12}}>
                                 {this.state.arrChannelMes[index]}
                                 </Text>
                                    ) 
                                )
                            }
                    </View>
                    {
                        item.block ? (null) : (
                            this.setTime(this.state.arrTimeMessage[index])
                        )
                    }
                </TouchableOpacity>
                <BottomBorderView 
                width='100%'
                horizontal={0}
                top={0}
                />
            </View>
        )
    }

    render(){
        const navigate = this.props.navigation.navigate;

        return(
            <View style={{flex:1, backgroundColor:'white'}}>
                  
                {this.state.loader ? <MyActivityIndicator /> : null}
                   
                <Header title={'Conversations'}  type={'patient'} 
                  navigation={navigate}/>
                            <BottomBorderView 
                              top={0}
                            />
                <SwipeListView
                    useFlatList
                    data={this.state.chatList}
                    renderItem = {this._renderItem} 
                    extraData={this.state}
                    // keyExtractor={(rowData, index) => {
                    //     return rowData.id.toString();
                    //   }}
                    // keyExtractor={(item) => item.channelname}
                    stopLeftSwipe = {-1}               
            
                    renderHiddenItem={ (data, rowMap) => (
                            data.item.type === 'group' ? (
                                data.item.admin.includes(parseInt(this.state.patientPhoneNo)) && !data.item.blockdoctor ? 
                                ( 
                                <TouchableOpacity style={[styles.rowBack, styles.backRightBtn, styles.backRightBtnRight]} 
                                    onPress={ _ => this.blockUser(rowMap, data.index, data.item) }>
                                    <View style={{flexDirection:'column', alignItems:'center', justifyContent:'center'}}>
                            {
                                data.item.block ? (
                                    <Text style={styles.backTextWhite}>Subscribe  </Text>
                                ) : (
                                    <Text style={styles.backTextWhite}>Unsubscribe  </Text>
                                )
                            }
                                </View>
                                </TouchableOpacity>) : (null)
                            ) : (null)
                    )}
                    rightOpenValue={-110}
                    // closeOnRowOpen={false}
                    previewRowKey={'0'}
                    previewOpenValue={-40}
                    previewOpenDelay={3000}
                    // onRowDidOpen={this.onRowDidOpen}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 110
    },
    backRightBtnRight: {
        backgroundColor: '#44A7AF',
        height:70,
        marginVertical:10,
        right: 0
    },
    backTextWhite: {
        color: 'white',
        fontWeight:'bold'
    },
    rowBack: {
		alignItems: 'center',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
        paddingLeft: 15
	},
});