
import React,{Component} from 'react';
import {View, Text, Image,Dimensions,
       FlatList} from 'react-native';
import Header from '../../Components/Header';
import MyActivityIndicator from '../../Components/activity_indicator';
import BottomBorderView from '../../Components/BottomBorderView';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import Moment from 'moment';

const DEVICE_WIDTH = Dimensions.get("window").width;

export default class TransactionHistory extends Component {

    constructor(){
        super();
        this.state = {
            loader:false,
            arrHistory:[]
        }
    }

 componentWillMount(){

        var _this = this;
        this.setState({loader:true})
        AsyncStorage.getItem('patientid').then((patientid)=>{
       var arrTransaction = []
       let doc = firebase.firestore().collection('Transactions').doc(patientid).collection('transactions');

        doc.get().then(docSnapshot => {
            _this.setState({loader:false})
            docSnapshot.forEach(doc => {
                let docDetails = doc.data()
                arrTransaction.push(docDetails)
            _this.setState({loader:false, arrHistory:arrTransaction})
        })
    }, err => {
        console.log(`Encountered error: ${err}`);
     });
   })
}

    backButtonAction(){
        this.props.navigation.goBack()
    }
    
    _renderItem = ({item, index}) => {

            return(
                <View style={{ 
                    backgroundColor:'white',
                    justifyContent:'center',
                    width:DEVICE_WIDTH,
                    marginVertical:10
                 }}>
                        <View style={{ flexDirection:'row', alignItems:'center',
                            marginVertical:10, }}>
                                <Image style={{
                                    height:40,
                                    width:40,
                                    borderRadius:20,
                                    marginHorizontal:5
                                }} source={require('../../assets/money-received.png')}>
                                </Image>
                                    <View style={{flexDirection:'row'}}>
                                        <Text style={{
                                                    fontSize:14,
                                                    flex:1.4
                                                }}>
                                                {"Payment made to "+item.doctor_name}
                                        </Text>
                                        <Text style={{fontSize:14,
                                            color:'#1D808D',
                                            flex:0.6
                                            }}>
                                                ${parseFloat(Math.round(item.amount * 100) / 100).toFixed(2)}
                                        </Text> 
                                    </View>
                                    <View style={{flexDirection:'row',
                                            marginTop:5}}>
                                        <Text style={{fontSize:10,
                                            color:'gray',
                                            flex:1
                                            }}>
                                            { Moment(item.date).format('MM/DD/YY hh:mmA')}
                                        </Text>
                                    </View>
                        </View>
                        <BottomBorderView 
                        width='100%'
                        horizontal={0}
                        top={0}
                        />
                    
                </View>
            )
    }

    render(){

        return(
            <View style={{flex:1, backgroundColor:'white'}}>
                 
                {this.state.loader ? <MyActivityIndicator /> : null}
                <Header title={'Transaction History'} withBackButton=
                    {() => this.backButtonAction()} type={'patient'} 
                    />
                    <FlatList
                    data = {this.state.arrHistory}
                    extraData={this.state}
                    renderItem = {this._renderItem} />
            </View>
        )
    }
}