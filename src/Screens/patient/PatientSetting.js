
import React,{Component} from 'react';
import {View, Text, Alert, Dimensions, TouchableOpacity, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import MyActivityIndicator from '../../Components/activity_indicator';
import BottomBorderView from '../../Components/BottomBorderView';
import Header from '../../Components/Header';
import firebase from 'react-native-firebase';
import ChatEngineProvider from "../../lib/pubnub-chatengine";

import { StackActions, NavigationActions } from 'react-navigation';

const DEVICE_WIDTH = Dimensions.get("window").width;

export default class PatientSetting extends Component {

    constructor(){
        super();
        this.state = {
            loader:false,
            arrChannelNames:[]
        }
    }

   async componentWillMount(){
    
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        AsyncStorage.getItem('patientphonenum').then((phoneNum)=>{
            this.findAllChannels(phoneNum)
        })
    })
    }

    async logoutAction() {
        AsyncStorage.getItem('token').then((token)=>{
    
            console.log("token /////", token)
            if(token !== null){
                var deviceType=''
                if(Platform.OS === 'ios'){
                    deviceType = 'apns'
                }else{
                    deviceType = 'gcm'
                }
                ChatEngineProvider.removeAllChannelsFromPush(token, deviceType)
            }       
        })
        AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
        await firebase.auth().signOut();
        firebase.firestore().disableNetwork()
        firebase.database().goOffline()
        ChatEngineProvider.unsubscribeAllChannel();
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'loginNavigationOption' })],
          });
          this.props.navigation.dispatch(resetAction);
        }

    cancelSubscriptionTapAction() {
        Alert.alert(
            "Termination of Services",
            `Are you sure you want to cancel subscription?
            
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.`,
            [
              {
                text: 'Yes',
                onPress: () => {
                   this.cancelSubscription()
                },
              },
              {text: 'No', onPress: () => 
              console.log("press no")},
            ],
            {cancelable: false},
          );
    }

    cancelSubscription() {

       this.setState({loader: true});
        AsyncStorage.getItem('patientid').then((patientid)=>{
            
            let body = {
                "user_id": patientid
            };
            console.log("firebase store to cancel subscription body")
            console.log(body)
        
            try {
                fetch('https://us-central1-aepiphany-bdba7.cloudfunctions.net/charge/cancelSubscription', {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(body)
                }) .then(response => response.json())
                .then(responseJson => {
                  this.setState({loader: false});
                    console.log("response firebase cancelSubscription")
                    console.log(responseJson)
                    if(responseJson.status){
                        this.saveUnsubscribedChannelsOnFirebase()
                    }
                })
                .catch(e => {
                   this.setState({loader: false});
                   console.log("error ...",e)
                }); 
               }catch (error) {
               this.setState({loader: false});
                console.log("error kjjl", error)
               }
        })
    }

    saveUnsubscribedChannelsOnFirebase() {

        let doc = firebase.firestore().collection('UnsubscribedChannels');
        
            doc.add({
               channels: this.state.arrChannelNames
                }).then(async()=>{
                   this.logoutAction()
                })
    }

    findAllChannels(phoneNumber){

        var _this = this;
        let doc = firebase.firestore().collection('Chatlist').doc(phoneNumber);

        doc.get().then(docSnapshot => {
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
               var channelsName = []
               docSnapshot.data().channels.map((item, index)=>{
                   channelsName.push(item.channelname)
                  
                  if(index === docSnapshot.data().channels.length - 1){
                     _this.setState({arrChannelNames:channelsName})
                  }
               })
        }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
    }

    render(){

        const navigate = this.props.navigation.navigate;

        return(
            <View style={{flex:1, backgroundColor:'white'}}>
                {this.state.loader ? <MyActivityIndicator /> : null}
                <Header title={'Settings'}  type={'patient'} 
                        navigation={navigate}/>

                <BottomBorderView 
                height={1}
                bkgColor='lightgray'
                width='100%'
                top={0}
                />
                <View style={{
                    marginHorizontal:10,
                    marginTop:30,
                    backgroundColor:'white',
                    borderRadius:5,
                    alignItems:'center',
                    borderRadius:5,
                    alignItems:'center',
                    borderColor:'lightgray', 
                    borderWidth:0.5,elevation: 5,
                    shadowColor: '#999666', 
                    shadowOffset: {width: 5, height: 5},
                    shadowOpacity: 0.7, shadowRadius: 5,
                }}>
                    <View style={{
                        flexDirection:'row',
                        marginVertical:15,
                        marginHorizontal:15,
                        width:DEVICE_WIDTH-40,
                        alignItems:'center'
                    }}>
                        <Text style={{
                            textAlign:'left',
                            width:DEVICE_WIDTH-100,
                        }}>
                            Subscription
                        </Text>
                        {
                                <TouchableOpacity style={{
                                    width:60,
                                    height:30,
                                    justifyContent:'center',
                                    alignItems:'center'
                                }} onPress={()=>this.cancelSubscriptionTapAction()}>
                                    <Text style={{color:'#1D808D',
                                        fontSize:14}}>
                                        Cancel
                                    </Text>
                                </TouchableOpacity>
                        }
                       
                    </View>
                    {/* <BottomBorderView 
                        height={1}
                        bkgColor='lightgray'
                        width='100%'
                        top={0}
                        />
                 <TouchableOpacity style={{
                            width:DEVICE_WIDTH-40,
                            height:60,
                            justifyContent:'center',
                        }}onPress={()=> this.props.navigation.navigate('PaymentSetting')}>
                            <Text style={{
                                textAlign:'left'
                            }}>
                                Payment setting
                            </Text>
                 </TouchableOpacity> */}
                 <BottomBorderView 
                        height={1}
                        bkgColor='lightgray'
                        width='100%'
                        top={0}
                        />
                     <TouchableOpacity style={{
                            width:DEVICE_WIDTH-40,
                            height:60,
                            justifyContent:'center',
                        }}onPress={()=> this.props.navigation.navigate('TransactionHistory')}>
                            <Text style={{
                                textAlign:'left'
                            }}>
                                Transaction history
                            </Text>
                 </TouchableOpacity>
                 <BottomBorderView 
                        height={1}
                        bkgColor='lightgray'
                        width='100%'
                        top={0}
                        />
                </View>
            </View>
        )
    }
}