
import React,{Component} from 'react';
import {View, Text,TouchableOpacity, ScrollView,Image,
      Dimensions, StyleSheet, Platform, FlatList, Modal, Linking} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Button from '../../Components/Button';
import BottomBorderView from '../../Components/BottomBorderView';
import Header from '../../Components/Header';
import MyActivityIndicator from '../../Components/activity_indicator';
import firebase from 'react-native-firebase';
import { StackActions, NavigationActions } from 'react-navigation';
import ProfileTextField from '../../Components/ProfileTextField';
import ChatEngineProvider from "../../lib/pubnub-chatengine";
import Pdf from 'react-native-pdf';

const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;

export default class ProfilePatient extends Component {

    constructor(){
        super();
        this.state = {
            loader:false,
            patientDetail:[],
            checkOtherPatient:false,
            arrRecDoc:[],
            arrSendDoc:[],
            phoneNum:'',
            showSend:true,
            showPdf:false,
            showImage:false,
            docImageLink:''
        }
    }

    componentDidMount () {
        
        if(this.props.navigation.state.params){
            if(this.props.navigation.state.params.otherPatientProfile){
            this.setState({patientDetail:this.props.navigation.state.params.otherPatientProfile,
                checkOtherPatient:true})
        }
        }else{
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            this.setState({loader:true})
            AsyncStorage.getItem('patientid').then((patientid)=>{
                this.getPatientDetail(patientid)
            })
        });
    }
}

 async getPatientDetail(patientid){

    var _this = this;

    let doc = firebase.firestore().collection('Users').doc(patientid);

        doc.get().then(docSnapshot => {
        _this.setState({loader:false})

        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
           
            _this.setState({patientDetail:docSnapshot.data(), phoneNum:docSnapshot.data().phone})
           
            var patDetailKey = [{'name':docSnapshot.data().name, 
            'phonenum': docSnapshot.data().phone.toString(), 'image': docSnapshot.data().image, 'email':docSnapshot.data().email}]
            var arrDocument = []
            AsyncStorage.setItem('patientdetail',JSON.stringify(patDetailKey))

            let doc = firebase.firestore().collection('Attachments').doc(docSnapshot.data().phone.toString())
                      .collection('files');

            doc.get().then(docSnapshot => {
              docSnapshot.forEach(doc => {
                 console.log(doc .data())
                let transDetails = doc .data()
                arrDocument.push(transDetails)
                console.log(arrDocument)
                _this.filterDocuments(arrDocument)
            })           
            }, err => {
            console.log(`Encountered error: ${err}`);
            });
        }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
}

filterDocuments(arrDocument) {

    var sendDoc = []
    var receiveDoc = []
    arrDocument.map((item, index)=>{
        if(item.senderId === this.state.phoneNum){
            sendDoc.push(item)
        }else{
            receiveDoc.push(item)
        }
        if(index === arrDocument.length - 1){
            this.setState({arrSendDoc:sendDoc, arrRecDoc:receiveDoc})
        }
    })
}

    editProfileAction(){
        this.props.navigation.navigate('EditProfilePatient', {patientDetail:this.state.patientDetail})
    }
    
   async logoutAction() {
        AsyncStorage.getItem('token').then((token)=>{
            console.log("token /////", token)
            if(token !== null){
                var deviceType=''
                if(Platform.OS === 'ios'){
                    deviceType = 'apns'
                }else{
                    deviceType = 'gcm'
                }
                ChatEngineProvider.removeAllChannelsFromPush(token, deviceType)
            }       
        })
        await firebase.auth().signOut();
         firebase.database().goOffline()
         firebase.firestore().disableNetwork()
        ChatEngineProvider.unsubscribeAllChannel();
        AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
       
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'loginNavigationOption' })],
          });
        this.props.navigation.dispatch(resetAction);
    }

    backButtonAction(){
        this.props.navigation.goBack()
    }

    changeDocumentTab(type){
        if(type === 'send'){
            this.setState({showSend:true})
        }else{
            this.setState({showSend:false})
        }
    }

    documentPressAction(item) {

        if(item.contentType.includes('image')){
            this.setState({showImage:true, docImageLink:item.mediaLink})
        }else if(item.contentType.includes('audio') || item.contentType.includes('video')){
            Linking.canOpenURL
            (item.mediaLink).then(supported => {
                if (supported) {
                  Linking.openURL(item.mediaLink);
                } else {
                  console.log("Don't know how to open URI: " + this.state.doctorDetail.website);
                }
              });
        }else{
            const source = {uri:item.mediaLink, cache:true};
            this.setState({showPdf:true, docImageLink:source})
        }
    }

    _renderItem = ({item}) => {

            return(
                <TouchableOpacity style={{ 
                    backgroundColor:'white',
                    justifyContent:'center',
                    width:DEVICE_WIDTH-20,
                    height:60
                 }}onPress={()=>this.documentPressAction(item)}>
                    <View style={{flexDirection:'row',alignItems:'center',
                    }}>
                        {
                            item.contentType.includes('image') ? (
                                <Image style={{
                                    height:40,
                                    width:40,
                                    borderRadius:20,
                                    marginHorizontal:5
                                }} source={{uri:item.mediaLink}}>
                                </Image>
                            ) : (
                                    item.contentType.includes('audio') ? (
                                        <Image style={{
                                            height:40,
                                            width:40,
                                            borderRadius:20,
                                            marginHorizontal:5
                                        }} source={require('../../assets/audio_round.png')}>
                                        </Image>
                                    ) : 
                                    (
                                        item.contentType.includes('video') ? (
                                            <Image style={{
                                                height:40,
                                                width:40,
                                                borderRadius:20,
                                                marginHorizontal:5
                                            }} source={require('../../assets/video_round.png')}>
                                            </Image>
                                        ) : (
                                        <Image style={{
                                            height:40,
                                            width:40,
                                            borderRadius:20,
                                            marginHorizontal:5
                                        }} source={require('../../assets/pdf_round.png')}>
                                        </Image>
                                        )
                                    )
                            )
                        }
                                <Text style={{
                                    fontSize:14
                                }}>
                                   {item.name}
                                </Text>
                                <Text style={{fontSize:10,
                                color:'gray',
                                marginTop:3}}>
                                    
                                </Text>                          
                                   
                    </View>
                    <BottomBorderView 
                    width='100%'
                    horizontal={0}
                    top={10}
                    />
                </TouchableOpacity>
            )
    }

    render(){

        const navigate = this.props.navigation.navigate;
        var sendBkgColor = ''
        var sendTextColor = ''
        var receiveBkgColor = ''
        var receiveTextColor = ''
        if(this.state.showSend){
            sendBkgColor = '#1D808D',
            sendTextColor = 'white',
            receiveBkgColor = 'white',
            receiveTextColor = 'black'
        }else{
            receiveBkgColor = '#1D808D',
            receiveTextColor = 'white',
            sendBkgColor = 'white',
            sendTextColor = 'black'
        }

        return(
            <View style={{flex:1, backgroundColor:'lightgray'}}>
            {
               this.state.showImage ? (

                <Modal animationType="fade" transparent={true} visible= {true}>
                    <View style={{alignItems:'center'}}>
                    <View style={{
                            height:'100%',
                            alignItems: 'center',
                            backgroundColor: 'rgba(0,0,0,0.60)'
                        }}>
                        {this.state.loader ? <MyActivityIndicator /> : null}
        
                        <View style={{
                            marginTop:30, 
                            marginHorizontal:20, 
                            flexDirection:'column', 
                            backgroundColor:'white',
                            borderRadius:5,
                            width:DEVICE_WIDTH-40,
                            height:DEVICE_HEIGHT-60,
                            alignItems:'center'}}>
                            
                            <TouchableOpacity style={{ 
                                position:'absolute', 
                                right:0,  
                                width:60,
                                height:70,
                                justifyContent:'center',
                                alignItems:'center'}} 
                                onPress={()=> this.setState({showImage:false})}>
                                            <Image source={require('../../assets/Close.png')} />
                            </TouchableOpacity>
                            <Image style={{
                                height:DEVICE_HEIGHT-100,
                                width:'100%',
                                marginTop:40,
                                resizeMode:'contain'
                            }} source={{uri:this.state.docImageLink}}>
                            </Image>
                        </View>
                    </View>
                    </View>
            </Modal> 

       ) : (null)
    }

{
        this.state.showPdf ? (
          <Modal animationType="fade" transparent={true} visible= {true}>
                  <View style={{
                        height:'100%',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.60)',
                        width:DEVICE_WIDTH
                    }}>
                       <View style={{
                         marginTop:30, 
                         flexDirection:'column', 
                         backgroundColor:'white',
                         borderRadius:5,
                        height:DEVICE_HEIGHT-60,
                         width:DEVICE_WIDTH,
                         alignItems:'center'}}>
                            <TouchableOpacity style={{ 
                            position:'absolute', 
                            right:0,  
                            width:60,
                            height:70,
                            justifyContent:'center',
                            alignItems:'center',
                        }} 
                            onPress={()=> this.setState({showPdf:false})}>
                                        <Image source={require('../../assets/Close.png')} />
                         </TouchableOpacity>

              <View style={{marginTop:50}}>
                <Pdf
                    source={this.state.docImageLink}
                    onLoadComplete={(numberOfPages,filePath)=>{
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        console.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        console.log(error);
                    }}
                    style={styles.pdf}/>
               </View>
                         </View>
                </View>
            </Modal> 
        ) : (null)
      }
                {this.state.loader ? <MyActivityIndicator /> : null}
                {
                this.state.checkOtherPatient ? (
                    <Header title={'Patient Profile'} type={'patient'} 
                                navigation={navigate}
                                withBackButton=
                                {() => this.backButtonAction()}/>
                ) : (
                    <Header title={'Patient Profile'} type={'patient'} 
                    navigation={navigate}
                    EDIT={() => this.editProfileAction()}/>
                )
            }
                <BottomBorderView 
                height={1}
                bkgColor='lightgray'
                width='100%'
                top={0}
                />
            <ScrollView style={{flex:1,backgroundColor:'white'}}>
                
                    <View style={{
                        width:'100%',
                        height:170,
                        backgroundColor:'#1D808D'
                    }}>
                        <View style={{
                            flexDirection:'row',
                            height:80,
                            alignItems:'center',
                            marginHorizontal:20,
                            marginTop:20
                        }}>
                            <View style={{
                                width:DEVICE_WIDTH-110,
                                justifyContent:'center'
                            }}>
                                <Text style={{color:'white'}}>
                                    {this.state.patientDetail ? this.state.patientDetail.name : ''}
                                </Text>
                                <View style={{flexDirection:'row',
                                            marginTop:5,
                                            alignItems:'center'}}> 
                                  <Image source={require('../../assets/mini-map.png')}>
                                  </Image>
                                  <Text style={{
                                      marginLeft:5,
                                      fontSize:10,
                                      color:'white'
                                  }}>
                                    {this.state.patientDetail ? (this.state.patientDetail.location ? this.state.patientDetail.location : 'No location found') : 'No location found'}
                                  </Text>
                                </View>
                            </View>
                            <Image style={{
                                width:60,
                                height:60,
                                borderRadius:30,
                                borderColor:'white',
                                borderWidth:2,
                            }}
                          source={this.state.patientDetail ? (this.state.patientDetail.image ? {uri:this.state.patientDetail.image} :
                            require('../../assets/avatar.png')) :
                             require('../../assets/avatar.png')}>
                        </Image>
                        </View>
                        <Text style={{
                            marginLeft:20,
                            marginTop:15,
                            width:'100%',
                            textAlign:'left',
                            color:'white'
                        }}>
                            Personal Details
                        </Text>
                    </View>
                <View style={{
                    marginHorizontal:10,
                    marginTop:-30,
                    width:DEVICE_WIDTH - 20,
                    backgroundColor:'white',
                    borderRadius:5,
                    alignItems:'center',
                    borderRadius:5,
                    alignItems:'center',
                    borderColor:'lightgray', 
                    borderWidth:0.5,elevation: 5,
                    shadowColor: '#999666', 
                    shadowOffset: {width: 5, height: 5},
                    shadowOpacity: 0.7, shadowRadius: 5,
                }}>
               <ProfileTextField
                    title={'E-mail address'}
                    value={this.state.patientDetail ? this.state.patientDetail.email : ''}
                    image ={require('../../assets/email.png')}
                />
                <ProfileTextField
                    title={'Phone number'}
                    value={this.state.patientDetail ? this.state.patientDetail.phone : ''}
                    image ={require('../../assets/phone.png')}
                />

{
    this.state.checkOtherPatient ? (null): (
        <View>
        <View style={styles.boxView}>
          <Image source={require('../../assets/documents.png')}>
          </Image>
          <Text style={{
              fontSize:12,
              marginLeft:5
          }}>
              Documents
          </Text>
          </View>
          <View style={{flexDirection:'row', marginBottom:10}}>
              <TouchableOpacity style={{width:DEVICE_WIDTH/2 - 22,
              backgroundColor:sendBkgColor,
              height:40,
              marginLeft:7,
              marginTop:10,
              alignItems:'center',
              justifyContent:'center',
              borderWidth:0.8,
              borderRadius:5}}
              onPress={()=>this.changeDocumentTab('send')}>
                  <Text style={{color:sendTextColor,
                      fontSize:16,
                      fontWeight:'bold'}}>
                      Shared
                  </Text>
              </TouchableOpacity>
              <TouchableOpacity style={{width:DEVICE_WIDTH/2 - 22,
                  backgroundColor:receiveBkgColor,
                  height:40,
                  marginLeft:10,
                  marginTop:10,
                  alignItems:'center',
                  justifyContent:'center',
                  borderWidth:0.8,
                  borderRadius:5}}
                  onPress={()=>this.changeDocumentTab('receive')}>
                  <Text style={{color:receiveTextColor,
                      fontSize:16,
                      fontWeight:'bold'}}>
                      Received
                  </Text>
              </TouchableOpacity>
          </View>

      <View style={{
          height:200}}>
      {
              this.state.showSend ? (
                  <FlatList
                  data = {this.state.arrSendDoc}
                  extraData={this.state}
                  renderItem = {this._renderItem} />
              ) : (
                  <FlatList
                  data = {this.state.arrRecDoc}
                  extraData={this.state}
                  renderItem = {this._renderItem} />
              )
          }
           </View>
      </View>
    )
}
                </View>    
                {
                this.state.checkOtherPatient ? (null): (
                <Button
                    horizontal = '8%'
                    top = {40}
                    width = '84%'
                    bottom={20}
                    radius = {20}
                    backgColor = '#44A7AF'
                    height = {40}
                    textColor = 'white'
                    titleSize = {16}
                    title = 'Logout'
                    buttonAction = {()=>this.logoutAction()}
                />
                )
                }
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    boxView:{ marginHorizontal:10,
        width:DEVICE_WIDTH - 40,
        alignItems:'center',
        marginTop:25,
        flexDirection:'row'},
    pdf: {
        flex:1,
        width:DEVICE_WIDTH,
        height:DEVICE_HEIGHT,
    }
})