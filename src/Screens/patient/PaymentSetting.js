

import React,{Component} from 'react';
import {View, Text, Image,Dimensions,
    TouchableOpacity} from 'react-native';
import Header from '../../Components/Header';
import MyActivityIndicator from '../../Components/activity_indicator';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

const DEVICE_WIDTH = Dimensions.get("window").width;

export default class PaymentSetting extends Component {

    constructor(){
        super();
        this.state = {
            loader:false,
            cardLastDigits:1245
        }
    }

 componentWillMount(){

 }

    backButtonAction(){
        this.props.navigation.goBack()
    }

    addNewCard() {
        
    }

    render(){

        return(
            <View style={{flex:1, backgroundColor:'white'}}>
                 
                {this.state.loader ? <MyActivityIndicator /> : null}
                <Header title={'Payment Setting'} withBackButton=
                    {() => this.backButtonAction()} type={'patient'} 
                    />

                <View style={{
                    flexDirection:'row',
                    marginHorizontal:10,
                    marginTop:20,
                   // backgroundColor:'red',
                    borderWidth:1,
                    borderRadius:15,
                    height:50,
                    alignItems:'center'
                }}>
                    <Text style={{
                        marginLeft:10,
                        color:'gray'
                    }}>
                        {`XXXX-XXXX-XXXX-${this.state.cardLastDigits}`}
                    </Text>
                    <TouchableOpacity style={{
                         position: 'absolute', right: 10, 
                         width: 50, height: 40, alignItems: 'center', justifyContent: 'center'
                    }} 
                    onPress={()=> this.addNewCard()}>
                            <Image source={require('../assets/patient_edit.png')}>
                            </Image>
                    </TouchableOpacity>
                </View>    
                   
            </View>
        )
    }
}