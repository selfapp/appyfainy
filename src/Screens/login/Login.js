
import React,{Component} from 'react';
import {View, Text, Image, TextInput, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard} from 'react-native';
import Button from '../../Components/Button';
import BottomBorderView from '../../Components/BottomBorderView';
import MyActivityIndicator from '../../Components/activity_indicator';

import * as firebase from 'react-native-firebase';

const doctorRef = firebase.firestore().collection('Doctors');
const userRef = firebase.firestore().collection('Users');

export default class Login extends Component {

    constructor(){
       super();
       this.state = {
           phoneNum:'',
           loader:false,
           countryCode:'+1',
           count:0,
           doctorPresense:false,
           patientPresence:false
       }
    }

    increseCount() {

        this.setState({count:this.state.count + 1})

        if(this.state.count > 2){
            alert("Change country code to India.")
            this.setState({countryCode:'+91'})
        }
    }

    signInWithPhoneNumber(){
        var phoneNo = this.state.countryCode+this.state.phoneNum
        this.setState({loader:true})
        firebase.auth().signInWithPhoneNumber(phoneNo)
        .then(confirmResult => {
            this.setState({loader:false})
            this.props.navigation.navigate('Otp',{phoneNumber:this.state.phoneNum,
                confirmResult:confirmResult, countryCode:this.state.countryCode })
        })
        .catch(error => {
            console.log("Erro occured ....")
            console.log(error)
            this.setState({phoneNum:''})
            var errorMessage = JSON.stringify(error)
            this.setState({loader:false})
            if(errorMessage.includes("Network error") || errorMessage.includes('auth/unknown')){
                alert("Please check your phone connection and try again.")
            }else{
                alert(error)
            }
        });
    }

   async continueAction() {
        if(this.state.phoneNum.length < 10){
            alert("Please enter correct phone number.")
        }else {
            this.setState({loader:true})

            var _this = this;
            firebase.database().goOnline()
            firebase.firestore().enableNetwork()
            doctorRef.where('phone', '==', parseInt(this.state.phoneNum)).get()
            .then(snapshot => {
               
                if (snapshot.empty) {
                    console.log('No matching documents.');

                    let doc = firebase.firestore().collection('Chatlist').doc(this.state.phoneNum);

                    doc.get().then(docSnapshot => {
                 
                    if(docSnapshot.data() === undefined){
                       _this.setState({loader:false, phoneNum:''})
                       alert("Sorry! you can not register at this time. Because no doctor has sent you invitation.")
                    }else{
                        console.log(docSnapshot.data())

                        userRef.where('phone', '==', parseInt(this.state.phoneNum)).get()
                            .then(snapshot => {
                        _this.setState({loader:false})
                          
                            if (snapshot.empty) {
                                console.log('No matching documents.');  
                                _this.signInWithPhoneNumber()
                              }  else{
                                snapshot.forEach(doc => {
                                    console.log("sfhkd jsdhkdfjs", doc.data());
            
                                    if(doc.data().subscription) {
                                        _this.signInWithPhoneNumber()
                                    }else{
                                        const keys = Object.keys(doc.data())
                                        if(keys.includes('subscription_id')) {
                                        // if(doc.data().subscription_id){
                                            if(doc.data().subscription_id.length > 0){
                                                _this.signInWithPhoneNumber()
                                            }else{
                                                alert("Please contact admin.")
                                            }
                                        }else{
                                            _this.signInWithPhoneNumber()
                                        }
                                    }
                                    })
                                }
                            })
                    }
                   
                    }, err => {
                    console.log(`Encountered error: ${err}`);
                    })
                  }  else{
                    snapshot.forEach(doc => {
                        _this.signInWithPhoneNumber()
                        console.log("sfhkd jsdhkdfjs", doc.data());
                      });
                  }
            })
        }
    }

    render(){
        return(
            <View style={{
                flex:1, backgroundColor:'white'
                }} >

               <KeyboardAvoidingView behavior={'position'}>
               {this.state.loader ? <MyActivityIndicator /> : null}
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View>

                            <View style={{alignItems:'center'}}>
                                <TouchableWithoutFeedback onPress={()=>this.increseCount()}>
                                <Image style={{width:145, height:107, marginTop:'20%'}} 
                                      source={require('../../assets/logo-green.png')}>
                                </Image>
                                </TouchableWithoutFeedback>
                            </View>
                            <Text style={{
                                fontWeight:'bold',
                                fontSize:18,
                                marginHorizontal:'15%',
                                marginTop:'5%',
                                color:'#1B7583',
                                textAlign:'center'
                            }}>
                                Please enter your phone number to get started
                            </Text>
                            <TextInput style={{
                                marginHorizontal:'15%',
                                marginTop:30
                            }}  placeholder="Enter your phone number"
                                underlineColorAndroid='transparent'
                                autoCorrect={false}
                                maxLength={10}
                                value={this.state.phoneNum}
                                keyboardType='phone-pad'
                                onChangeText={(value)=> this.setState({phoneNum:value})}
                            >
                            </TextInput>
                            <BottomBorderView 
                            horizontal='15%'
                            top={5}
                            />
                            <Text style={{
                                color:'gray',
                                fontSize:11,
                                marginHorizontal:'15%',
                                marginTop:'5%'
                            }}>
                                A 6 digit passcode will be sent via text to verify your mobile number!
                            </Text>
                            <Button
                            horizontal = '8%'
                            top = '10%'
                            radius = {20}
                            backgColor = '#44A7AF'
                            height = {40}
                            // weight = 'bold'
                            textColor = 'white'
                            titleSize = {16}
                            title = 'Continue'
                            buttonAction = {()=>this.continueAction()}
                            />
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </View>
        )
    }
}