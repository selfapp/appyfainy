
import React,{Component} from 'react';
import {View, Text, Image, KeyboardAvoidingView, 
    TouchableWithoutFeedback, Keyboard} from 'react-native';
import OTPInput from 'react-native-otp';
import AsyncStorage from '@react-native-community/async-storage';
import Button from '../../Components/Button';
import BottomBorderView from '../../Components/BottomBorderView';
import BackButton from '../../Components/BackButton';
import MyActivityIndicator from '../../Components/activity_indicator';
import * as firebase from 'react-native-firebase';
import ChatEngineProvider from "../../lib/pubnub-chatengine";
import { StackActions, NavigationActions } from 'react-navigation';

const doctorRef = firebase.firestore().collection('Doctors');

export default class Otp extends Component {

    constructor(){
        super();
        this.state = {
            otp:'',
            loader:false,
            confirmResult:'',
            phone:'',
            countryCode:'',            
        }
        this._isMounted = false
    }

    componentDidMount(){

        this._isMounted = true;
        this._isMounted && this.setState({confirmResult:this.props.navigation.state.params.confirmResult,
            phone: this.props.navigation.state.params.phoneNumber,
          countryCode:this.props.navigation.state.params.countryCode})
    }

    componentWillUnmount() {
        this._isMounted = false;
        this.setState({ otp:'',
        confirmResult:'',
        phone:'',
        countryCode:'',
        loader:false})
     }

    resendCodeAction() {

        this.setState({loader:true})
        var phoneNo = this.state.countryCode+this.state.phone
        firebase.auth().signInWithPhoneNumber(phoneNo, true)
        .then((confirmResult) => {
            alert('Code resent.')
            this.setState({confirmResult:confirmResult, loader:false})
        }).catch(error =>{
            this.setState({loader:false})
            alert(error)
           console.log("Error", error)
        }
        );
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }

    continueAction() {
        var _this = this;
        if(this.state.otp.length === 6){
            this.setState({loader:true})
            Keyboard.dismiss()
            this.state.confirmResult.confirm(this.state.otp)
            .then((user) => {
                console.log("otp user faip")
                console.log(user)
                AsyncStorage.setItem('useruid',user.uid)
                _this.checkUser(user)
            })
            .catch(error =>{
                this.setState({loader:false,
                otp:''})
               console.log("Error", error)
               var errorMessage = JSON.stringify(error)
                if(errorMessage.includes('invalid-verification-code')){
                alert("Verification code is incorrect. Please enter correct verification code.")
               }else if(errorMessage.includes("Network error") || errorMessage.includes('auth/unknown')){
                   alert("Please check your phone connection and try again.")
               }
               else{
                alert(error)
               }
            }
            );
        }else{
            alert("Please enter correct otp.")
        }
    }

    checkUser(user){
       
            let phoneNumber = parseInt(this.state.phone)
            var _this = this;

            // firebase.auth().onAuthStateChanged( async (user) => {
                // if(this._isMounted){
             
                // if (user) {
                    firebase.database().goOnline()
                    firebase.firestore().enableNetwork()
                    let doc = firebase.firestore().collection('Users').doc(user.uid);

                    doc.get().then(docSnapshot => {
                   
                    if(docSnapshot.data() === undefined){
                        doctorRef.where('phone', '==', phoneNumber).get()
                        .then(snapshot => {
                         
                            if (snapshot.empty) {
                                console.log("enter into if otp")
                                _this.checkPatientInvitedOrNot(_this.state.phone)
                              }  else{
                                snapshot.forEach(doc => {
                                    console.log("enter into else otp")
                                    console.log("sfhkd jsdhkdfjs", doc.data());
                                    ChatEngineProvider.connect(
                                        _this.state.phone,
                                        doc.data().name
                                    )
                                    AsyncStorage.setItem('phonenumber',_this.state.phone)
                                    AsyncStorage.setItem('doctorname',doc.data().name)
                                    const resetAction = StackActions.reset({
                                        index: 0,
                                        actions: [NavigationActions.navigate({ routeName: 'TabNavigator' })],
                                    });
                                    _this.props.navigation.dispatch(resetAction);
                                  });
                              }
                        });

                    }else{
                console.log("otp else called jfslksdjfldjsfk")
                        AsyncStorage.setItem('patientphonenum',_this.state.phone)
                        _this.setState({loader:false})
                        AsyncStorage.setItem('patientid',user.uid)
                        // ChatEngineProvider.connect(
                        //     _this.state.phone,
                        //     _this.state.phone
                        // )
                        console.log(docSnapshot.data())
                        if(docSnapshot.data().subscription){
                            _this.findDoctorDetails(_this.state.phone)
                        }else{
                            console.log("tos otp called")
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate({ routeName: 'TOS' })],
                                });
                            _this.props.navigation.dispatch(resetAction);
                        }
                    }
                   
                    }, err => {
                    console.log(`Encountered error: ${err}`);
                    });
                // } else {
                //         AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
                //     _this.setState({loader:false})
                // }
            // }else{
            //     return
            // }
        // });               
    }

    checkPatientInvitedOrNot(patientPhoneNo){

        var _this = this;
        AsyncStorage.setItem('patientphonenum',this.state.phone)

        let doc = firebase.firestore().collection('Chatlist').doc(patientPhoneNo);

        doc.get().then(docSnapshot => {
        _this.setState({ loader:false})
            if(docSnapshot.data() === undefined){
               console.log("jflks jj empty")
            alert("Sorry! you can not register at this time. Because no doctor has sent you invitation.")
            }else{
                console.log(docSnapshot.data())
                if(docSnapshot.data().channels.length > 0){
                const resetAction = StackActions.reset({
                             index: 0,
                             actions: [NavigationActions.navigate({ routeName: 'Register' , params:{phoneNumber:patientPhoneNo}})],
                             });
                  _this.props.navigation.dispatch(resetAction);
                }
            }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
    }

    findDoctorDetails(phoneNum){

        var _this = this;
        _this.setState({loader:true})
console.log("findDoctorDetails ...")
        let doc = firebase.firestore().collection('Chatlist').doc(phoneNum);

        doc.get().then(docSnapshot => {
       console.log(docSnapshot )

       _this.setState({ loader:false})
        if(docSnapshot.data() === undefined){
            console.log("jflks jj empty")
            alert("Sorry! you can not register at this time. Because no doctor has sent you invitation.")
            AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
            _this.setState({loader:false})
            _this.props.navigation.navigate('loginNavigationOption')
        }else{     
        }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });    
    }

    setValues(data, phoneNum){

        var _this = this;
        var arrDoc = []
        data.map((item, index)=>{
            if(item.type === "patient"){
               arrDoc.push(item)
            }
            if(index === data.length - 1){
                if(arrDoc.length > 0){
                    var docDetail = arrDoc[0];
                    var newKey = docDetail.channelname.replace(('_'),'')
                    var newKey1 = newKey.replace((phoneNum),'')

                    doctorRef.where('phone', '==', parseInt(newKey1)).get()
                        .then(snapshot => {
                       _this.setState({loader:false})
                       
                        if (snapshot.empty) {
                            console.log('No matching documents.');                                                  
                          }  else{
                            snapshot.forEach(doc => {
                                var docDetailKey = [{'name':doc.data().name, 
                                'phonenum': doc.data().phone, 'image': doc.data().image}]
                                AsyncStorage.setItem('docdetail',JSON.stringify(docDetailKey))
                                _this.props.navigation.navigate('TabNavigatorPatinet');
                              });
                          }
                    });
                }
            }
        })
    }

    render(){

        return(
            <View style={{flex:1, backgroundColor:'white'}}>
                {this.state.loader ? <MyActivityIndicator /> : null}
                <KeyboardAvoidingView behavior={'position'} enabled keyboardVerticalOffset={50}>
                    <View>
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View>
                                <View style={{flexDirection:'row',
                                            height:40, marginTop:40}}>
                                        <BackButton
                                        buttonAction = {()=> this.backButtonAction()}   
                                        image = {require('../../assets/arrow.png')}
                                        />
                                </View>
                                <View style={{alignItems:'center'}}>
                                        <View style={{alignItems:'center'}}>
                                            <Image style={{width:145, height:107, marginTop:0}} source={require('../../assets/logo-green.png')}>
                                            </Image>
                                        </View>
                                        <Text style={{
                                            fontWeight:'bold',
                                            fontSize:18,
                                            marginTop:'5%',
                                            color:'#1B7583'
                                        }}>
                                            Passcode Verification
                                        </Text>
                                        <Text style={{
                                            fontSize:14,
                                            marginTop:'3%',
                                            marginHorizontal:'5%',
                                            textAlign:'center'
                                        }}>
                                            { "Enter Verification Code that we sent on" + " " + this.props.navigation.state.params.phoneNumber}
                                        </Text>
                                        <View style={{marginTop: 15}}>

                                            <OTPInput
                                            value={this.state.otp}
                                            onChange={value => this.setState({otp: value})}
                                            tintColor='gray'
                                            offTintColor='#BBBCBE'
                                            otpLength={6}
                                            />
                                        </View>
                                        <Button
                                            horizontal = '8%'
                                            top = {30}
                                            width = '84%'
                                            radius = {20}
                                            backgColor = '#44A7AF'
                                            height = {40}
                                            textColor = 'white'
                                            titleSize = {16}
                                            title = 'Continue'
                                            buttonAction = {()=>this.continueAction()}
                                        />
                                        <BottomBorderView 
                                            top={30}
                                            width='84%'
                                        />
                                        <Button
                                            horizontal = '8%'
                                            top = {10}
                                            width = '84%'
                                            radius = {0}
                                            backgColor = 'white'
                                            height = {40}
                                            textColor = '#6C6C6C'
                                            titleSize = {16}
                                            title = 'Resend code'
                                            buttonAction = {()=>this.resendCodeAction()}
                                        />
                                </View>
                        </View>
                    </TouchableWithoutFeedback>
                    </View>
                </KeyboardAvoidingView>
            </View>
        )
    }
}