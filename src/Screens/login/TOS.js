
import React, {Component} from 'react';
import {View, Text, ScrollView, StyleSheet, TouchableOpacity, 
    Image, Modal, Dimensions} from 'react-native';

import Button from '../../Components/Button';
import BottomBorderView from '../../Components/BottomBorderView';
import TOSView from '../../Components/TOSView';
import { StackActions, NavigationActions } from 'react-navigation';

const DEVICE_WIDTH = Dimensions.get("window").width;
import Header from '../../Components/Header';

export default class TOS extends Component {

    constructor(){
        super();

        this.state = {
            acceptAgreement:false,
            showModal:false,
            selctedIndex:0,
            TOSTitle:''
        }
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }

    termsOfuseAction = ()  =>{
        this.setState({showModal:true, selctedIndex:0, TOSTitle:'Terms of Use'})
    }

    privacyPolicyAction = ()  =>{
        this.setState({showModal:true, selctedIndex:1, TOSTitle:'Privacy Policy'})
    }

    hippaNoticeAction = ()  =>{
        this.setState({showModal:true, selctedIndex:2, TOSTitle:'HIPPA Notice of Privacy Policies'})
    }

    telehealthConsentction = ()  =>{
        this.setState({showModal:true, selctedIndex:3, TOSTitle:'Telehealth Services Informed Consent'})
    }

    continueAction() {

        if(this.state.acceptAgreement){
            const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'Subscription' })],
                      });
           this.props.navigation.dispatch(resetAction);
        }else{
            alert("Please accept Terms of condition.")
        }
    }

    renderContent() {

        if(this.state.selctedIndex === 0){
            return(
                <Text style={{marginHorizontal:10,
                marginVertical:20,
                textAlign:'justify'}}>{`AEpiphany, its predecessors, licensors, beneficiaries, and its contracted independent entities, including without limitation AEpiphany Doctors and other Licensed Providers (collectively, the “Companies”, “we”, or “us”), operates the website located at AEpiphany.com and mobile applications (collectively, the “Site”).  

By accessing or using the Site and Services you agree to comply with and to be bound by all of the terms and conditions described in these Terms, including the Privacy Policy. If you do not agree to all of these terms and conditions, you are not authorized to use the Site and Services.  The terms “You”,Your”, and “Yourself” refer to the individual user of the Site and Services. 

The Company may revise and update this Agreement at any time. Your continued use of the Site or our Services will indicate your acknowledgement that you accept the revised terms and agree to be bound by the revised Terms of Use and/or Privacy Policy. AEpiphany.com website (the “Website”) has been designed to provide general information about our products and services and the AEpiphany app is designed to provide you with a secure electronic means to communicate with your doctor, therapist, 
or other licensed provider to receive telehealth services. You are responsible for all equipment necessary to appropriately access the Site.

In order to use AEpiphany your provider has to be registered with us and send you an invitation, and once you have accepted the invitation to connect you can begin receiving telehealth services from your provider.  AEpiphany does not recommend or endorse any specific healthcare service or service providers. 


`}

{`  Send or otherwise transmit to or through the sites any unlawful, infringing, harmful, harassing, defamatory, threatening, hateful, or otherwise objectionable material of any kind. 

Send or otherwise transmit any material that can cause harm or delay to the sites or computers of any kind, and any unsolicited advertising, solicitation or promotional materials.

Misrepresent your identity or affiliation in any way.
   
Attempt to obtain personal information, or collect information about users of the Site.

Gain unauthorized access to the Sites, to other users' accounts, names, personally identifiable information or other information, or to other computers or websites connected or linked to the Site. 
  
Reverse engineer or otherwise attempt to discover the source code of any portion of the Site, interfere with the operation of the Site including a) uploading or otherwise disseminating virus, adware, spyware, worm, or other malicious code; b) interfering with or disrupting any network, equipment, or server.   

Create more than one account or create an account for anyone other than yourself without first receiving permission from the other person.

Impersonating another person. You may never use another person’s sign-in details to access the Site and/or Services.


`}


<Text style={{color: '#2F60CE'}}>Intellectual Property Ownership and Limitations of Use</Text>

{`
All materials on the Sites, including the design, layout, and organization (collectively referred to as “Content”), are owned and copyrighted by AEpiphany or its suppliers or vendors, and are protected by all applicable intellectual property laws. The Content contains trademarks, service marks, and registered trademarks of AEpiphany and its licensors. All rights and title to the Content, trademarks and service marks herein remain with AEpiphany or its licensors.

You are authorized to view the site and Content. You are not authorized to alter, obscure, or remove any copyright or trademark notice present in the Content. All rights not expressly granted herein are reserved to AEpiphany.

You may not reproduce, duplicate, copy, sell, trade, resell, or exploit for commercial gain any portion of the Sites or the Content. You agree not to change or delete any copyright or proprietary notice from materials downloaded from this site or any site accessible through this site. You may not link to this site.


`}
<Text style={{color: '#2F60CE'}}>Eligibility</Text>

{`
You hereby certify that you are over the age of 18 and have the legal ability to consent to medical or mental health treatment. 
 
To request services for a minor patient, you must be the parent or legal guardian, or an individual with uncontested legal authority to arrange for and consent to the treatment of the patient. Parent or legal guardian must provide consent to the treatment of the minor patient prior to availing these services for a minor. An unemancipated minor (under 18 years old) has no confidentiality from their parents or guardian unless otherwise specified in a formal agreement. AEpiphany does not seek through this site to gather personal information from or about persons under the age of 13 without the consent of a parent or guardian. 
 
You acknowledge that your ability to access and use the Service is conditioned upon the truthfulness of the information you provide. Providers you access via this Service are relying upon this certification in order to interact with you and provide the Services. 

We reserve the right to refuse or cancel your registration or your use of the Site and/or the Services, if we determine that you have not provided complete and accurate information regarding your identity and/or have otherwise not provided accurate information regarding your age, residence, and contact Information.

You agree that you will notify your Provider of any change in your eligibility to use the Services.  Additionally, you agree to notify us immediately of any known or suspected breach of security or unauthorized use of your registration with AEpiphany.  
  
`}
<Text style={{color: '#2F60CE'}}>Providers</Text>
 
{`\nAll Healthcare Professionals providing telehealth Services to Patients via AEpiphany app are required to be licensed and in current good standing with the applicable state licensing agency in the states where they practice. 
 
Providers are solely responsible for determining the suitability for telehealth services for their clients prior to initiating services via AEpiphany app.

The laws and professional standards that apply to in-person medical and/or mental health services also apply to telehealth services. Healthcare Providers are responsible for a) knowing and complying with laws and regulations required by their state laws and their licensing boards b) for maintaining and complying with the requirements for their malpractice and liability insurance. 

Reliance on any information provided by any Provider on or through the site or services is solely at your own risk. You agree not to hold AEpiphany liable in any way for any malpractice or unsatisfactory treatment the health care Provider may provide to you or any minor for whom you are responsible.   AEpihany makes no representations or warranties as to the conduct, ability or the efficacy, accuracy, completeness, timeliness, or relevance of the information provided by said Providers or by third parties featured on or through the Site or services.   


`}
<Text style={{color: '#2F60CE'}}>Payments</Text>
{`
You acknowledge and agree that you shall be personally responsible for all incurred fees, charges, and expenses for the services you receive via this site. 

Any payments collected by AEpiphany are used to compensate AEpiphany for its software development, overhead, administrative services and other corporate costs and fees, including transaction fees for credit card usage.   
 
Providers using AEpiphany app to provide telehealth services compensate for the use of the AEpiphany technology and for administrative fees. Part of your payment for the use of the Services, accounted for separately, is remitted to the Provider for the direct clinical services provided to you. AEpiphany does not participate, split, or take a percentage of this payment to the Provider. Regardless of any paymentments made, AEpiphany does not hold itself out as your direct provider of clinical services as that is the role of your licensed Provider. 

You agree to pay all fees or charges to your account in accordance with the fees, charges, and billing terms in effect at the time a fee or charge is due and payable. When you register on the Site, you are required to create an account by entering your name, email address, and phone number. When registering an account, we will request your credit card information. This information will be used to bill you. Your credit card or other transaction data are retained using bank-level encryption so that AEpiphany can charge you monthly. AEpiphany charges your card before the beginning of the next month’s service.  

You understand that if the payment on your account is delinquent this will result in disruption to telehealth services via AEpiphany app. Your Provider will be notified of this disruption in services immediately. Disruption of services via this Site does not preclude your ability to contact and/or receive services from your provider via phone, email, or other methods you and your provider have previously agreed upon.   


`}
<Text style={{color: '#2F60CE'}}>Cancellation of Services</Text>
{`
You can choose to terminate your subscription at your discretion, all you have to do is text or call your Provider and inform them that you would like to discontinue your subscription. Your Provider will inform us of your request to cancel services, and we will stop charging your card for the next billing cycle. You may continue to utilize the services until the end of your current billing cycle.  


`}
<Text style={{color: '#2F60CE'}}>Indemnification</Text>
{`
You agree to defend, indemnify, and to hold harmless the Companies, together with its officers and directors, from any and all liabilities, penalties, claims, causes of action, and demands brought by third-parties (including the costs, expenses and attorneys’ fees on account thereof) arising, resulting from or relating to: (a) your use of the Site or Services  or your inability to use the Site or Services; (b) an allegation that you violated any representation, warranty, covenant or condition in this Agreement; (c) your intentional or negligent misrepresentation or misuse confidential or protected information. Your agreement to defend, to indemnify, and to hold the Companies (and their officers and directors) harmless applies whether any claim against the Companies is based in allegations of violation(s) of law or contract or tort (including strict liability), and regardless of the form of action, including but not limited to your violation of any third-party rights, a claim that the Site and/or Services  caused damage to you or to any third party and/or your use and access to the Site and/or Services. This indemnification section shall survive your termination of or cessation of use of the Site and Services.`}  

                </Text>
            )
        }else if(this.state.selctedIndex === 1){
            return(
                <Text style={{marginHorizontal:10,
                    marginVertical:20,
                    textAlign:'justify'}}>
    {`Our Privacy Policy is expressly incorporated into the Terms of Use and other  agreements on this site by this reference. By continuing to use the Site and services and by providing your personal information to us, you agree to the practices and policies described in this Privacy Policy. 
    
    
`}
<Text style={{color: '#2F60CE'}}>Information You Give Us</Text>
    {`
We collect your personal health information (PHI) such as name, email address, phone number, and credit card information for payment, only when you voluntarily give it to us to register for an account and subscribe to our Services. 
We will not use any PHI for any other purpose without your written authorization, or unless otherwise permitted or required by law. You may revoke, in writing, any such authorization at any time, except to the extent we have taken action in reliance thereon.
     
We do not knowingly enroll directly or collect information directly from children under the age of 18. 
     
If you contact us about our Site or services, we may retain the content of your email, your email address and our response to you. We retain this information for a period of time to resolve disputes, troubleshoot problems, or for other valid business or legal reasons.  
     
`}
<Text style={{color: '#2F60CE'}}>Other Information We Collect</Text>
{`
Images, videos, or audio recordings will only be collected from your device if you voluntarily select it to upload to the application to store or share with your Provider or other users within the Site. Multimedia will not be shared with other Site users without your consent (with the exception of your profile photo, to the extent such feature is offered, which will appear in your user profile).
     
We may use mobile application tracking and/or analytics services. These services may record unique mobile gestures such as tap, double-tap, zoom, pinch, scroll, swipe and tilt but do not collect personally identifiable information that you do not voluntarily enter in the Site. These services do not track your browsing habits across mobile applications that do not use the same services. We are using the information collected by these services to understand user behavior and optimize site performance.

`}
<Text style={{color: '#2F60CE'}}>Sharing of Information</Text>
{`
We do not sell or rent your personal information to any other third party. We only share Personal Information with third parties who are bound by terms at least as restrictive as this Privacy Policy and only for the following reasons: 
    
We share Personal information with third-party service providers who perform a variety of functions to enable us to provide our services. These service providers only have access to the Personal Information necessary to perform their functions, only act on our behalf and under our instructions, and may not use this information for any other reasons aside to perform such functions.    
    
We may disclose Personal information to courts, law enforcement agencies and other government bodies if we suspect that an individual’s use of our Site or services involves illegal activity or is otherwise inconsistent with any applicable terms and conditions. We may also disclose Personal Information where we believe that doing so would be in accordance with or required by any applicable law, regulations, or legal process.  
    
We may transfer any or all Personal information in the event of a corporate transaction, such as the sale of all or a portion of our business, a divestiture, merger, consolidation, or asset sale, or in the event of bankruptcy, as required or allowed by law.

`}
<Text style={{color: '#2F60CE'}}>Protecting your Information</Text>
{`
We secure information by using reasonable administrative, physical, and technical safeguards. All messages are end to end encrypted. We will store user profiles on controlled servers with restricted access, but we do not store any messages transmitted between patients and providers. 
    
In order to protect your confidentiality, all messages are maintained only on the end devices (e.g., your cell phone and your provider’s cell phone). In the event that you lose your device we are unable to provide you a copy of your message; WE STRONGLY RECOMMEND THAT YOU BACK UP YOUR MESSAGES REGULARLY. 
    
Please be advised that despite using these current technical and industry recommended practices, it is never possible to fully guarantee against breaches in security.  
     
In the unlikely event of a data breach, you will be notified as soon as reasonably possible, in accordance with applicable law. Furthermore, we are not responsible for any breach of security or for any actions of any third parties that receive the information.
    
`}
<Text style={{color: '#2F60CE'}}>Links to Other Sites</Text>
{`

Our Site may contain links to external sites or services. AEpiphany does not maintain these sites and is not responsible for the privacy practices of sites or services it does not operate. You should consult the respective privacy policies posted on these sites. 

`}
<Text style={{color: '#2F60CE'}}>Account Security</Text>
{`
It is your responsibility to maintain privacy on the client end of communication. You shall at all times remain responsible for maintaining the confidentiality of your account password and username if any and any other security information related to your account. Your Provider will not be liable for any loss that you  incur as a result of someone else accessing and using your account, either with or without your knowledge.`
    }
    
                </Text>
            )
        }else if(this.state.selctedIndex === 2){
            return(
                <Text style={{marginHorizontal:10,
                    marginVertical:20,
                    textAlign:'justify'}}>
{
`This notice describes how health information about you may be used and disclosed and how you can get access to this information. Please review it carefully. 

Please note that the only information we collect and store for individuals using our Site and services is name, phone numbers, email addresses, and credit card information for payments.  We do not access or store your medical records or communications with your Provider.  All pertinent health and psychological information between Providers and their clients is stored on your cell phone.  Providers and clients can back this data up to a location of their choosing and/or delete this data at anytime. Each Provider is required to maintain health records according to his/her licensing requirements; please talk to your Provider directly to learn more about their record storage practices. 

To obtain additional information or to file a complaint you can call 1-877-696-6775, or visiting https://www.hhs.gov/hipaa/for-individuals/index.html.  


`}
<Text style={{color: '#2F60CE'}}>Our Responsibility</Text>
{`
AEpiphany is required by the Health Insurance Portability and Accountability Act of 1996 (HIPPA) to maintain the privacy and security of your personal health information (“Protected Health Information”). We will inform you if a breach occurs that may have compromised the privacy or security of your information. We will not use or share your information other than as described in our “Web and Mobile Privacy Policy.”    

We reserve the right to make changes to our Terms of Use and/or Privacy Policies, and the changes will apply to all the information we have about you. The new notice will be available on our website and via the AEpiphany app.  

AEpiphany corporation may use and disclose your PHI for the following purposes: 

To create an account for you on our platform that allows you to receive telehealth services from your Provider. 

To bill to and receive payments from you, a provider, or a third party for the utilization of our platform.
For healthcare operations such as activities necessary to support, operate, and improve AEpiphany’s Site and services.
To coordinate delivery of services through other companies or individuals known as “Business Associates,” who provide services to us. Business Associates are required to protect the privacy and security of your Protected Health Information and notify us if any improper disclosure of information.  
We may have to disclose your PHI as required by law, by any applicable federal, state or local law. 


`}
<Text style={{color: '#2F60CE'}}>All Other uses and Disclosures of Protected Health Information</Text>
{`
We will ask for your written authorization before using or disclosing your PHI for any purpose not described above. You may revoke your authorization, in writing, at any time except for disclosures that the company has already acted upon or are required by law. We will never sell or give your personal identifying information to third party marketers. 

We at AEpiphany corporation do not have access to the contents of your communication with your providers. All data is end to end encrypted both at rest and while being transmitted. We will never share or read your psychotherapy and/or medical notes. We do not store your data in the cloud; the decision about where your psychotherapy or medical notes are stored is between you and your provider.  We are a technology platform that transmits encrypted messages, but we do not read or store these messages.    


`}
<Text style={{color: '#2F60CE'}}>Your Right to Access Protected Health Information</Text>
{`
You, or your authorized or designated personal representative, have the right to inspect or copy your Protected Health Information. As stated above, aside from your name, phone number, email information, and credit card information for payments we do not maintain health records. All of your treatment notes and health records are maintained by your provider. You can submit a direct request to your Provider’s office to obtain or copy your health information/records.  

You have the right to correct or update information about yourself if you believe the PHI we maintain about you contains an error, you may request that we correct or update your information. Your request must be in writing and must explain why the information should be corrected or updated. We may deny you request under certain circumstances and provide a written explanation. 

You may request a list, or accounting, of certain disclosures of your PHI made by us or our business associates for purposes other than payment, healthcare operations and certain other activities. The request must be in writing and state a time period, which may not be longer than the prior six years.   

`}
<Text style={{color: '#2F60CE'}}>Information Breach Notification:</Text>
{` We are required to notify you following the discovery of a breach of your unsecured PHI, unless there is a demonstration, based on a risk assessment, that there is a “low probability” that the PHI has been compromised. You will be notified in a timely fashion, no later than 60 days after discovery of the breach.  

Questions and Complaints: If you have questions or concerns about our privacy practices or would like a more detailed explanation about your privacy rights, please contact us at Team@aepiphany.com 

If you believe that we may have violated your privacy rights, you may submit a complaint to us. You also may submit a written complaint to the U.S. Department of Health and Human Services. We will provide you with the address to file your complaint with the U.S. Department of Health and Human Services upon request. AEpiphany corporation will not take retaliatory action against you and you will not be penalized in any way if you choose to file a complaint with us or with the U.S. Department of Health and Human Services.  

Change to this Notice: We reserve the right to change our privacy practices and the terms of use at any time, provided such changes are permitted by applicable law. We will promptly post any changes to the Notice on our website or otherwise provide them to you.`
}

                    </Text>
            )
        }else{
            return(
                <Text style={{marginHorizontal:10,
                    marginVertical:20,
                    textAlign:'justify'}}>
                        <Text style={{fontWeight:'bold', textAlign:'center'}}>
                        Do not use this or any other telehealth service for emergency medical or psychiatric needs. If you experience a medical or psychiatric emergency call 911 immediately or go to your nearest emergency hospital or Urgent Care Center for an evaluation. 

                        </Text>


{
    
`

As a client receiving health (including psychological) services  through telehealth methods, you understand:

1) This service is provided by technology (limited to video, phone, text, and email) and may not involve direct, face to face, communication. There are benefits and limitations to this service. You will need access to, and familiarity with, the appropriate technology to participate in the service provided. Exchange of information will not be direct and any paperwork exchanged will likely be exchanged through electronic means. 

2) If a need for direct, face to face services arises, it is your responsibility to contact the appropriate providers in your area, or to contact your Provider’s office for a face to face appointment. You understand that an opening may not be immediately available.

3) These services rely on technology, which allows for greater access and convenience in availing service delivery. There are risks in transmitting information over the internet that include, but are not limited to, breaches of confidentiality, theft of personal information, and disruption of service due to technical difficulties. 

4) The laws and professional standards that apply to in-person psychological or medical services also apply to telehealth services. Providers in  the State of California are required by state law to recognize intent to harm self or another person and act to both inform the potential victim and local police.Providers are also obligated to report acts of abuse or neglect of a child or vulnerable adult to Child Protective Services or Adult Protective Services respectively. An unemancipated minor (under 18 years old) has no confidentiality from their parents or guardian unless otherwise specified in a formal agreement.          

5) The privacy of medical records is further specified by a federal law entitled the Health Information Portability and Accountability act of 1996 (HIPPA) insuring medical records are maintained in a secure form, available for reviews and amendment by patients upon their written request and reason. With some exceptions, all protected health information (PHI) acquired in the course of evaluation and treatment is confidential unless specific written authorization is given by the patient or the patient’s legal guardian or conservator to disclose.  This authorization must specify a verifiable recipient, how PHI will be used, and an expiration date. An authorization may be revoked in writing at any time.
The disclosure of PHI may be made without authorization: in efforts to notify significant others such as family or friends critical to patient’s welfare of the location, general condition, or death of a patient. In such an emergency, PHI will be released on a minimum necessary basis in your provide’rs judgment.

Other prominent exceptions include the release of PHI:
a) Legal proceedings when ordered by a judge
b) Authorized federal officials for national security purposes
c) Compliance with the state Workers’ Compensation law.
 PHI safeguards include reasonable physical and electronic security of the device on which the services will be provided; including password mediated access.  Upon written request, patients have the right to review their records copied in a timely fashion.

6) Your provider and you will predetermine a schedule for responding  to communications and routine messages via text messaging.  Real time chat and/or video conference will need to be prearranged and scheduled with your  provider ahead of time for a set time and date. AEpiphany corporation is NOT responsible or liable for your satisfaction or dissatisfaction  with the quality of care provided by your Provider.

7) It is your responsibility to maintain privacy on the client end of communication. You shall at all times remain responsible for maintaining the confidentiality of your account password and username if any and any other security information related to your account. Your provider or AEpiphany corporation will not be liable for any loss that you incur as a result of someone else accessing and using your  account, either with or without your knowledge.

8) In the event of disruption of service, or for routine or administrative reasons, it may be necessary to communicate by other means:

  a) Should service be disrupted due to technology related challenges such as loss of internet or mobile phone connection please call the administrative phone number at your provider’s office for all non-urgent matters. FOR all emergencies please call 911. 

`
}
                </Text>
            )
        }
    }

    render() {

        return(
            <View style={{flex:1, backgroundColor:'white'}}>
                 {
                        this.state.showModal ? (
                            <Modal animationType="fade" transparent={true} visible= {true}>
                                    <View style={{
                                            height:'100%',
                                            alignItems: 'center',
                                            backgroundColor: 'rgba(0,0,0,0.60)'
                                        }}>
                                            <View style={{
                                               marginTop:40, 
                                                flexDirection:'column', 
                                                backgroundColor:'white',
                                                height:40,
                                                width:DEVICE_WIDTH-40,
                                                alignItems:'center'}}>
                                                <TouchableOpacity style={{ 
                                                    position:'absolute', 
                                                    right:5,  
                                                    marginTop:10}} 
                                                    onPress={()=> this.setState({showModal:false})}>
                                                                <Image source={require('../../assets/Close.png')} />
                                                </TouchableOpacity>
                                                <Text style={{
                                                    marginTop:15,
                                                    height:25,
                                                    fontSize:15,
                                                    textAlign:'center'}}>
                                                        
                                                    {this.state.TOSTitle}
                                                    </Text>
                                        </View>
                                        <BottomBorderView 
                                            horizontal={20}
                                            />
                                       
                                        <ScrollView style={{flex:1, backgroundColor:'white', 
                                            marginHorizontal:20, marginBottom:30}}>
                                                
                                                {
                                                    this.renderContent()
                                                }
                                        </ScrollView>
                                           
                                    </View>
                            </Modal> 

                       ) : (null)
                }

                <Header title={'Terms of conditions'} type={'patient'}
                    />
                <BottomBorderView 
                    horizontal={0}
                    top={0}
                    />
                <ScrollView style={styles.mainContainer}>

                    <TOSView
                    title='Terms of Use'
                    action={this.termsOfuseAction.bind(this)}
                    detailText="Purpose of this document is to prevent misuse or abuse of our services. It 
                    governs our right to suspend, ban, or stop providing our services if policies or terms 
                    of use are not followed, or if we are investigating suspected misconduct."
                    />

                    <TOSView
                    title='Privacy Policy'
                    action={this.privacyPolicyAction.bind(this)}
                    detailText="Purpose of this document is to describe our practices and policies regarding 
                    how we collect, use, store, and disclose personal information provided to us through the 
                    website and mobile application."
                    />

                    <TOSView
                    title='HIPPA Notice'
                    action={this.hippaNoticeAction.bind(this)}
                    detailText="Purpose of this notice is to describe your rights over your health information 
                    and rules and limits on who can look at and receive your health information according to the 
                    Federal laws."
                    />
                    
                    <TOSView
                    title='Telehealth Consent'
                    action={this.telehealthConsentction.bind(this)}
                    detailText="Purpose of this consent is to inform our users of the benefits and limitations 
                    of using electronic means for communicating with your providers."
                    />

                       <Text style={{
                           marginTop:20,
                            marginHorizontal:20,
                            fontSize:13,
                        }}>
                            Please read these Terms and Policies carefully, as they govern and apply to your access and use of the Site and Services available through AEpiphany Inc. 

                        </Text>
                    <View style={{
                        flexDirection:'row',
                        marginHorizontal:'5%',
                    alignItems:'center',
                    marginTop:30
                    }}>
                        <TouchableOpacity style={{

                        }} onPress={()=>this.setState({acceptAgreement:!this.state.acceptAgreement})}>
                            {
                                this.state.acceptAgreement ? (
                                    <Image style={{width:18, height:18}} source={require('../../assets/check-square.png')}>
                                    </Image>
                                ) : (
                                    <Image style={{width:18, height:18}} source={require('../../assets/check_box.png')}>
                                    </Image>
                                )
                            }
                            
                        </TouchableOpacity>

                        <Text style={{
                            marginLeft:10,
                            fontSize:10,
                            color:'gray'
                        }}>
                            "I have read and understand the information provided above and agree to these contracts."
                        </Text>
                    </View>
                    <Button
                        horizontal = '8%'
                        top = {60}
                        radius = {20}
                        backgColor = '#44A7AF'
                        height = {40}
                        textColor = 'white'
                        titleSize = {16}
                        title = 'Continue'
                        bottom = {40}
                        buttonAction = {()=>this.continueAction()}
                    />
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer:{flex:1,backgroundColor:'white'},
});