

import React,{Component} from 'react';
import {View, Text, Image, StyleSheet,Dimensions, Modal,TouchableOpacity,
    KeyboardAvoidingView, TextInput,ScrollView
    } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { StackActions, NavigationActions } from 'react-navigation';
import Button from '../../Components/Button';
import BottomBorderView from '../../Components/BottomBorderView';
import MyActivityIndicator from '../../Components/activity_indicator';
import Header from '../../Components/Header';

const DEVICE_WIDTH = Dimensions.get("window").width;

export default class PaymentDetail extends Component {

    constructor(){
        super();
        this.state = {
            form: {},
            loader:false,
            showModal:false,
            disable:true,
            zipCode:'',
            patientid:'',
            cardNumber:'',
            cardCVC:'',
            expiryMonth:'',
            expiryYear:''
        }
    }
    
    componentWillMount(){
        AsyncStorage.getItem('patientid').then((patientid)=>{
            this.setState({patientid:patientid})
        })
    }
   
    backButtonAction() {
        const resetAction = StackActions.reset({
                         index: 0,
              actions: [NavigationActions.navigate({ routeName: 'Subscription' })],
                                  });
         this.props.navigation.dispatch(resetAction);
    }

   async paymentThroughFirebase(token){

    this.setState({loader: true});

    let body = {
        "token": token,
        "doctor_phone": this.props.navigation.state.params.doctorPhoneNum,
        "user_id": this.state.patientid
    };

    try {
        fetch('https://us-central1-aepiphany-bdba7.cloudfunctions.net/charge/charge', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        }) .then(response => response.json())
        .then(responseJson => {
           this.setState({loader: false});
            console.log("response firebase payment")
            console.log(responseJson)
            if(responseJson.statusCode === 200){
                this.setState({showModal:true})
            }
            else{
                alert(responseJson)
            }
        })
        .catch(e => {
           this.setState({loader: false});
           console.log("error ...",e)
        }); 
       }catch (error) {
        console.log("error kjjl", error)
       }
   }

    completePaymentAction = async ()=>{

        if(this.state.cardNumber.length < 16){
            alert("Please enter correct card number.")
        }else if(this.state.expiryMonth.length < 2){
            alert("Please enter correct expiry month.")
        }else if(this.state.expiryYear.length < 4){
            alert("Please enter correct expiry year.")
        }else if(this.state.cardCVC.length < 3){
            alert("Please enter correct cvc number.")
        }else{
            this.setState({loader: true});
            let body = {
                "card[number]": this.state.cardNumber,
                "card[exp_month]": this.state.expiryMonth,
                "card[exp_year]": this.state.expiryYear,
                "card[cvc]": this.state.cardCVC
            };
            console.log("Stripe body")
            console.log(body)
            let formBody = [];
            for (let property in body) {
                let encodedKey = (property);
                let encodedValue = (body[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");
            try {
             fetch('https://api.stripe.com/v1/tokens', {
                 method: 'post',
                 headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer sk_test_SX1zYiU329Paa6uAiASqLfO7',
                 },
                 body: formBody
             }) .then(response => response.json())
             .then(responseJson => {
                this.setState({loader: false});
                 console.log("response stripe payment")
                 console.log(responseJson)
                 if(responseJson.error){
                     alert(responseJson.error.message)
                 }else{
                    console.log("stripeToken", responseJson.id)
                    this.paymentThroughFirebase(responseJson.id)
                 }
             })
             .catch(e => {
                this.setState({loader: false});
                console.log("error ...",e)
             });
            }catch (error) {
             // Error retrieving data
             console.log("error kjjl", error)
            }
        }
    }

    okAction = async ()=>{

        this.setState({showModal:false})
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'TabNavigatorPatinet' })],
        });
        this.props.navigation.dispatch(resetAction);
    }

    async onChange(form) {
        if (form.valid) {
            this.setState({form: form.values, disable: false})

        } else {
            this.setState({disable: true})
        }
    }

    handleCardNumber = (text) => {
        let formattedText = text.split(' ').join('');
        if (formattedText.length > 0) {
          formattedText = formattedText.match(new RegExp('.{1,4}', 'g')).join(' ');
        }
        this.setState({ cardNumber: formattedText });
      }

      handleExpiryMonth = (text) => {
        if(parseInt(text) > 12){
            this.setState({expiryMonth:''})
        }else{
            this.setState({expiryMonth:text})
        }
      }

    render(){
        const navigate = this.props.navigation.navigate;

        return(
            <View style={{flex:1, backgroundColor:'#F5F3F8'}}>
                 
                 {
                        this.state.showModal ? (
                            <Modal animationType="fade" transparent={true} visible= {true}>
                                <View style={{
                                        height:'100%',
                                        alignItems: 'center',
                                        backgroundColor: 'rgba(0,0,0,0.60)'
                                    }}>
                    
                                    <View style={{
                                         marginTop:100, 
                                         marginHorizontal:'7%', 
                                         flexDirection:'column', 
                                         backgroundColor:'white',
                                         borderRadius:5,
                                         width:'86%',
                                         alignItems:'center'}}>
                                        <TouchableOpacity style={{ 
                                            position:'absolute', 
                                            right:15,  
                                            marginTop:20}} 
                                            onPress={()=> this.setState({showModal:false})}>
                                                        <Image source={require('../../assets/Close.png')} />
                                        </TouchableOpacity>
                                        <BottomBorderView 
                                            horizontal={10}
                                            width='100%'
                                            top={50}
                                            />
                                        <Image style={{
                                            marginVertical:20
                                        }} source={require('../../assets/check-simple.png')}>
                                        </Image>
                                            <Text style={{fontSize:19, marginHorizontal:20}}>
                                            Payment Successfull
                                            </Text>
                                            <Text style={{color:'gray', fontSize:15, marginHorizontal:20, marginVertical:20}}>
                                            We sent you a confirmation email with your order details. Thank you!
                                            </Text>
                                            <Button
                                                top = {40}
                                                radius = {20}
                                                backgColor = '#44A7AF'
                                                height = {40}
                                                textColor = 'white'
                                                titleSize = {16}
                                                title = 'Ok'
                                                width='80%'
                                                bottom = {20}
                                                buttonAction = {()=>this.okAction()}
                                            />
                                    </View>
                                          
                                </View>
                            </Modal> 

                       ) : (null)
                    }
                {this.state.loader ? <MyActivityIndicator /> : null}
                <Header title={'Payment details'} withBackButton=
                    {() => this.backButtonAction()} type={'patient'} 
                        navigation={navigate}
                        />
                    <BottomBorderView 
                        horizontal={0}
                        top={0}
                        />
         <ScrollView style={styles.mainContainer}>
                    <KeyboardAvoidingView behavior={'position'}>
                    <View style={{
                        backgroundColor:'white'
                    }}>
                         <Text style={{
                            marginLeft:20,
                            fontSize:18,
                            marginTop:20
                            }}>
                            Summary
                        </Text>
                        <View style={{
                            flexDirection:'row',
                            marginHorizontal:40,
                            marginTop:20,
                            
                        }}>
                            <Text style={{
                                width:100
                            }}>
                                Total amount
                            </Text>
                            <Text style={{
                                width:DEVICE_WIDTH-200,
                                textAlign:'right',
                                color:'#44A7AF',
                                fontSize:17,
                                fontWeight:'600'
                            }}>
                                $ {this.props.navigation.state.params.amount}
                            </Text>
                        </View>
                        <BottomBorderView 
                        horizontal={40}
                        top={5}
                        bottom={10}
                        />
                    </View>

                    <View style={{ marginVertical: 20, marginHorizontal: 10, flex:1}}>
                        <View style={[styles.Card, { flex: 1 }]}>
                            <View style={{ marginHorizontal:10, flex: 1, marginVertical:20 }}>
                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center'}}>
                                    <Text style={{ }}>CARD NUMBER</Text>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', 
                                marginTop:5}}>
                                    <TextInput
                                        style={{ flex: 1, backgroundColor:'lightgray', height: 40, borderRadius: 5, paddingLeft: 10, fontSize:16, alignItems:"flex-start", color:'black', marginLeft: 10}} 
                                        placeholder ="**** **** **** 0000"
                                        keyboardType={'phone-pad'}
                                        maxLength={19}
                                        value={this.state.cardNumber}
                                        onChangeText={(cardNumber) => this.handleCardNumber(cardNumber)}
                                    />
                                </View>
                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center',marginTop:10}}>
                                    <View style={{ flex: 3}}>
                                    <Text style={{ }}>EXPIRATION DATE</Text>
                                    </View>
                                    <View style={{ flex: 1}}>
                                    <Text style={{ }}>CVC</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent:'space-around',
                            marginTop:5}}>
                                    <View style={{flexDirection: 'row', backgroundColor:'lightgray', height: 40, borderRadius: 5, paddingHorizontal: 10, alignItems:'center', justifyContent:'space-around'  }}>
                                        <TextInput 
                                        style={{textAlign:'center', width:70}}
                                        placeholder ="MM"
                                        keyboardType={'phone-pad'}
                                        maxLength={2}
                                        value={this.state.expiryMonth}
                                        onChangeText={(month) => this.handleExpiryMonth(month)}
                                        />
                                    </View>
                                    <View style={{flexDirection: 'row', backgroundColor:'lightgray', height: 40, borderRadius: 5, paddingHorizontal: 10, alignItems:'center', justifyContent:'space-around'  }}>
                                    <TextInput 
                                       style={{textAlign:'center', width:80}}
                                       placeholder ="YYYY"
                                       keyboardType={'phone-pad'}
                                       maxLength={4}
                                       value={this.state.expiryYear}
                                       onChangeText={(year) => this.setState({expiryYear:year})}
                                     />
                                    </View>
                                    <View style={{flexDirection: 'row', backgroundColor:'lightgray', height: 40, width: 80, borderRadius: 5, paddingHorizontal: 10, alignItems:'center'}}>
                                    <TextInput
                                        style={{ flex: 1, backgroundColor:'lightgray', height: 40, borderRadius: 5, paddingLeft: 10, fontSize:16, alignItems:"flex-start", color:'black', marginLeft: 10}} 
                                        placeholder ="***"
                                        keyboardType={'phone-pad'}
                                        maxLength={3}
                                        secureTextEntry={true}
                                        value={this.state.cardCVC}
                                        onChangeText={(cvc) => this.setState({cardCVC:cvc})}
                                    />
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                           
                     <Button
                        horizontal = '8%'
                        top = {40}
                        radius = {20}
                        backgColor = '#44A7AF'
                        height = {40}
                        textColor = 'white'
                        titleSize = {16}
                        title = 'Complete payment'
                        bottom={20}
                        buttonAction = {()=>this.completePaymentAction()}
                    />
                     </KeyboardAvoidingView>
                    </ScrollView>
               
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer:{flex:1,backgroundColor:'white'},
    Card:{
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        shadowColor: '#451B2D',
        shadowOffset: {width: 0, height: 9},
        shadowOpacity: 0.18,
        shadowRadius: 21
    }
});