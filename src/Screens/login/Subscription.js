

import React,{Component} from 'react';
import {View, Text, Image, ScrollView, StyleSheet, Platform
    } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Button from '../../Components/Button';
import BottomBorderView from '../../Components/BottomBorderView';
import MyActivityIndicator from '../../Components/activity_indicator';
import * as firebase from 'react-native-firebase';
import ChatEngineProvider from "../../lib/pubnub-chatengine";
import { StackActions, NavigationActions } from 'react-navigation';

const doctorRef = firebase.firestore().collection('Doctors');

export default class Subscription extends Component {

    constructor(){
        super();
        this.state = {
            arrSpecialities:[],
            doctorName:'',
            speciality:"",
            doctorPhoneNum:'',
            image:'',
            amount:'',
            loader:false,
            patientPhoneNo:''
        }
    }

    componentWillMount(){

        var _this = this;

        AsyncStorage.getItem('patientphonenum').then((phoneNum)=>{
            _this.setState({loader:true, patientPhoneNo:phoneNum})

            let doc = firebase.firestore().collection('Chatlist').doc(phoneNum);

            doc.get().then(docSnapshot => {
          
               _this.setState({ loader:false})
                if(docSnapshot.data() === undefined){
                    alert("Sorry! you can not register at this time. Because no doctor has sent you invitation.")
                    AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
                    _this.setState({loader:false})
                    _this.props.navigation.navigate('loginNavigationOption')
                }else{
                    if(docSnapshot.data().channels.length > 0){
                    _this.setValues(docSnapshot.data().channels, phoneNum)
                    }
                }
            }, err => {
            console.log(`Encountered error: ${err}`);
            });
        })
    }

    setValues(data, phoneNum){

        var _this = this;
        var arrDoc = []
         data.map((item, index)=>{
             if(item.type === "patient"){
                arrDoc.push(item)
             }
 
             if(index === data.length - 1){
                 if(arrDoc.length > 0){
                     var docDetail = arrDoc[0];
                     var newKey = docDetail.channelname.replace(('_'),'')
                     var newKey1 = newKey.replace((phoneNum),'')
 
                     doctorRef.where('phone', '==', parseInt(newKey1)).get()
                        .then(snapshot => {
                         
                            if (snapshot.empty) {
                                console.log('No matching documents.');                                                  
                              }  else{
                                snapshot.forEach(doc => {
                                    _this.setState({arrSpecialities:doc.data().services,
                                        doctorName:doc.data().name,
                                        speciality:doc.data().speciality,
                                        image:doc.data().image,
                                        amount:doc.data().amount,
                                        doctorPhoneNum:doc.data().phone})
                                    ChatEngineProvider.connect(
                                                phoneNum,
                                                phoneNum
                                            )
                                  });
                              }
                        });
                 }
             }
         })
    }

    subscribeMonthlyAction() {

        var docDetailKey = [{'name':this.state.doctorName, 
        'phonenum': this.state.doctorPhoneNum, 'image': this.state.image}]
        AsyncStorage.setItem('docdetail',JSON.stringify(docDetailKey))

        const resetAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: 'PaymentDetail' , 
                  params:{amount:this.state.amount, 
                    doctorPhoneNum:this.state.doctorPhoneNum}})]
                });
       this.props.navigation.dispatch(resetAction);
    }

    backButtonAction() {
        this.props.navigation.goBack()
    }

    async logoutAction() {
        AsyncStorage.getItem('token').then((token)=>{

            console.log("token /////", token)
            if(token !== null){
                var deviceType=''
                if(Platform.OS === 'ios'){
                    deviceType = 'apns'
                }else{
                    deviceType = 'gcm'
                }
                ChatEngineProvider.removeAllChannelsFromPush(token, deviceType)
            }       
        })
        AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
        await firebase.auth().signOut();
        firebase.database().goOffline()   
        firebase.firestore().disableNetwork()     
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'loginNavigationOption' })],
          });
        this.props.navigation.dispatch(resetAction);
    }

    render(){

        return(
            <View style={{flex:1, backgroundColor:'white'}}>
                {this.state.loader ? <MyActivityIndicator /> : null}
                    <View style={{justifyContent:'center', alignItems:'center'}}>

                        <Text style={{
                            marginTop:-38,
                            height:50,
                            textAlign:'center'}}>
                            Subscription
                            </Text>
                    </View>
                    <BottomBorderView 
                        horizontal={0}
                        top={-20}
                        />
                        {
                            this.state.doctorName ? (
                                <ScrollView style={styles.mainContainer}>

                    <Text style={{
                        marginHorizontal:'5%',
                        fontSize:18,
                        fontWeight:'500',
                        marginTop:'17%',
                        textAlign:'center'
                    }}>
                        Your Monthly Subscription
                    </Text>
                    <View style={{
                        flexDirection:'row',
                        marginTop:25,
                        marginHorizontal:'10%',
                        alignItems:'center',
                        justifyContent:'center'
                    }}>
                        <Text style={{
                            fontSize:20,
                            color:'#44A7AF'
                        }}>
                            $
                        </Text>
                        <Text style={{
                            fontSize:50,
                            fontWeight:'bold',
                            color:'#44A7AF'
                        }}>
                            {this.state.amount}
                        </Text>
                        <Text style={{
                            fontSize:20,
                            color:'black',
                            marginLeft:5
                        }}>
                            /mo
                        </Text>
                    </View>
                    <View style={{
                        marginHorizontal:'8%',
                        marginVertical:20,
                        height:150,
                        borderRadius:5,
                        alignItems:'center',
                        elevation: 5,
                        shadowColor: '#999666', shadowOffset: {width: 5, height: 5},
                        shadowOpacity: 0.7, shadowRadius: 5,
                        backgroundColor:'white'
                    }}>
                        <Image style={{
                            width:60,
                            height:60,
                            marginTop:20,
                            borderRadius:30
                        }} 
                        source={
                             require('../../assets/avatar.png')

                            // this.state.image ? {uri:this.state.image} : require('../../assets/avatar.png')
                        }>
                        </Image>
                        <Text style={{
                            fontSize:16,
                            fontWeight:'600',
                            marginTop:10
                        }}>
                            {this.state.doctorName}
                        </Text><Text style={{
                            fontSize:14,
                            marginTop:10,
                            color:'#44A7AF'
                        }}>
                            {this.state.speciality}
                        </Text>
                    </View>

                    {
                       this.state.arrSpecialities ? (
                        this.state.arrSpecialities.map((value)=>{
                            return(
                             <View style={{
                                marginHorizontal:'8%',
                                marginTop:15,
                                flexDirection:'row',
                                alignItems:'center'
                            }}>
                                <Image style={{
                                    marginHorizontal:10
                                }} source={require('../../assets/check_circle.png')}>
                                </Image>
                                <Text style={{
                                    fontSize:12,
                                    color:'gray',
                                    marginLeft:5
                                }}>
                                    {value}
                                </Text>
                            </View>
                            )
                        })
                       ) : (null)
                    }
                                      
                     <Button
                        horizontal = '8%'
                        top = {40}
                        radius = {20}
                        backgColor = '#44A7AF'
                        height = {40}
                        textColor = 'white'
                        titleSize = {16}
                        title = 'Subscribe Monthly'
                        bottom={20}
                        buttonAction = {()=>this.subscribeMonthlyAction()}
                    />
                </ScrollView>
                            ) : (
                                <View style={{flex:1,
                                justifyContent:'center',
                                alignItems:'center'}}>
                                    <Text style={{
                                        textAlign:'center',
                                        fontSize:18,
                                        fontWeight:'600'
                                    }}>
                                        Sorry! you can not register at this time.
                                        Because no doctor has sent you invitation.
                                    </Text>
                                    <Button
                                    horizontal = '8%'
                                    top = {50}
                                    width = '84%'
                                    bottom={20}
                                    radius = {20}
                                    backgColor = '#44A7AF'
                                    height = {40}
                                    textColor = 'white'
                                    titleSize = {16}
                                    title = 'Logout'
                                    buttonAction = {()=>this.logoutAction()}
                                />
                                </View>
                            )
                        }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer:{flex:1,backgroundColor:'white'},
});