
import React,{Component} from 'react';
import {View, Image, Text, KeyboardAvoidingView, 
    TouchableWithoutFeedback, Keyboard} from 'react-native';
    import AsyncStorage from '@react-native-community/async-storage';
import Button from '../../Components/Button';
import TextField from '../../Components/TextField';
import BackButton from '../../Components/BackButton';
import MyActivityIndicator from '../../Components/activity_indicator';
import * as firebase from 'react-native-firebase';
import { StackActions, NavigationActions } from 'react-navigation';

const userRef = firebase.firestore().collection('Users');

export default class Register extends Component {

    constructor() {
        super();
        this.state = {
            name:'',
            email:'',
            phone:'',
            useruid:'',
            loader:false
        }
    }

    componentWillMount(){

        this.setState({phone:this.props.navigation.state.params.phoneNumber})
        AsyncStorage.getItem('useruid').then((userid)=>{
            this.setState({useruid:userid})
        })
    }

    validate = text => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
          return false;
        } else {
          return true;
        }
      };

    continueAction() {

        if(this.state.name.length === 0){
            alert("Please enter name.")
        }else if(this.state.email.length === 0){
            alert("Please enter email id.")
        }else{

            if(this.validate(this.state.email)){
                this.setState({loader:true})
                let phoneNumber = parseInt(this.state.phone)

                userRef.doc(this.state.useruid).set({
                    user:this.state.useruid,
                    phone: phoneNumber,
                    name:this.state.name,
                    email:this.state.email,
                    subscription:false,
                }).then(async()=>{
                    this.setState({loader:false})

                    var docDetailKey = [{'name':this.state.name, 
                             'phonenum': this.state.phone, 'image': ''}]
                    AsyncStorage.setItem('patientdetail',JSON.stringify(docDetailKey))
                    AsyncStorage.setItem('patientphonenum',this.state.phone)

                    var unsubscribe =  firebase.auth().onAuthStateChanged( async (user) => {
                        
                        if (user) {
                            console.log("Register tos called ....")
                            AsyncStorage.setItem('patientid',user.uid)
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate({ routeName: 'TOS' })],
                              });
                            this.props.navigation.dispatch(resetAction);
                        }
                    })
                    unsubscribe();
                    })
            }else{
                alert("Please enter correct email address.")
            }
        }
    }

    backButtonAction() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'loginNavigationOption' })],
          });
          this.props.navigation.dispatch(resetAction);
    }

    render() {

        return(

            <View style={{flex:1, backgroundColor:'white'}}>
                {this.state.loader ? <MyActivityIndicator /> : null}
                <KeyboardAvoidingView behavior={'position'}>
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View>
                            <View style={{flexDirection:'row',
                                    height:40, marginTop:40}}>
                                <BackButton
                                        buttonAction = {()=> this.backButtonAction()}   
                                    image = {require('../../assets/arrow.png')}
                                    />
                            </View>
                            <View style={{alignItems:'center'}}>
                            <View style={{alignItems:'center'}}>
                                <Image style={{width:145, height:107, marginTop:'10%'}} source={require('../../assets/logo-green.png')}>
                                </Image>
                            </View>
                            <Text style={{
                                fontWeight:'bold',
                                fontSize:18,
                                marginTop:'5%',
                                color:'#1B7583'
                            }}>
                                Let's create your account
                            </Text>
                            <TextField 
                                size = {15}
                                weight = '200'
                                width = "80%"
                                image = {require('../../assets/user.png')}
                                placeholderText = "Name"
                                height = {50}
                                textWidth='90%'
                                value = {this.state.name}
                                onChangeText = {(name)=> this.setState({name})}
                                keyboard = 'default'
                            />
                            <BottomBorderView 
                            width='80%'
                            top={5}
                            />

                            <TextField 
                                size = {15}
                                weight = '200'
                                width = "80%"
                                height = {50}
                                textWidth='90%'
                                image = {require('../../assets/email.png')}
                                placeholderText = "Email address"
                                value = {this.state.email}
                                onChangeText = {(email)=> this.setState({email})}
                                keyboard = 'email-address'
                             />
                            <BottomBorderView 
                            width='80%'
                            top={5}
                            />

                            <Button
                                horizontal = '8%'
                                top = '10%'
                                width = '84%'
                                radius = {20}
                                backgColor = '#44A7AF'
                                height = {40}
                                // weight = 'bold'
                                textColor = 'white'
                                titleSize = {16}
                                title = 'Continue'
                                buttonAction = {()=>this.continueAction()}
                             />
                              </View>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </View>
        )
    }
}