import React,{Component} from 'react';
import {
  View, 
  TouchableOpacity,
  Dimensions,
  Text
} from 'react-native';
import BottomBorderView from '../../Components/BottomBorderView';
import Header from '../../Components/Header';

const DEVICE_WIDTH = Dimensions.get("window").width;

export default class SettingDoctor extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
      }

    render(){
        const navigate = this.props.navigation.navigate;
       
        return(
          <View style={{flex:1, backgroundColor:'white'}}>
              <Header title={'Settings'}  type={'doctor'} 
              navigation={navigate}/>
                <BottomBorderView 
                              top={0}
                    />

               <View style={{
                    marginHorizontal:10,
                    marginTop:30,
                    backgroundColor:'white',
                    borderRadius:5,
                    alignItems:'center',
                    borderRadius:5,
                    alignItems:'center',
                    borderColor:'lightgray', 
                    borderWidth:0.5,elevation: 5,
                    shadowColor: '#999666', 
                    shadowOffset: {width: 5, height: 5},
                    shadowOpacity: 0.7, shadowRadius: 5,
                }}>
                  
                 <BottomBorderView 
                        height={1}
                        bkgColor='lightgray'
                        width='100%'
                        top={0}
                        />
                     <TouchableOpacity style={{
                            width:DEVICE_WIDTH-40,
                            height:60,
                            justifyContent:'center',
                        }}onPress={()=> this.props.navigation.navigate('TransactionHistoryDoctor')}>
                            <Text style={{
                                textAlign:'left'
                            }}>
                                Transaction history
                            </Text>
                 </TouchableOpacity>
                 <BottomBorderView 
                        height={1}
                        bkgColor='lightgray'
                        width='100%'
                        top={0}
                        />
                </View>
          </View>
        )
        }
}