
import React,{Component} from 'react';
import {View, Text, Modal, Image,Dimensions,
      TouchableOpacity, KeyboardAvoidingView, Alert, StyleSheet, 
      Platform, FlatList} from 'react-native';
import Button from '../../Components/Button';
import MyActivityIndicator from '../../Components/activity_indicator';
import Header from '../../Components/Header';
import TextField from '../../Components/TextField';
import BottomBorderView from '../../Components/BottomBorderView';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { SwipeListView } from 'react-native-swipe-list-view';
import ChatEngineProvider from "../../lib/pubnub-chatengine";
import Moment from 'moment';

const doctorRef = firebase.firestore().collection('Doctors');
const chatlistRef = firebase.firestore().collection('Chatlist');
const userRef = firebase.firestore().collection('Users');
const unsubscribedChannelRef = firebase.firestore().collection('UnsubscribedChannels');

const DEVICE_WIDTH = Dimensions.get("window").width;

export default class ChatScreen extends Component {

    constructor(){
        super();
        this.state = {
            loader:false,
            showModal:false,
            phoneNum:'',
            name:'',
            providerName:'',
            chatList:[],
            doctorDetail:[],
            doctorKey:'',
            doctorPhoneNum:'',
            arrChannelMes:[],
            arrMessageNo:[],
            arrTimeMessage:[],
            arrNewMessIndex:[],
            doctorName:'',
            doctorImage:'',
            token:'',
            arrSubscribedChannels:[],
            arrUnSubscribedChannels:[],
            conversationType:'patient',
            arrDoctorNames:'',
            arrFilteredNames:'',
        }
    }

    componentWillMount(){
        
        var _this = this;
          
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            this.addListener();
           
            AsyncStorage.getItem('newchatindex').then((value)=>{
               
                if(value){
                    if(_this.props.navigation.state.params){
                        if(_this.props.navigation.state.params.channelName){
                            var toRemove = _this.props.navigation.state.params.channelName;
                            var arrIndex = [...JSON.parse(value)]
                           
                            var index = arrIndex.indexOf(toRemove);
                            if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
                                arrIndex.splice(index, 1);
                            }
                            AsyncStorage.setItem('newchatindex', JSON.stringify(arrIndex))
                            _this.setState({arrNewMessIndex:arrIndex})
                        }
                    }else{
                        _this.setState({arrNewMessIndex:JSON.parse(value)})
                    }
                }
            })
            setTimeout(function(){
                if(_this.state.doctorName === undefined){
                    _this.setState({loader:true})
                }
              }, 1000);
           
            AsyncStorage.getItem('phonenumber').then((phoneNum)=>{
                this.setState({doctorPhoneNum:phoneNum})
              let phoneNumber = parseInt(phoneNum)
              this.findListOfUnsubscribedChannels(phoneNumber);
            //   this.deleteMessageFromChannel()
    
              doctorRef.where('phone', '==', phoneNumber).onSnapshot
               (snapshot => {
    
                if (snapshot.empty) {
                    console.log('No matching documents.');                                      
                  }  else{
                    snapshot.forEach(doc => {
                    let docDetails = doc.data()
                        _this.findListOfAllDoctors(docDetails.name)

                     _this.setState({doctorName:docDetails.name, 
                        doctorImage:docDetails.image})
                     _this.setState({loader:false, showModal:false,
                         doctorDetail:docDetails})
                      });
                  }
          })
          })
        })
    }

    findListOfUnsubscribedChannels(phoneNumber){

        var count = 0
        var arrUnsubChannel = []
        unsubscribedChannelRef.onSnapshot
            (snapshot => {
        if(snapshot._docs.length === 0){
            this.findListOfChannels(phoneNumber)
        }else{
        snapshot.forEach(doc => {
            count = count + 1
            doc.data().channels.map((value)=>{
                arrUnsubChannel.push(value)
            })       
            if(count === snapshot._docs.length){
                this.setState({arrUnSubscribedChannels:arrUnsubChannel})
                this.findListOfChannels(phoneNumber)
            }
       })
    }
})
            
    }

    findListOfChannels(phoneNumber){

        var _this = this;
        let doc = firebase.firestore().collection('Chatlist').doc(phoneNumber.toString());

        doc.onSnapshot(snapshot=> {
       
           if (snapshot.empty) {
                console.log('No matching documents.');                                          
           }  else{
                if(snapshot.data() != null){
                     if(snapshot.data().channels){
                        _this.setState({ chatList:snapshot.data().channels})
                        _this.findLastMessage(snapshot.data().channels)
                       }
                }
           }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
    }

    addListener(){
        var _this = this;
        ChatEngineProvider._pubnub.addListener({

            message: function(m) {
                console.log("received meassage in chat history........doctor screen ", m)
                var msg = m.message; // The Payload
                var subscribedChannel = m.subscribedChannel;
               var publisher = m.publisher; //The Publisher
            _this.updateChatHistory(subscribedChannel, msg.text[0].text, msg.text[0].createdAt, publisher)
            },
            presence: function(p) {
                // handle presence
            },
            signal: function(signalMessage) {
                // Handle signal message
            },
            user: function(userEvent) {
                // for Objects, this will trigger when:
            },
            space: function(spaceEvent) {
                // for Objects, this will trigger when:
            },
            membership: function(membershipEvent) {
                // for Objects, this will trigger when:
            },
            status: function(s) {
                console.log("Status for listnenre", s)
            }         
        });
      }

    findLastMessage(arrChat) {

        var arrChannel = []
        arrChat.map((item, index)=>{
            console.log("find last message item ....", item)
            if(!item.block){
                if(this.state.arrUnSubscribedChannels.includes(item.channelname)){

                }else{
                    if(item.admin.includes(parseInt(this.state.doctorPhoneNum))){
                        arrChannel.push(item.channelname)
                    }else if(item.nonadmin.length > 0){
                        if(item.nonadmin.includes(parseInt(this.state.doctorPhoneNum))){
                            arrChannel.push(item.channelname)
                        }
                    }
                }
            }
            if(index === arrChat.length -1){
                this.setState({arrSubscribedChannels:arrChannel})
                this.fetchMessage(arrChannel)
            }  
        })
    }

    fetchMessage(arrChannel){
        var _this = this;
        ChatEngineProvider._pubnub.fetchMessages({
            channels: arrChannel,
            count: 1,
          }, (status, response) => {
            if(response != undefined){
                if(response.channels){
                  _this.createLastMessageForChannels(response.channels)
                  _this.subscribeALlChannel(arrChannel)
                  AsyncStorage.getItem('token').then((token)=>{
                    console.log("token iss s s", token)
                    if(token){
                        _this.setState({token:token})
                        _this.addAllChannelForPush(arrChannel, token)
                    }else{
                        setTimeout(function(){
                            _this.checkTokenGenerated(arrChannel)
                          }, 2000);
                    }
                })                  
                }
            }
          });
    }

    checkTokenGenerated(arrChannel){
        AsyncStorage.getItem('token').then((token)=>{
            console.log("token called jkjk", token)
            if(token !== null){
                this.setState({token:token})
                this.addAllChannelForPush(arrChannel, token)
            }
        })
    }

    subscribeALlChannel(arrChannel){
        ChatEngineProvider._pubnub.subscribe({
            channels: arrChannel,
            withPresence: true,
            triggerEvents: true,
            autoload: 100
        });
    }

    addAllChannelForPush(arrChannel, token){

        var platformType = ''
        if(Platform.OS === 'ios'){
            platformType='apns'
        }else{
            platformType='gcm'
        }
        ChatEngineProvider._pubnub.push.addChannels({
            channels: arrChannel,
            device: token,
            pushGateway: platformType, // apns or gcm
          }, (status) => {
            if (status.error) {
              console.log('operation failed w/ status: ', status);
            } else {
              console.log('all channels for push are done!');
            }
          });
    }

    createLastMessageForChannels(channels){

        var arrMess = []
        var arrMessNum = []
        var arrTime = []
        var index = 0
       
        const keys = Object.keys(channels)
        for (var key in channels) {

        index = index + 1
        if(channels[key][0].message.text){
            var message = channels[key][0].message.text[0].text
            var time = channels[key][0].message.text[0].createdAt
            arrMess.push(message)
            arrMessNum.push(key)
            arrTime.push(time)
        }
        this.setState({ arrMessageNo:arrMessNum})
        if(index === keys.length){
            this.mappingMessageTOPatient(arrMess, arrTime)
        }
    }
}

    mappingMessageTOPatient(arrMess, arrTime){

        const newMessArr = [...arrMess]
        
        const newTimeArr = [...arrTime]
        this.state.chatList.map((item, index)=>{
           var newIndex = ''
            newIndex = this.state.arrMessageNo.indexOf(item.channelname)
            newMessArr[index] = arrMess[newIndex]
            newTimeArr[index] = arrTime[newIndex]
        })
        this.setState({arrChannelMes:newMessArr})
        this.setState({arrTimeMessage:newTimeArr})
    }

    updateChatHistory(publisherId, newMessage, newCreatedDate, publisher){

        if(this.state.arrNewMessIndex.includes(publisherId)){
        }else{
            if(publisher !== this.state.doctorPhoneNum){
                var arrIndex = [...this.state.arrNewMessIndex]
                arrIndex.push(publisherId)
        
               this.setState({arrNewMessIndex:arrIndex})
               AsyncStorage.setItem('newchatindex', JSON.stringify(arrIndex))
            }
        }

        const newMessArr = [...this.state.arrChannelMes]
        const newTimeArr = [...this.state.arrTimeMessage]
        const newChatList = [...this.state.chatList]
      
        this.state.chatList.map((item, index)=>{
            if(item.channelname === publisherId){
                newMessArr[0] = newMessage
                newTimeArr[0] = newCreatedDate
                newChatList[0]= newChatList[index]
                for(let i = 1; i<=index; i++){
                    newChatList[i]= this.state.chatList[i-1]
                    newMessArr[i]= this.state.arrChannelMes[i-1]
                    newTimeArr[i]= this.state.arrTimeMessage[i-1]
                }
            }
        })
        this.setState({arrChannelMes:newMessArr})
        this.setState({arrTimeMessage:newTimeArr})
        this.setState({chatList:newChatList})
        this.updateChatHistoryInFirebase()
    }

    updateChatHistoryInFirebase(){

        chatlistRef.doc(this.state.doctorPhoneNum).set({
                        channels:this.state.chatList
                      }).then(async()=>{
            })
    }

    updateChatHistoryWhileTapping(tapIndex) {

        const newChatList = [...this.state.chatList]
            newChatList[0]= newChatList[tapIndex]
                for(let i = 1; i<=tapIndex; i++){
                    if(i===tapIndex){
                        newChatList[i]= this.state.chatList[i-1]
                    }else{
                        newChatList[i]= this.state.chatList[i-1]
                    }
                    if(this.state.chatList.length === newChatList.length){
                        this.setState({chatList:newChatList}, () => {
                           })  
                    }
                }
    }

    invitePatient(){
        var _this = this;
        this.setState({loader:true})

        let phoneNumber = parseInt(this.state.phoneNum)
        var channelN = ''
        if(this.state.doctorPhoneNum > phoneNumber){
            channelN = this.state.doctorPhoneNum + "_" + phoneNumber
           }else{
            channelN = phoneNumber + "_" + this.state.doctorPhoneNum
           }

        var admin = []
        admin.push(parseInt(_this.state.doctorPhoneNum))
        admin.push(phoneNumber)

        var checkPatientPresent = false
        var type = ''
        if(this.state.chatList.length > 0){
            this.state.chatList.map((item, index)=>{
                
                if(item.phone === phoneNumber){
                    checkPatientPresent = true
                    type = item.type
                }

                if(this.state.chatList.length === index+1){
                    if(checkPatientPresent){
                        if(type === "doctor"){
                            alert("This Doctor is already added in your chat list.")
                        }else{
                            alert("This Patient is already added in your chat list.")
                        }
                        this.setState({name:'', phoneNum:'', loader:false})
                    }else{

                        this.setState({loader:true})
                     doctorRef.where('phone', '==', phoneNumber).get()
                        .then(snapshot => {
                         
                            if(snapshot.empty){

                                var addPatient = {name:_this.state.name, 
                                    phone:phoneNumber, type: 'patient', block:false, channelname:channelN, 
                                    admin:admin, nonadmin:'', blockpatient:false, blockdoctor:false, 
                                     image:_this.state.doctorImage}
                                _this.fetchChannelFromFirebase(phoneNumber, addPatient)
        
                                var arrChat = []
                                     
                                if(_this.state.chatList.length > 0){
                                    arrChat = _this.state.chatList
                                }
                                var addPatientIndoctor = {name:_this.state.name, 
                                    phone:phoneNumber, type: 'patient', block:false, channelname:channelN, 
                                    admin:admin, nonadmin:'', blockpatient:false, blockdoctor:false,
                                     image:''}
                                arrChat.splice(0, 0, addPatientIndoctor);
                                _this.addChannelInFirebase(_this.state.doctorPhoneNum, arrChat)
                                _this.sendTextMessageToPatient()
                            }else{
                                _this.setState({name:'', phoneNum:'', loader:false})
                                alert("You have entered a provider’s number. Please enter the patient’s number and try again.")
                            }
                        })                       
                    }
                }

            })
        }else{
            this.checkDoctorPresence()
            }
    }

    inviteDoctor(channelName, otherDocPhoneNum, otherDocName, otherDocImage) {

        console.log("invite doctor called ....")
        console.log(channelName)
        console.log(otherDocPhoneNum)
        console.log(otherDocName)
        var _this = this;
        this.setState({loader:true})

        var admin = []
        admin.push(parseInt(this.state.doctorPhoneNum))
        admin.push(otherDocPhoneNum)

        // var checkPatientPresent = false
        // var type = ''
        // if(this.state.chatList.length > 0){
            // this.state.chatList.map((item, index)=>{
                
                // if(this.state.chatList.length === index+1){
                    
                        this.setState({loader:true})
            //          doctorRef.where('phone', '==',otherDocPhoneNum).get()
            //             .then(snapshot => {
                //              console.log(snapshot)
                //             if(snapshot.empty){

                                var addDoctor = {name:_this.state.doctorName, 
                                    phone:_this.state.doctorPhoneNum, type: 'doctor', block:false, channelname:channelName, 
                                    admin:admin, nonadmin:'', blockpatient:false, blockdoctor:false, 
                                     image:_this.state.doctorImage}
                                _this.fetchChannelFromFirebase(otherDocPhoneNum, addDoctor)
        
                                var arrChat = []
                                     
                                if(_this.state.chatList.length > 0){
                                    arrChat = _this.state.chatList
                                }
                                var addDoctorIndoctor = {name:otherDocName, 
                                    phone:otherDocPhoneNum, type: 'doctor', block:false, channelname:channelName, 
                                    admin:admin, nonadmin:'', blockpatient:false, blockdoctor:false,
                                     image:otherDocImage}
                                arrChat.splice(0, 0, addDoctorIndoctor);
                                _this.addChannelInFirebase(_this.state.doctorPhoneNum, arrChat)
                            // }else{
                            //     _this.setState({name:'', phoneNum:'', loader:false})
                            // }
                        // })                       
                    // }
                // }

            // })
        // }else{
        //     // this.checkDoctorPresence()
        //     }
    }

    inviteAction(){

        var _this = this;

        if(this.state.phoneNum.length === 0){
            alert("Please enter phone number.")
        }else if(this.state.name.length === 0){
            alert("Please enter first name.")
        }
        else{

            this.setState({loader:true})
            let doc = firebase.firestore().collection('Chatlist').doc(this.state.phoneNum);

            doc.get().then(docSnapshot => {
            if(docSnapshot.data() === undefined){
               console.log("jflks jj empty")
               _this.invitePatient()
            }else{
                   this.setState({loader:false})
                if(docSnapshot.data().channels.length > 0){
                    userRef.where('phone', '==', parseInt(this.state.phoneNum)).get()
                            .then(snapshot => {
                                _this.setState({loader:false})
                                    if (snapshot.empty) {
                                           alert("This patient is already subscribed by another doctor.")   
                                           _this.setState({phoneNum:'', name:''})                        
                                      }  else{
                                        snapshot.forEach(doc => {
                                            if(doc.data().subscription) {
                                                alert("This patient is already subscribed by another doctor.")
                                                _this.setState({phoneNum:'', name:''})
                                            }else{
                                                alert("Please contact admin.")
                                                    _this.setState({phoneNum:'', name:''})
                                            }
                                        });
                                    }
                        });
                }
            }
            }, err => {
            console.log(`Encountered error: ${err}`);
            });
    }
}

async sendTextMessageToPatient(){

    this.setState({loader: true});
    // const phoneNumPatient = "+91" + this.state.phoneNum
    const phoneNumPatient = "+1" + this.state.phoneNum

    let body = {
        "phone_no": phoneNumPatient,
        "doctor_name": this.state.doctorName
    };

    try {
        fetch('https://us-central1-aepiphany-bdba7.cloudfunctions.net/charge/sendInvite', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        }) .then(response => response.json())
        .then(responseJson => {
           this.setState({loader: false, showModal:false});
            if(responseJson.status){
                alert("Invited successfully.")
            }
        })
        .catch(e => {
           this.setState({loader: false});
           console.log("error ...",e)
        }); 
       }catch (error) {
        this.setState({loader: false});
        console.log("error kjjl", error)
       }
   }


checkDoctorPresence(){
        var _this = this;
        let phoneNumber = parseInt(this.state.phoneNum)
        var channelN = ''
        if(this.state.doctorPhoneNum > phoneNumber){
            channelN = this.state.doctorPhoneNum + "_" + phoneNumber
           }else{
            channelN = phoneNumber + "_" + this.state.doctorPhoneNum
           }
    
            doctorRef.where('phone', '==', phoneNumber).get()
            .then(snapshot => {

                if (snapshot.empty) {
                  
                var admin = []
                admin.push(parseInt(_this.state.doctorPhoneNum))
                admin.push(phoneNumber)
                var addPatient = {name:_this.state.name, 
                    phone:phoneNumber, type: 'patient', block:false, channelname:channelN, 
                    admin:admin, nonadmin:'',blockpatient:false, blockdoctor:false, image:_this.state.doctorImage}
                    _this.fetchChannelFromFirebase(phoneNumber, addPatient)

                var arrChat = []
                if(_this.state.chatList.length > 0){
                    arrChat = _this.state.chatList
                }
                var addPatientInDoctor = {name:_this.state.name, 
                    phone:phoneNumber, type: 'patient', block:false, channelname:channelN, 
                    admin:admin, nonadmin:'',blockpatient:false, blockdoctor:false,image:''}
                    arrChat.splice(0, 0, addPatientInDoctor);
                _this.addChannelInFirebase(_this.state.doctorPhoneNum, arrChat)
                _this.sendTextMessageToPatient()
            }else{
                alert("You have entered a provider’s number. Please enter the patient’s number and try again.")
                _this.setState({name:'', phoneNum:'', loader:false})
            }
        })                       
}

    fetchChannelFromFirebase(phoneNumber, addPatient){

        var _this = this;
        let doc = firebase.firestore().collection('Chatlist').doc(this.state.phoneNum);

        doc.get().then(docSnapshot => {
       
        var arrData = []
            if(docSnapshot.data() === undefined){
            console.log("jflks jj empty")
            }else{            
                if(docSnapshot.data().channels.length > 0){
                    arrData = docSnapshot.data().channels
                }
            }
            arrData.push(addPatient)
            _this.addChannelInFirebase(phoneNumber, arrData)
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
    }

    addChannelInFirebase(key, arrData){

        chatlistRef.doc(key.toString()).set({
            channels:arrData
          }).then(async()=>{
                this.setState({loader:false})
                if(key === parseInt(this.state.doctorPhoneNum)){
                    this.setState({loader:false, showModal:false, chatList:arrData,
                        name:'', phoneNum:'', providerName:''})
                }else{
                    this.setState({loader:false, showModal:false,
                        name:'', phoneNum:'', providerName:''})
                }
            })
    }

    openChatScreen(item, tapindex){

        // this.deleteMessageFromChannel()

        if(this.state.arrUnSubscribedChannels.includes(item.channelname)){
           alert("Please contact admin.")
        }else{
            if(item.block){
                if(item.type === 'group'){
                    if(item.admin.includes(parseInt(this.state.doctorPhoneNum)) && item.blockdoctor){
                        this.goToChatScreen(item, tapindex)
                    }else if(item.blockpatient){
                        this.goToChatScreen(item, tapindex)
                    }else{
                        this.goToChatScreen(item, tapindex)
                    }               
                }else{
                    this.goToChatScreen(item, tapindex)
                }
            }else if(!this.state.arrSubscribedChannels.includes(item.channelname)){
    
                Alert.alert(
                            "You have to join group to start chat.",
                            'Would you like to join group chat.',
                            [
                              {
                                text: 'Yes',
                                onPress: () => {
                                   this.joinGroupChat(item, tapindex)
                                },
                              },
                              {text: 'No', onPress: () => 
                              console.log("press no")},
                            ],
                            {cancelable: false},
                          );
            }
            else{
                this.goToChatScreen(item, tapindex)
            }   
        }
    }

    goToChatScreen(item, tapindex){
        
        var toRemove = item.channelname;
        var arrIndex = [...this.state.arrNewMessIndex]
       
        var index = arrIndex.indexOf(toRemove);
        if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
            arrIndex.splice(index, 1);
        }
        AsyncStorage.setItem('newchatindex', JSON.stringify(arrIndex))
        this.setState({arrNewMessIndex:arrIndex})
        this.props.navigation.navigate('ChatPage', 
        {doctorId:this.state.doctorPhoneNum,
            item:item,
            username:this.state.doctorName,
            ownerType:'doctor',
            userImage:this.state.doctorImage,
            tapindex:tapindex
        })
        this.updateChatHistoryWhileTapping(tapindex)
    }

    onRowDidOpen = (rowKey, rowMap) => {
		setTimeout(() => {
			this.closeRow(rowMap, rowKey);
		}, 800);
    }

    closeRow(rowMap, rowKey) {
		if (rowMap[rowKey]) {
			rowMap[rowKey].closeRow();
		}
    }

    setTime(itemDate){

        var sendMessageTime = ''
        var currentDate  = Moment().format('YYYY-MM-DD');
        var messageDate  = Moment(itemDate).format('YYYY-MM-DD');
        if(currentDate === messageDate){
            sendMessageTime =  Moment(itemDate).format('hh:mm A')
        }else{
            sendMessageTime =  Moment(itemDate).utc().format('MM/DD/YY')
        }
        return(
            <View style={{}}>
                <Text style={{fontSize:10}}>
                    {sendMessageTime}
                </Text>
            </View>
        )
    }

    blockUser = async(rowMap, index, item)=> {
        
        if(item.type === 'group'){

            var arrTotalParticiptant = []
            item.admin.map((phonenum, index)=>{
                arrTotalParticiptant.push(phonenum)
            if(index === item.admin.length -1){
                if(item.nonadmin.length > 0){
                    item.nonadmin.map((phonenum)=>{
                        arrTotalParticiptant.push(phonenum)
                        arrTotalParticiptant.map((phonenum)=>{
                            this.blockUnblockOtherDoctorInChannel(phonenum, item.channelname)
                        })
                    })
                }else{
                    arrTotalParticiptant.map((phonenum)=>{
                        this.blockUnblockOtherDoctorInChannel(phonenum, item.channelname)
                    })
                }
        }
            })
        }else{
            var newKey1 = ''
            var newKey = item.channelname.replace(('_'),'')
            newKey1 = newKey.replace((this.state.doctorPhoneNum),'')
            if(item.block){
                this.blockUnblockOtherDoctorInChannel(this.state.doctorPhoneNum, item.channelname)
                this.blockUnblockOtherDoctorInChannel(newKey1, item.channelname)
            }else{
                this.blockUnblockOtherDoctorInChannel(this.state.doctorPhoneNum, item.channelname)
                this.blockUnblockOtherDoctorInChannel(newKey1, item.channelname)
            }
        }
    }

    blockUnblockOtherDoctorInChannel(phoneNumber, channelName){

        var _this = this;

        let doc = firebase.firestore().collection('Chatlist').doc(phoneNumber.toString());

        doc.get().then(docSnapshot => {
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
                
            if(docSnapshot.data().channels.length > 0){
               var arrData = [...docSnapshot.data().channels]
               docSnapshot.data().channels.map((item, index)=>{
   
                   if(item.channelname === channelName){
                       var channelDetail = arrData[index]
                       if(channelDetail.block){
                           channelDetail['block'] = false
                           channelDetail['blockpatient'] = false
                           channelDetail['blockdoctor'] = false
                       }else{
                           channelDetail['block'] = true
                           channelDetail['blockpatient'] = false
                           channelDetail['blockdoctor'] = true
                       }
                       arrData[index]= channelDetail
                   }
               })
               _this.addChannelInFirebase(phoneNumber, arrData) 
            }
        }
      
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
    }

    joinGroupChat(item, index){

        var arrnonadmin = []
        var modifiedChannelName = item.groupchannelname    
        const mySplitedString = this.state.doctorName.split(' ');
        modifiedChannelName = modifiedChannelName + ',' + mySplitedString[1]
        modifiedChannelName = modifiedChannelName.replace(",,",",")

        if(item.nonadmin){
        if(item.nonadmin.length > 0){
            arrnonadmin = [...item.nonadmin]
            arrnonadmin.splice(0, 0, parseInt(this.state.doctorPhoneNum))
        }
        }else{
        arrnonadmin.push(parseInt(this.state.doctorPhoneNum))
        }
               
        var addOtherDoctor = {name:item.name, 
            phone:parseInt(this.state.doctorPhoneNum), type: 'group', block:false, 
            channelname:item.channelname, admin:item.admin, nonadmin:arrnonadmin
            ,blockpatient:false, blockdoctor:false, groupchannelname:modifiedChannelName, image:''}
       
        item.admin.map((value)=>{
          this.firebaseUpdateAfterJoiningGroupChat(value, item.channelname, addOtherDoctor)
        })
        if(item.nonadmin.length > 0){
            item.nonadmin.map((value)=>{
                this.firebaseUpdateAfterJoiningGroupChat(value, item.channelname, addOtherDoctor)
              })
        }
        this.firebaseUpdateAfterJoiningGroupChat(parseInt(this.state.doctorPhoneNum), item.channelname, addOtherDoctor)
    }

    firebaseUpdateAfterJoiningGroupChat(phoneNumber, channelName, addOtherDoctor){

        var _this = this;

        let doc = firebase.firestore().collection('Chatlist').doc(phoneNumber.toString());

                doc.get().then(docSnapshot => {
              
                var arrData = []
                if(docSnapshot.data() === undefined){
                   console.log("jflks jj empty")
                }else{
                   
                    if(docSnapshot.data().channels.length > 0){
                        
                    arrData = [...docSnapshot.data().channels]
                    docSnapshot.data().channels.map((item, index)=>{
                    if(item.channelname === channelName){
                        arrData[index] = addOtherDoctor
                        checkDatapresence = true
                    }else{
                        arrData[index] = item
                    }
                    })
                    }
                }
                if(!checkDatapresence){
                    arrData.push(addOtherDoctor)
                }
                _this.addChannelInFirebase(phoneNumber, arrData) 
                }, err => {
                console.log(`Encountered error: ${err}`);
                });
    }

    openPatientProfile(phone) {
        var _this = this;
        this.setState({loader:true})

        userRef.where('phone', '==', phone).get()
            .then(snapshot => {
            if (snapshot.empty) {
                this.setState({loader:false})
                alert("Sorry! Patient not registered yet.")
            }  else{
                 snapshot.forEach(doc => {
                    console.log("sfhkd jsdhkdfjs", doc.data());
                    _this.props.navigation.navigate('ProfilePatient', {otherPatientProfile:doc.data()})
                });
            }
        })
    }

    openDoctorProfile(phone) {
        var _this = this;
        console.log("phone num", phone)
        this.setState({loader:true})

        doctorRef.where('phone', '==', parseInt(phone)).get()
            .then(snapshot => {
                _this.setState({loader:false})
                if (snapshot.empty) {
                    console.log('No matching documents.');                                      
                  }  else{
                      snapshot.forEach(doc => {
                          _this.props.navigation.navigate('ProfileDoctor', {otherDoctorProfile:doc.data()})
                        });
                      }
                    });
    }

//Invite doctor functions

 findListOfAllDoctors(doctorname) {

    doctorRef.onSnapshot
            (snapshot => {
      var names = []
      var count = 0
      snapshot.forEach(doc => {
        count = count + 1
          if(doc.data().name !== doctorname){
            names.push(doc.data().name)
          }
          if(count === snapshot._docs.length - 1){
            this.setState({arrDoctorNames:names})
          }
         })
    })
  }

 providerSelectedAction(item) {
    this.setState({providerName:item, arrFilteredNames:[]})
  }

 searchForName(providerName) {
    this.setState({providerName:providerName})
    if(providerName.length > 0){      
       var filtered = []
       this.state.arrDoctorNames.map((value, index)=>{
        if(value.includes(providerName)){
          filtered.push(value)
        }
        if(index === this.state.arrDoctorNames.length - 1){
          this.setState({arrFilteredNames:filtered})
        }
      })
    }else{
      this.setState({arrFilteredNames:[]})
    }
  }

    headerTapSwitchOption(type){
       this.setState({conversationType:type})
       }

sendMessageAction(){

     if(this.state.providerName.length === 0){
        alert("Please enter name.")
    }else if(this.state.doctorName.includes(this.state.providerName)){
        alert("You cannot add yourself.")
        this.setState({providerName:''})
    }else{
        this.findDoctorDetailUsingName()     
    }
        // if(this.state.providerName.length === 0){
        //     alert("Please enter phone number.")
        // }else if(this.state.providePhoneNum === this.state.doctorPhoneNum){
        //     alert("You cannot add yourself.")
        //     this.setState({providePhoneNum:''})
        // }
        // else{
        //     this.searchDoctorAvailabilityInDatabase()
        // }
}   

findDoctorDetailUsingName(){

    var _this = this;
    var checkDoctorFound = false
    var docDetails = []
    this.setState({loader:true})
    doctorRef.get().then(snapshot => {
     _this.setState({loader:false})
     var count = 0
      if(snapshot.empty){
         console.log("jflks jj empty")
       }else{
         snapshot.forEach(doc => {
          count = count + 1
          if(doc.data().name.includes(_this.state.providerName)){
            checkDoctorFound = true
            docDetails = doc.data()
          }
              if(count === snapshot._docs.length){
                _this.setState({providePhoneNum:docDetails.phone})
                if(checkDoctorFound){

                    var channelN = ''
                    if(this.state.doctorPhoneNum > docDetails.phone){
                        channelN = this.state.doctorPhoneNum + "_" + docDetails.phone
                    }else{
                        channelN = docDetails.phone + "_" + this.state.doctorPhoneNum
                    }
                    console.log("channel name combined is", channelN)
                    console.log("invited doctor channel")

                    console.log("chat lsit ''''....")
                    console.log(_this.state.chatList)
                   
                   if(_this.state.chatList.length > 0){
                    let checkDoctor = false;
                    var chatlistcount = 0
                        _this.state.chatList.map((item)=>{
                            console.log("chat list item")
                            console.log(item)
                            chatlistcount = chatlistcount + 1
                            if(item.channelname === channelN){
                                checkDoctor = true
                            }
                            if(chatlistcount === _this.state.chatList.length){
                                if(checkDoctor){
                                    alert("This Doctor is already added in your chat list.")
                                }else{
                                    _this.inviteDoctor(channelN, docDetails.phone, docDetails.name, docDetails.image)
                                }
                            }
                        })
                    }else{
                        _this.inviteDoctor(channelN, docDetails.phone, docDetails.name, docDetails.image)
                    }
                  
                }else{
                  alert("Sorry! No doctor found for name you provided.")
                  _this.setState({providerName:'', providePhoneNum:''})
                }
              }
            })
          }
        })
}

    renderHeaderUI(){

        var patientBkgColor = ''
        var providerBkgColor = ''
        var patientTextColor = ''
        var providerTextColor = ''

        if(this.state.conversationType === 'patient'){
            patientBkgColor = '#1E818D'
            providerBkgColor = 'white'
            patientTextColor = 'white'
            providerTextColor = 'black'
        }else{
            patientBkgColor = 'white'
            providerBkgColor = '#F1424F'
            patientTextColor = 'black'
            providerTextColor = 'white'
        }

        return(
            //  <View style={{
            //     marginTop:20,
            //     marginHorizontal:20,
            //     height:36,
            //     flexDirection:'row',
            //     width:DEVICE_WIDTH-100,
            //     alignItems:'center',
            // }}>
            //     <TouchableOpacity style={{
            //         width:(DEVICE_WIDTH-106)/2,
            //         borderRadius:15,
            //         height:30,
            //         justifyContent:'center',
            //         alignItems:'center',
            //         backgroundColor:patientBkgColor,
            //     }} onPress={()=> this.headerTapSwitchOption('patient')}>
            //         <Text style={{fontSize:15, color:patientTextColor}}>
            //             Invite Patient
            //         </Text>
            //     </TouchableOpacity>
            //     <TouchableOpacity style={{
            //         width:(DEVICE_WIDTH-106)/2,
            //         borderRadius:15,
            //         height:30,
            //         justifyContent:'center',
            //         alignItems:'center',
            //         backgroundColor:providerBkgColor
            //     }} onPress={()=> this.headerTapSwitchOption('doctor')}>
            //         <Text style={{fontSize:15, color:providerTextColor}}>
            //             Invite Doctor
            //         </Text>
            //     </TouchableOpacity>
            // </View>
            <View style={{
                marginTop:20,
                marginHorizontal:20,
                height:36,
                flexDirection:'row',
                width:DEVICE_WIDTH-100,
                alignItems:'center',
            }}>
                <View style={{
                    width:(DEVICE_WIDTH-106),
                    borderRadius:15,
                    marginLeft:2,
                    justifyContent:'center',
                    alignItems:'center'
                }}>
                    <Text style={{fontSize:15, color:'black'}}>
                        Invite Patient
                    </Text>
                </View>
            </View>
        )
    }

    renderUIForDetail(){
if(this.state.conversationType === 'patient'){
            return(
                <View style={{flexDirection:'column'}}>
                <TextField 
                size = {13}
                weight = '200'
                width = '80%'
                length = {10}
                image = {require('../../assets/phone.png')}
                placeholderText = "Enter phone number"
                height = {50}
                textWidth='90%'
                value = {this.state.phoneNum}
                onChangeText = {(phoneNum)=> this.setState({phoneNum})}
                keyboard = 'phone-pad'
                />
                <BottomBorderView 
                horizontal={0}
                top={0}
                />
                <TextField 
                    size = {13}
                    weight = '200'
                    width = "80%"
                    height = {50}
                    textWidth='90%'
                    image = {require('../../assets/user.png')}
                    placeholderText = "Name"
                    value = {this.state.name}
                    onChangeText = {(name)=> this.setState({name})}
                    keyboard = 'default'
                />
                <BottomBorderView 
                top={5}
                />
                </View>
            )
}else{
            return(
                <View style={{flexDirection:'column'}}>
                <TextField 
                size = {13}
                weight = '200'
                width = '80%'
                // length = {10}
                image = {require('../../assets/phone.png')}
                placeholderText = "Name"
                height = {50}
                textWidth='90%'
                value = {this.state.providerName}
                onChangeText = {(providerName)=> this.searchForName(providerName)}
                keyboard = 'default'
                />
    
                <BottomBorderView 
                horizontal={0}
                top={0}
                />
            {
              this.state.arrFilteredNames.length > 0 ? (
                <View style={{height:100,
                  marginTop:10}}>
                      <FlatList
                          data = {this.state.arrFilteredNames}
                          extraData={this.state}
                          renderItem = {this._renderItemProvider} />
                  </View>
              ) : (null)
            }
                </View>
            )
        }
    }

_renderItemProvider = ({item}) => {
    
  return(
    <TouchableOpacity style={{height:30
    }}onPress={()=> this.providerSelectedAction(item)}>
       <Text>
       {item}
       </Text>
    </TouchableOpacity>
  )
}

    _renderItem = ({item, index}) => {

        return(
            <View style={{ 
                backgroundColor:'white',
                justifyContent:'center',
                width:DEVICE_WIDTH
             }}>
                <TouchableOpacity style={{flexDirection:'row',alignItems:'center',
                }}
                    onPress={()=>this.openChatScreen(item, index)} >

                        {
                            item.type === 'patient' ? (
                                <View style={{width:5,
                                    backgroundColor:'#44A7AF', 
                                    marginVertical:0,
                                    height:'100%'}}>
                                    </View>
                            ) : (
                                item.type === 'doctor' ? (
                                    <View style={{width:5,
                                        backgroundColor:'#F64351', 
                                        marginVertical:0,
                                        height:'100%'}}>
                                        </View>
                                ) : (
                                    <View style={{width:5,
                                        backgroundColor:'darkgray', 
                                        marginVertical:0,
                                        height:'100%'}}>
                                    </View>
                                )
                            )
                        }
                    {
                        item.type === 'group' ? (
                            <Image style={{
                                height:40,
                                width:40,
                                borderRadius:20,
                                marginHorizontal:5
                            }} source={require('../../assets/group-icon.png')}>
                            </Image>
                        ) : (
                            <TouchableOpacity onPress={
                                item.type === 'patient' ? (
                                    ()=>this.openPatientProfile(item.phone)
                                ) : (
                                    ()=>this.openDoctorProfile(item.phone)
                                )
                               }
                                >
                                <Image style={{
                                    height:40,
                                    width:40,
                                    borderRadius:20,
                                    marginHorizontal:5
                                }} source={item.image ? {uri:item.image} : 
                                    (require('../../assets/avatar.png'))}>
                                </Image>
                            </TouchableOpacity>
                        )
                    }
                    <View style={{width:DEVICE_WIDTH-110, marginVertical:20,
                    }}>
                            <Text style={{
                                fontSize:15
                            }}>
                                {item.type === 'group' ? (item.groupchannelname) : (item.name)}
                            </Text>
                            {
                                this.state.arrUnSubscribedChannels.includes(item.channelname) ? (
                                    <Text style={{color:'gray',
                                    fontSize:15}}>
                                    Not available
                                    </Text>
                                ) :
                                (
                                    item.block ? (
                                        <Text style={{color:'red',
                                        fontSize:15}}>
                                     Discontinued
                                     </Text>
                                    ) : (
                                        this.state.arrNewMessIndex.includes(item.channelname)? (
                                            <Text style={{color:'red',
                                        fontSize:12}}>
                                     {this.state.arrChannelMes[index]}
                                     </Text>
                                        ) : (
                                            this.state.arrSubscribedChannels.includes(item.channelname)? (
                                                <Text style={{color:'gray',
                                                fontSize:12}}>
                                             {this.state.arrChannelMes[index]}
                                             </Text>
                                            ) : (
                                                <Text style={{color:'red',
                                                fontSize:12}}>
                                                You left the group
                                             </Text>
                                            )
                                           
                                        ) 
                                    )
                                )

                                
                            }
                    </View>
                    {
                        item.block ? (null) : (
                            this.setTime(this.state.arrTimeMessage[index])
                        )
                    }
                        
                </TouchableOpacity>
                <BottomBorderView 
                width='100%'
                horizontal={0}
                top={0}
                />
            </View>
        )
    }

    deleteMessageFromChannel(){
        ChatEngineProvider._pubnub.deleteMessages(
            {
                channel: "group_8888888888_1571830532849",
            },
            (result) => {
                console.log("Delete message result .....")
                console.log(result);
            }
        );
    }
   
    render(){
        const navigate = this.props.navigation.navigate;

        return(
            <View style={{flex:1, backgroundColor:'white'}}>
                  {
                        this.state.showModal ? (
                            <KeyboardAvoidingView behavior={'padding'} enabled>

                            <Modal animationType="fade" transparent={true} visible= {true}>
                            {this.state.loader ? <MyActivityIndicator /> : null}
                                  <View style={{alignItems:'center'}}>
                                <View style={{
                                        height:'100%',
                                        alignItems: 'center',
                                        backgroundColor: 'rgba(0,0,0,0.60)'
                                    }}>
                    
                                    <View style={{
                                         marginTop:40, 
                                         marginHorizontal:30, 
                                         flexDirection:'column', 
                                         backgroundColor:'white',
                                         borderRadius:5,
                                         width:DEVICE_WIDTH-60,
                                         alignItems:'center'}}>
                                        <Text style={{marginTop:20,
                                        fontSize:16}}>
                                            New Conversation
                                        </Text>
                                        <TouchableOpacity style={{ 
                                            position:'absolute', 
                                            right:15,  
                                            marginTop:20}} 
                                            onPress={()=> this.setState({showModal:false})}>
                                                        <Image source={require('../../assets/Close.png')} />
                                        </TouchableOpacity>
                                        <BottomBorderView 
                                            horizontal={10}
                                            width='100%'
                                            top={15}
                                            />
                                      {
                                          this.renderHeaderUI()
                                      }
                                      {
                                          this.renderUIForDetail()
                                      }
                                     {
                                          this.state.conversationType === 'patient' ? (
                                            <Button
                                            top = {40}
                                            radius = {20}
                                            backgColor = '#1E818D'
                                            height = {40}
                                            textColor = 'white'
                                            titleSize = {16}
                                            title = 'Invite'
                                            width='80%'
                                            bottom = {20}
                                            buttonAction = {()=>this.inviteAction()}
                                        />
                                          ): (
                                                  <Button
                                            top = {40}
                                            radius = {20}
                                            backgColor = '#F1424F'
                                            height = {40}
                                            textColor = 'white'
                                            titleSize = {16}
                                            // title = 'Send message'
                                            title = 'Invite'
                                            width='80%'
                                            bottom = {20}
                                            buttonAction = {()=>this.sendMessageAction()}
                                        />

                                          )
                                     }
                                    </View>
                                </View>
                                </View>
                            </Modal> 
                            </KeyboardAvoidingView>
                       ) : (null)
                    }
                {this.state.loader ? <MyActivityIndicator /> : null}
                               
                <Header title={'Conversations'} type={'doctor'} navigation={navigate}
                        ADD={() => this.setState({showModal:!this.state.showModal})}/>
                            <BottomBorderView 
                              top={0}
                            />
                                 <SwipeListView
                                        useFlatList
                                        data={this.state.chatList}
                                        renderItem = {this._renderItem} 
                                        extraData={this.state}
                                        stopLeftSwipe = {-1}
                                        renderHiddenItem={ (data, rowMap) => (
                                                data.item.type === 'group' ? (
                                                    data.item.admin.includes(parseInt(this.state.doctorPhoneNum)) && !data.item.blockpatient ? 
                                                    ( 
                                                    <TouchableOpacity style={[styles.rowBack, styles.backRightBtn, styles.backRightBtnRight]} 
                                                        onPress={ _ => this.blockUser(rowMap, data.index, data.item) }>
                                                     <View style={{flexDirection:'column', alignItems:'center', justifyContent:'center'}}>
                                                {
                                                    data.item.block ? (
                                                        <Text style={styles.backTextWhite}>Continue  </Text>
                                                    ) : (
                                                        <Text style={styles.backTextWhite}>Discontinue  </Text>
                                                    )
                                                }
                                               
                                            </View>
                                        </TouchableOpacity>) : (null)

                                                ) : (
                                                    <TouchableOpacity style={[styles.rowBack, styles.backRightBtn, styles.backRightBtnRight]} 
                                                                onPress={ _ => this.blockUser(rowMap, data.index, data.item) }>
                                                    <View style={{flexDirection:'column', alignItems:'center', justifyContent:'center'}}>
                                                        {
                                                            data.item.block ? (
                                                                <Text style={styles.backTextWhite}>Continue  </Text>
                                                            ) : (
                                                                <Text style={styles.backTextWhite}>Discontinue  </Text>
                                                            )
                                                        }
                                                       
                                                    </View>
                                                </TouchableOpacity>
                                                )                                           
                                        )}
                                        rightOpenValue={-110}
                                        // closeOnRowOpen={false}
                                        previewRowKey={'0'}
                                        previewOpenValue={-40}
                                        previewOpenDelay={3000}
                                        // onRowDidOpen={this.onRowDidOpen}
                                    />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    backRightBtn: {
		alignItems: 'center',
		bottom: 0,
		justifyContent: 'center',
		position: 'absolute',
		top: 0,
		width: 110
    },
    backRightBtnRight: {
        backgroundColor: '#44A7AF',
        height:70,
        marginVertical:10,
        right: 0
    },
    backTextWhite: {
        color: 'white',
        fontWeight:'bold'
    },
    rowBack: {
		alignItems: 'center',
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
        paddingLeft: 15
	},
});