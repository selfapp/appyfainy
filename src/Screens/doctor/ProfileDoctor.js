

import React,{Component} from 'react';
import {View, Text,TouchableOpacity, ScrollView,Image, Modal,
      Dimensions, StyleSheet, Platform, Linking, FlatList} from 'react-native';
import Button from '../../Components/Button';
import BottomBorderView from '../../Components/BottomBorderView';
import MyActivityIndicator from '../../Components/activity_indicator';
import * as firebase from 'react-native-firebase';
import { StackActions, NavigationActions } from 'react-navigation';
import ProfileTextField from '../../Components/ProfileTextField';
import ChatEngineProvider from "../../lib/pubnub-chatengine";
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../../Components/Header';
import Pdf from 'react-native-pdf';

const doctorRef = firebase.firestore().collection('Doctors');
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;

export default class ProfileDoctor extends Component {

    constructor(){
        super();
        this.state = {
            loader:false,
            doctorDetail:[],
            arrDays:[],
            checkOtherDoctor:false,
            arrDocuments:[],
            showPdf:false,
            showImage:false,
            docImageLink:'',
            phoneNum:''
        }
    }

    componentDidMount () {
        
        var _this = this;

        if(this.props.navigation.state.params){
            if(this.props.navigation.state.params.otherDoctorProfile){
            const keys = Object.keys(this.props.navigation.state.params.otherDoctorProfile)
            if(keys.includes('days')) {
                this.daysMaping(this.props.navigation.state.params.otherDoctorProfile.days)
            }
            this.setState({doctorDetail:this.props.navigation.state.params.otherDoctorProfile,
                checkOtherDoctor:true})
        }
        }else{
            this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
                _this.setState({loader:true})
                AsyncStorage.getItem('phonenumber').then((phoneNum)=>{
                  let phoneNumber = parseInt(phoneNum)
                  this.setState({phoneNum:phoneNum})
                  doctorRef.where('phone', '==', phoneNumber).get()
                    .then(snapshot => {
                        if (snapshot.empty) {
                            console.log('No matching documents.');                                              
                          }  else{
                            snapshot.forEach(doc => {
                                const keys = Object.keys(doc.data())

                                if(keys.includes('days')) {
                                    _this.daysMaping( doc.data().days)
                                }
                                _this.setState({doctorDetail: doc.data()})
                                _this.setState({loader:false})
                                var arrDocument = []
                                let docNew = firebase.firestore().collection('Attachments').
                                             doc(phoneNum).collection('files');

                                docNew.get().then(docSnapshot => {
                      
                                docSnapshot.forEach(doc => {
                                    let transDetails = doc .data()
                                    arrDocument.push(transDetails)
                                    _this.filterDocuments(arrDocument)
                                })                               
                                    }, err => {
                                    console.log(`Encountered error: ${err}`);
                                    });
                              });
                          }
                });
              })
          });
        }
}

filterDocuments(arrDocument) {

    var sendDoc = []
    arrDocument.map((item, index)=>{
        if(item.senderId === parseInt(this.state.phoneNum)){
            sendDoc.push(item)
        }
        if(index === arrDocument.length - 1){
            this.setState({arrDocuments:sendDoc})
        }
    })
}

daysMaping(days){
        
        var daysValue = []
        days.map((value)=>{

            if(value === 1){
                daysValue.push("Monday")
            }else if(value === 2){
                daysValue.push("Tuesday")
            }else if(value === 3){
                daysValue.push("Wednesday")
            }else if(value === 4){
                daysValue.push("Thursday")
            }else if(value === 5){
                daysValue.push("Friday")
            }else if(value === 6){
                daysValue.push("Saturday")
            }else{
                daysValue.push("Sunday")
            }
        })
        this.setState({arrDays:daysValue})
}

    editProfileAction(){
        this.props.navigation.navigate('EditProfileDoctor')
    }
    
   async logoutAction() {
    AsyncStorage.getItem('token').then((token)=>{

        console.log("token /////", token)
        if(token !== null){
            var deviceType=''
            if(Platform.OS === 'ios'){
                deviceType = 'apns'
            }else{
                deviceType = 'gcm'
            }
            ChatEngineProvider.removeAllChannelsFromPush(token, deviceType)
        }       
    })
    AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
    await firebase.auth().signOut();
    firebase.database().goOffline()
    firebase.firestore().disableNetwork()
    ChatEngineProvider.unsubscribeAllChannel();
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'loginNavigationOption' })],
      });
      this.props.navigation.dispatch(resetAction);
    }

    backButtonAction(){
        this.props.navigation.goBack()
    }

    documentPressAction(item) {

        if(item.contentType.includes('image')){
            this.setState({showImage:true, docImageLink:item.mediaLink})
        }else if(item.contentType.includes('audio') || item.contentType.includes('video')){
            Linking.canOpenURL
            (item.mediaLink).then(supported => {
                if (supported) {
                    Linking.openURL(item.mediaLink);
                } else {
                    console.log("Don't know how to open URI: " + this.state.doctorDetail.website);
                }
                });
        }
        else{
            const source = {uri:item.mediaLink, cache:true};
            this.setState({showPdf:true, docImageLink:source})
        }
    }

    openUrlAction(){

        Linking.canOpenURL(this.state.doctorDetail.website).then(supported => {
            if (supported) {
              Linking.openURL(this.state.doctorDetail.website);
            } else {
              console.log("Don't know how to open URI: " + this.state.doctorDetail.website);
            }
          });
        }

    _renderItem = ({item}) => {

            return(
                <TouchableOpacity style={{ 
                    backgroundColor:'white',
                    justifyContent:'center',
                    width:DEVICE_WIDTH-20,
                    height:60,
                    marginTop:10
                 }}onPress={()=>this.documentPressAction(item)}>
                    <View style={{flexDirection:'row',alignItems:'center',
                    }}>
                        {
                            item.contentType.includes('image') ? (
                                <Image style={{
                                    height:40,
                                    width:40,
                                    borderRadius:20,
                                    marginHorizontal:5
                                }} source={{uri:item.mediaLink}}>
                                </Image>
                            ) : (
                                item.contentType.includes('audio') ? (
                                    <Image style={{
                                        height:40,
                                        width:40,
                                        borderRadius:20,
                                        marginHorizontal:5
                                    }} source={require('../../assets/audio_round.png')}>
                                    </Image>
                                ) : 
                                (
                                    item.contentType.includes('video') ? (
                                        <Image style={{
                                            height:40,
                                            width:40,
                                            borderRadius:20,
                                            marginHorizontal:5
                                        }} source={require('../../assets/video_round.png')}>
                                        </Image>
                                    ) : (
                                        <Image style={{
                                            height:40,
                                            width:40,
                                            borderRadius:20,
                                            marginHorizontal:5
                                        }} source={require('../../assets/pdf_round.png')}>
                                        </Image>
                                    )
                                )
                            )
                        }
                                <Text style={{
                                    fontSize:14
                                }}>
                                   {item.name}
                                </Text>
                                <Text style={{fontSize:10,
                                color:'gray',
                                marginTop:3}}>
                                    
                                </Text>                          
                                   
                    </View>
                    <BottomBorderView 
                    width='100%'
                    horizontal={0}
                    top={10}
                    />
                </TouchableOpacity>
            )
    }

    render(){
        const navigate = this.props.navigation.navigate;

        return(
            <View style={{flex:1, backgroundColor:'lightgray'}}>

    {
        this.state.showImage ? (

            <Modal animationType="fade" transparent={true} visible= {true}>
                  <View style={{alignItems:'center'}}>
                <View style={{
                        height:'100%',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.60)'
                    }}>
                      {this.state.loader ? <MyActivityIndicator /> : null}
    
                    <View style={{
                         marginTop:30, 
                         marginHorizontal:20, 
                         flexDirection:'column', 
                         backgroundColor:'white',
                         borderRadius:5,
                         width:DEVICE_WIDTH-40,
                         height:DEVICE_HEIGHT-60,
                         alignItems:'center'}}>
                        
                        <TouchableOpacity style={{ 
                             position:'absolute', 
                             right:0,  
                             width:60,
                             height:70,
                             justifyContent:'center',
                             alignItems:'center'}} 
                            onPress={()=> this.setState({showImage:false})}>
                                        <Image source={require('../../assets/Close.png')} />
                        </TouchableOpacity>
                        <Image style={{
                            height:DEVICE_HEIGHT-100,
                            width:'100%',
                            marginTop:40,
                            resizeMode:'contain'
                        }} source={{uri:this.state.docImageLink}}>
                        </Image>
                       
                    </View>
                </View>
                </View>
            </Modal> 

       ) : (null)
    }

{
        this.state.showPdf ? (
          <Modal animationType="fade" transparent={true} visible= {true}>
                  <View style={{
                        height:'100%',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.60)',
                        width:DEVICE_WIDTH
                    }}>
                       <View style={{
                         marginTop:30, 
                         flexDirection:'column', 
                         backgroundColor:'white',
                         borderRadius:5,
                        height:DEVICE_HEIGHT-60,
                         width:DEVICE_WIDTH,
                         alignItems:'center'}}>
                            <TouchableOpacity style={{ 
                            position:'absolute', 
                            right:0,  
                            width:60,
                            height:70,
                            justifyContent:'center',
                            alignItems:'center',
                        }} 
                            onPress={()=> this.setState({showPdf:false})}>
                                        <Image source={require('../../assets/Close.png')} />
                         </TouchableOpacity>

              <View style={{marginTop:50}}>
                <Pdf
                    source={this.state.docImageLink}
                    onLoadComplete={(numberOfPages,filePath)=>{
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        console.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        console.log(error);
                    }}
                    style={styles.pdf}/>
               </View>
                         </View>
                </View>
            </Modal> 
        ) : (null)
      }
                {this.state.loader ? <MyActivityIndicator /> : null}

            {
                this.state.checkOtherDoctor ? (
                    <Header title={'Profile'} type={'doctor'} 
                                navigation={navigate}
                                withBackButton=
                                {() => this.backButtonAction()}/>
                ) : (
                    <Header title={'Profile'} type={'doctor'} 
                                navigation={navigate}
                                EDIT={() => this.editProfileAction()}/>
                )
            }
                <BottomBorderView 
                height={1}
                bkgColor='lightgray'
                width='100%'
                top={0}
                />
            <ScrollView style={{flex:1,backgroundColor:'white'}}>
                
                    <View style={{
                        width:'100%',
                        height:150,
                        backgroundColor:'#F64351',
                        borderBottomLeftRadius:20,
                        borderBottomRightRadius:20
                    }}>
                    </View>

                <View style={{
                    marginHorizontal:10,
                    marginTop:-80,
                    width:DEVICE_WIDTH - 20,
                    backgroundColor:'white',
                    borderRadius:5,
                    alignItems:'center',
                    borderColor:'lightgray', 
                    borderWidth:0.5,elevation: 5,
                    shadowColor: '#999666', 
                    shadowOffset: {width: 5, height: 5},
                    shadowOpacity: 0.7, shadowRadius: 5,
                }}>
                    <Image style={{
                    marginTop:-40,
                    height:80,
                    width:80,
                    borderRadius:40,
                    borderColor:'#F64351',
                    borderWidth:2,
                }} source={this.state.doctorDetail ? (this.state.doctorDetail.image ? 
                {uri:this.state.doctorDetail.image } : require('../../assets/avatar.png'))  
                : require('../../assets/avatar.png')}>
                </Image>
                    <Text style={{marginTop:15}}>
                        {this.state.doctorDetail ? 
                            (this.state.doctorDetail.name ? 
                                this.state.doctorDetail.name : '') : ''}
                    </Text>
                    <Text style={{marginTop:5, color:'gray', fontSize:13}}>
                    {this.state.doctorDetail ? this.state.doctorDetail.speciality : ''}

                    </Text>
                    <BottomBorderView 
                    width='100%'
                    top={10}
                    />
                    <View style={{
                        marginHorizontal:10,
                        width:DEVICE_WIDTH - 40,
                        alignItems:'center',
                        marginTop:25,
                        flexDirection:'row',
                    }}>
                        <Image style={{
                            marginLeft:10
                        }} source={require('../../assets/calender.png')}>
                        </Image>
                        <Text style={{
                            fontSize:10,
                            color:'gray',
                            marginLeft:5
                        }}>
                            {this.state.doctorDetail ? 
                            (this.state.arrDays.length > 0 ? (
                                this.state.arrDays.map((day, index)=>{
                                    return(
                                        this.state.arrDays.length - 1 === index ? (
                                            day
                                        ) : (
                                            day + ", "
                                        )
                                    )
                                })
                            ) : 'No data found') :
                             'No data found'}
                        </Text>
                    </View>
                    <BottomBorderView 
                    horizontal={40}
                    width={DEVICE_WIDTH - 40}
                    top={10}
                    />
                    <View style={{
                        marginHorizontal:10,
                        width:DEVICE_WIDTH - 40,
                        alignItems:'center',
                        marginTop:15,
                        flexDirection:'row',
                    }}>
                        <Image style={{
                            marginLeft:10
                        }} source={require('../../assets/Clock.png')}>
                        </Image>
                        <Text style={{
                            fontSize:10,
                            color:'gray',
                            marginLeft:5,
                            width:DEVICE_WIDTH-180
                        }}>
                         {this.state.doctorDetail ? 
                            (this.state.doctorDetail.starttime ? this.state.doctorDetail.starttime + " - " : '00:00 AM - ') : '00:00 AM - '}
                          
                            {this.state.doctorDetail ? (this.state.doctorDetail.endtime ? this.state.doctorDetail.endtime :'00:00 AM') : '00:00 AM'}
                        </Text>
                    </View>
                    <BottomBorderView 
                    horizontal={40}
                    width={DEVICE_WIDTH - 40}
                    top={10}
                    />
                     <View style={{
                        marginHorizontal:10,
                        width:DEVICE_WIDTH - 40,
                        alignItems:'center',
                        marginTop:15,
                        flexDirection:'row',
                        marginBottom:20
                    }}>
                        <Image style={{
                            marginLeft:10
                        }} source={require('../../assets/map.png')}>
                        </Image>
                        <Text style={{
                            fontSize:10,
                            color:'gray',
                            marginLeft:5,
                            marginRight:15
                        }}>
                        {this.state.doctorDetail ? this.state.doctorDetail.location : ''}
                        </Text>
                    </View>
                </View>
                <Text style={{
                    marginLeft:20,
                    marginTop:25,
                    width:'100%',
                    textAlign:'left'
                }}>
                    Personal Details
                </Text>
                <View style={{
                    marginHorizontal:10,
                    marginTop:10,
                    width:DEVICE_WIDTH - 20,
                    backgroundColor:'white',
                    borderRadius:5,
                    alignItems:'center',
                    borderRadius:5,
                    alignItems:'center',
                    borderColor:'lightgray', 
                    borderWidth:0.5,elevation: 5,
                    shadowColor: '#999666', 
                    shadowOffset: {width: 5, height: 5},
                    shadowOpacity: 0.7, shadowRadius: 5,
                }}>
                <ProfileTextField
                    title={'Name'}
                    value={this.state.doctorDetail ? 
                        (this.state.doctorDetail.name ? this.state.doctorDetail.name : '') : ''}
                    image ={require('../../assets/name.png')}
                />
                <ProfileTextField
                    title={'Experience'}
                    value={this.state.doctorDetail ? 
                        (this.state.doctorDetail.experience ? this.state.doctorDetail.experience + " " + "Years" : '') : ''}
                    image ={require('../../assets/experience.png')}
                />
                <ProfileTextField
                    title={'Website'}
                    color={'blue'}
                    underline={'underline'}
                    makeTouchable ={true}
                    buttonAction = {()=> this.openUrlAction()} 
                    value={this.state.doctorDetail ? (this.state.doctorDetail.website ? this.state.doctorDetail.website:'') : ''}
                    image ={require('../../assets/website.png')}
                />
                <View>
                  <View style={styles.boxView}>
                    <Image style={{
                        marginLeft:10
                    }} source={require('../../assets/expertise.png')}>
                    </Image>
                    <Text style={{
                        fontSize:12,
                        marginLeft:5
                    }}>
                        Additional Expertise
                    </Text>
                    </View>
                    {
                        this.state.doctorDetail ? (
                            this.state.doctorDetail.services ? (
                                this.state.doctorDetail.services.map((value)=>{
                                    return(
                                        <Text style={{marginTop:5, fontSize:10,
                                            color:'gray',marginLeft:35}}>
                                            {value}
                                        </Text>
                                    )
                                })
                            ) : (null)
                            
                        ) : (null)  
                    }
                    <BottomBorderView 
                    horizontal={20}
                    width={DEVICE_WIDTH-40}
                    top={10}
                    />
                </View>
        {
            this.state.checkOtherDoctor ? (null): (
                <View>
                <View style={styles.boxView}>
                <Image source={require('../../assets/Documents_doctor.png')}>
                </Image>
                <Text style={{
                    fontSize:12,
                    marginLeft:5
                }}>
                    Documents
                </Text>
                </View>
            <View style={{
                height:200}}>
        
                    <FlatList
                    data = {this.state.arrDocuments}
                    extraData={this.state}
                    renderItem = {this._renderItem} />
                </View>
            </View>
            )
        }
                </View>    
                {
                this.state.checkOtherDoctor ? (null): (
                    <Button
                    horizontal = '8%'
                    top = {40}
                    width = '84%'
                    bottom={20}
                    radius = {20}
                    backgColor = '#F64351'
                    height = {40}
                    textColor = 'white'
                    titleSize = {16}
                    title = 'Logout'
                    buttonAction = {()=>this.logoutAction()}
                />
                )
                }
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    boxView:{ marginHorizontal:10,
        width:DEVICE_WIDTH - 40,
        alignItems:'center',
        marginTop:25,
        flexDirection:'row'},
    pdf: {
        flex:1,
        width:DEVICE_WIDTH,
        height:DEVICE_HEIGHT,
    }
})