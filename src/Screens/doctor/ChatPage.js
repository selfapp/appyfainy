import React, { Component } from "react";
import { View, Text, Platform, Modal, TouchableOpacity, Image, 
  KeyboardAvoidingView, Dimensions, 
  FlatList, Linking, InteractionManager} from "react-native";
import { GiftedChat, Message } from "react-native-gifted-chat";
import ActionSheet from 'react-native-actionsheet';
import ChatEngineProvider from "../../lib/pubnub-chatengine";
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import TextField from '../../Components/TextField';
import Moment from 'moment';
import Header from '../../Components/Header';
import ChatBubble from "../../Components/ChatBubble";
import ImagePicker from 'react-native-image-crop-picker';
import RNFS from "react-native-fs";
import DocumentPicker from 'react-native-document-picker';
import sha1 from "sha1";
import Pdf from 'react-native-pdf';

const doctorRef = firebase.firestore().collection('Doctors');
const chatlistRef = firebase.firestore().collection('Chatlist');
const userRef = firebase.firestore().collection('Users');
const unsubscribedChannelRef = firebase.firestore().collection('UnsubscribedChannels');

const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;

export default class ChatPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      doctorPhoneNum:'',
      doctorName:'',
      showModal:false,
      channelName:'',
      providePhoneNum:'',
      providerName:'',
      message:'',
      loader:false,
      arrAdmin:[],
      arrNonadmin:[],
      groupChannelName:'',
      otherUsername:'',
      type:'',
      groupName:'',
      isGroupAdmin:false,
      attachImageUrl:'',
      showPdf:false,
      showPdfModal:false,
      asyncStorageKeys:'',
      fileNameonServer:'',
      blockedChat:false,
      senderImage:'',
      senderName:'',
      arrDoctorNames:'',
      arrFilteredNames:'',
      arrUnSubscribedChannels:[]
    };
  }  

 async componentDidMount() {
    this.addListener();

    this.findListOfChannels()
    this.findListOfUnsubscribedChannels();
    const asyncStorageKeys = await AsyncStorage.getAllKeys();
        this.setState({asyncStorageKeys:asyncStorageKeys})

    // InteractionManager.runAfterInteractions(() => {
      // ...long-running synchronous task...

   
    
    AsyncStorage.getItem('phonenumber').then((phoneNum)=>{

      AsyncStorage.getItem('doctorname').then((doctorname)=>{

        if(this.props.navigation.state.params.ownerType === 'patient'){
          AsyncStorage.getItem('patientid').then((patientid)=>{
            this.findPatientImage(patientid)
        })
        }else if(this.props.navigation.state.params.ownerType === 'doctor'){
          this.findListOfAllDoctors(doctorname)
          this.findDoctorImage()
        }
        this.setState({doctorPhoneNum:phoneNum})
        this.setState({doctorName:doctorname})

        var itemDetails = this.props.navigation.state.params.item

        if(this.props.navigation.state.params.ownerType === 'patient'){

          if(itemDetails.block){
            if(itemDetails.type === 'group' && itemDetails.blockpatient){
                alert("You have to unblock the group to start chat.")
            }else if(itemDetails.blockdoctor){
                alert("Your provider has discontinued the service. Please contact your provider's office.")
            }
        }
      }else if(this.props.navigation.state.params.ownerType === 'doctor'){
        if(itemDetails.block){
          if(itemDetails.type === 'group'){
              if(itemDetails.admin.includes
                (parseInt(this.props.navigation.state.params.doctorId)) 
              && itemDetails.blockdoctor){
                  alert("You have to unblock the group to start chat.")
              }else if(itemDetails.blockpatient){
                  alert("Your patient has unsubscribed")
              }else{
                  alert("Sorry! you are blocked you can't chat.")
              }               
          }
        }
      }
        if(itemDetails.type !== 'group'){
          var groupchName = itemDetails.name
          if(doctorname !== null){
            const mySplitedString = doctorname.split(' ');
            groupchName = groupchName + "," + mySplitedString[1]
            this.setState({groupChannelName:groupchName})
          }
        }else{
          this.setState({groupChannelName:itemDetails.groupchannelname})
        }
        const keys = Object.keys(itemDetails)
        if(keys.includes('nonadmin')) {
          this.setState({arrNonadmin:itemDetails.nonadmin})
        }
        this.setState({
          otherUsername:itemDetails.name,
          channelName:itemDetails.channelname,
          type:itemDetails.type,
          groupName:itemDetails.name,
          arrAdmin:itemDetails.admin
        },
        ()=>{
          if(this.state.arrAdmin.includes(parseInt(this.state.doctorPhoneNum))){
            this.setState({isGroupAdmin:true})
          }else{
            this.setState({isGroupAdmin:false})
          }
      ChatEngineProvider._pubnub.history(
      { 'channel': this.state.channelName, reverse: true, count: 100 },
      (status, res) => {
        let newmessage = [];
        if(res !== undefined){
          res.messages.forEach(function(element, index) {
            if(element.entry.text){
              newmessage[index] = element.entry.text[0];
            }
          });
          this.setState(previousState => ({
            messages: GiftedChat.append(
              previousState.messages,
              newmessage.reverse()
            )
          }));
        }
      }
    ); 
        }
        )
      })
    })
  // })
  }

  findListOfUnsubscribedChannels(){

    unsubscribedChannelRef.onSnapshot
            (snapshot => {
    if(snapshot._docs.length === 0){
      }else{
      snapshot.forEach(doc => {
      doc.data().channels.map((value)=>{
        if(value === this.props.navigation.state.params.item.channelname){
          this.setState({blockedChat:true})
        }
      })
    })
    }
 })
}

  findListOfAllDoctors(doctorname) {

    doctorRef.onSnapshot
            (snapshot => {
      var names = []
      var count = 0
      snapshot.forEach(doc => {
        count = count + 1
          if(doc.data().name !== doctorname){
            names.push(doc.data().name)
          }
          if(count === snapshot._docs.length - 1){
            this.setState({arrDoctorNames:names})
          }
         })
    })
  }

  findListOfChannels(){

    let doc = firebase.firestore().collection('Chatlist').
      doc(this.props.navigation.state.params.doctorId);
      doc.onSnapshot(docSnapshot => {
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
            this.showAlert(docSnapshot.data().channels[this.props.navigation.state.params.tapindex])
        }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
}

showAlert(data){

  if(this.props.navigation.state.params.item.channelname === data.channelname){
    if(data.block){
        this.setState({blockedChat:true})      
    }
    else{
        this.setState({blockedChat:false})
    }
}
}

findDoctorImage(){

  var _this = this;

  let doc = firebase.firestore().collection('Doctors').doc(this.props.navigation.state.params.doctorId);

        doc.onSnapshot(docSnapshot => {
        if(docSnapshot.data() === undefined){
             console.log("jflks jj empty")
          }else{
          _this.setState({senderName:docSnapshot.data().name, 
            senderImage:docSnapshot.data().image}) 
          }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
}

findPatientImage(patientid){
  var _this = this;

  let doc = firebase.firestore().collection('Users').doc(patientid);

  doc.onSnapshot(docSnapshot => {
  if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
  }else{
      _this.setState({senderName:docSnapshot.data().name, 
            senderImage:docSnapshot.data().image}) 
  }
  }, err => {
        console.log(`Encountered error: ${err}`);
        });
}

 addListener(){
    var _this = this;
     ChatEngineProvider._pubnub.addListener({
      message:async function(m) {
          // handle message
          console.log("received meassage in chat page ", m)
          var msg = m.message; // The Payload
          var subscribedChannel = m.subscribedChannel;
          if(subscribedChannel === _this.state.channelName){
               _this.setState(previousState => ({
                messages: GiftedChat.append(previousState.messages, msg.text[0]),
            }));
          }
      },
      presence: function(p) {
          // handle presence
      },
      signal: function(signalMessage) {
          // Handle signal message
      },
      user: function(userEvent) {
          // for Objects, this will trigger when:
          // . user updated
          // . user deleted
      },
      space: function(spaceEvent) {
          // for Objects, this will trigger when:
          // . space updated
          // . space deleted
      },
      membership: function(membershipEvent) {
          // for Objects, this will trigger when:
          // . user added to a space
          // . user removed from a space
          // . membership updated on a space
      },
      status: function(s) {
        console.log("Status for listnenre", s)
    }  
    });
  }

  backButtonAction(){
    if(this.props.navigation.state.params.ownerType === 'patient'){
      this.props.navigation.navigate('ChatScreenPatient', 
      {channelName:this.props.navigation.state.params.item.channelname})
    }else{
      this.props.navigation.navigate('ChatScreen', 
      {channelName:this.props.navigation.state.params.item.channelname})
    }   
  }

  onSend(messages = [], attachementType) {
      this.setState({
        is_sending: false
      });
    
    var messObject = messages[0]
    messObject['type']= attachementType

    ChatEngineProvider.publishMessage(messages,this.state.channelName, 
      this.props.navigation.state.params.username)

      var textMessage = ''
      if(messObject.text.length > 0){
         textMessage = messObject.text
      }else{
        textMessage = messObject.image
      }
    this.saveMessageOnFirebase(textMessage, messObject.createdAt)
  }

  saveMessageOnFirebase(message, date) {

    let doc = firebase.firestore().collection('Chats').
              doc(this.state.channelName).collection('chats');
     doc.add({
      name:this.props.navigation.state.params.username,
      message:message,
      date:date
    }).then(async()=>{
    })
  }
  
  sendMessageAction(){
    
    if(this.state.providerName.length === 0){
        alert("Please enter name.")
    }else if(this.props.navigation.state.params.username.includes(this.state.providerName)){
        alert("You cannot add yourself.")
        this.setState({providerName:''})
    }else if(this.state.arrNonadmin.length === 3){
        alert("Sorry you can't add more doctors.")
        this.setState({providerName:''})
    }else{
        this.findDoctorDetailUsingName()     
    }
}

findDoctorDetailUsingName(){

    var _this = this;
    var checkDoctorFound = false
    var docDetails = []
    this.setState({loader:true})
    doctorRef.get().then(snapshot => {
     _this.setState({loader:false})
     var count = 0
      if(snapshot.empty){
         console.log("jflks jj empty")
       }else{
               
         snapshot.forEach(doc => {
          count = count + 1
          if(doc.data().name.includes(_this.state.providerName)){
            checkDoctorFound = true
            docDetails = doc.data()
          }
              if(count === snapshot._docs.length){
                _this.setState({providePhoneNum:docDetails.phone})
                if(checkDoctorFound){
                  if(_this.state.arrNonadmin){
                      if(_this.state.arrNonadmin.length > 0){
                        if(_this.state.arrNonadmin.includes(docDetails.phone)){
                          alert("You can't add this doctor. It is already available in group.")
                          _this.setState({providerName:'', providePhoneNum:''})
                        }else{
                          if(_this.state.type === "group"){
                            _this.findOtherDoctorDetail(docDetails)
                          }else{
                            _this.createNewGroupChat(docDetails)
                          }
                        }
                      }else{
                        if(_this.state.type === "group"){
                          _this.findOtherDoctorDetail(docDetails)
                        }else{
                          _this.createNewGroupChat(docDetails)
                        }
                      }
                    }else{
                      if(_this.state.type === "group"){
                        _this.findOtherDoctorDetail(docDetails)
                      }else{
                        _this.createNewGroupChat(docDetails)
                      }
                    } 
                }else{
                  alert("Sorry! No doctor found for name you provided.")
                  _this.setState({providerName:'', providePhoneNum:''})
                }
              }
            })
          }
        })
}

createNewGroupChat(doctDetails) {

  var _this = this;
  var arrProvName = this.state.providerName.split(' ')
  var splitProviderName = this.state.providerName

  if(arrProvName.length > 1){
    splitProviderName = arrProvName[1]
  }
  var newGroupChannelName = this.state.groupChannelName
  newGroupChannelName = newGroupChannelName + "," + splitProviderName
  this.setState({groupChannelName:newGroupChannelName})

  let doc = firebase.firestore().collection('Chatlist').doc(_this.state.doctorPhoneNum);

        doc.get().then(docSnapshot => {
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
            if(docSnapshot.data().channels.length > 0){
                var checkDoctorAvailabityInGroup = false
                docSnapshot.data().channels.map((item, index)=>{

                  if(item.name === _this.state.groupName){
                      if(item.nonadmin.length === 1){
                        if(item.nonadmin[0] === _this.state.providePhoneNum){
                          checkDoctorAvailabityInGroup = true
                        }
                      }
                    }
                    if(index === docSnapshot.data().channels.length - 1){
                      if(checkDoctorAvailabityInGroup){
                        alert("You can't add this doctor. It is already available in group.")
                        _this.setState({providerName:'', providePhoneNum:''})
                      }else{

                          var arrnonadmin = []
                          arrnonadmin.push(parseInt(doctDetails.phone))
                          var currentDate  = Moment()
                          createGroupChannelName = "group" + "_" + _this.state.doctorPhoneNum + "_" +currentDate

                          var addOtherDoctor = {name:_this.state.groupName, 
                              phone:doctDetails.phone, type: 'group', block:false, 
                              channelname:createGroupChannelName, admin:_this.state.arrAdmin, nonadmin:arrnonadmin
                              ,blockpatient:false, blockdoctor:false, blocksubscription:false,
                              groupchannelname:newGroupChannelName, image:''}
                        
                          _this.setState({arrNonadmin:arrnonadmin})
                          _this.addValueInFirebaseForAdmin(addOtherDoctor)
                          _this.addValueInFirebaseForNonAdmin(addOtherDoctor, arrnonadmin)
                        }
                      }
                    })
                  }
                }
              }, err => {
        console.log(`Encountered error: ${err}`);
        });
}

findOtherDoctorDetail(doctDetails){

  this.setState({loader:true})
  let phoneNumber = parseInt(this.state.providePhoneNum)
    var _this = this;

      var arrProvName = this.state.providerName.split(' ')
      var splitProviderName = this.state.providerName
      if(arrProvName.length > 1){
        splitProviderName = arrProvName[1]
      }
      var arrnonadmin = []
      var newGroupChannelName = this.state.groupChannelName

      newGroupChannelName = newGroupChannelName + "," + splitProviderName
      this.setState({groupChannelName:newGroupChannelName})

      if(_this.state.arrNonadmin){
      if(_this.state.arrNonadmin.length > 0){
        arrnonadmin = _this.state.arrNonadmin
        arrnonadmin.splice(0, 0, parseInt( doctDetails.phone));
      }
      }else{
      arrnonadmin.push(parseInt(doctDetails.phone))
      }
      
      var addOtherDoctor = {name:_this.state.groupName, 
          phone:doctDetails.phone, type: 'group', block:false, 
          channelname:_this.state.channelName, admin:_this.state.arrAdmin, nonadmin:arrnonadmin
          ,blockpatient:false, blockdoctor:false,blocksubscription:false, groupchannelname:newGroupChannelName, image:''}
    
      _this.setState({arrNonadmin:arrnonadmin})
      _this.updateValueInFirebaseForNonAdmin(arrnonadmin, newGroupChannelName)
      _this.updateValueInFirebaseForAdmin(arrnonadmin, newGroupChannelName)

      let doc = firebase.firestore().collection('Chatlist').doc(phoneNumber);

        doc.get().then(docSnapshot => {
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
           
            var arrData = []
            _this.setState({loader:false})
            var checkDatapresence = false
              if(docSnapshot.data().channels.length > 0){
                  arrData = [...docSnapshot.data().channels]
                  docSnapshot.data().channels.map((item, index)=>{
                    if(item.channelname === _this.state.channelName){
                      arrData[index] = addOtherDoctor
                      checkDatapresence = true
                    }else{
                      arrData[index] = item
                    }
                  })
              }
            if(!checkDatapresence){
              arrData.push(addOtherDoctor)
            }
            _this.addChannelInFirebase(phoneNumber, arrData, false)
        }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
}

addChannelInFirebase(key, arrData, checkNewGroup){

  this.setState({loader:true})
    var _this = this;
    chatlistRef.doc(key).set({
                  channels:arrData
                }).then(async()=>{
        _this.setState({loader:false})
      if(checkNewGroup){
        _this.props.navigation.goBack()
      }
        if(key === parseInt(this.state.doctorPhoneNum)){
            _this.setState({loader:false, showModal:false, providerName:'', message:''})
        }else{
            _this.setState({loader:false, showModal:false,
              providerName:'', message:''})
        }
      })
}

unsubscribeChannel(channel){
  console.log("unsubscribe channel ....", channel)
  ChatEngineProvider._pubnub.unsubscribe({
      channels: [channel]
  });
}

updateNonAdminValueInFirebase(arrnonadmin, emptyNonAdmin, groupChannelName) {

  var _this = this;
  if(emptyNonAdmin){

    let doc = firebase.firestore().collection('Chatlist').doc(this.state.doctorPhoneNum);

        doc.get().then(docSnapshot => {
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
           
            if(docSnapshot.data().channels.length > 0){
                var arrChannel = docSnapshot.data().channels
                arrChannel.map((channel, index)=>{
                    if(channel.channelname === _this.state.channelName){
                    _this.updateChannelInFirebase(_this.state.doctorPhoneNum, index, arrnonadmin, arrChannel, groupChannelName)
                  }
                })
              }
            }
          }, err => {
        console.log(`Encountered error: ${err}`);
        });
  }else{
    this.state.arrNonadmin.map((item)=>{

      let doc = firebase.firestore().collection('Chatlist').doc(item);

        doc.get().then(docSnapshot => {
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
            if(docSnapshot.data().channels.length > 0){
                var arrChannel = docSnapshot.data().channels
                arrChannel.map((channel, index)=>{
                    if(channel.channelname === _this.state.channelName){
                      _this.updateChannelInFirebase(item.toString(), index, arrnonadmin, arrChannel, groupChannelName)
                    }
                  })
                }
              }
            }, err => {
        console.log(`Encountered error: ${err}`);
      });
    })
  }
}

addValueInFirebaseForAdmin(addDoctor){

  var _this = this;
  this.state.arrAdmin.map((item)=>{

    let doc = firebase.firestore().collection('Chatlist').doc(item.toString());

        doc.get().then(docSnapshot => {
         var arrData = []
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
           let data = docSnapshot.data();
            if(docSnapshot.data().channels.length > 0){
               arrData = data.channels
            }
        }
        arrData.push(addDoctor)
        _this.addChannelInFirebase(item.toString(), arrData, true)
        }, err => {
          console.log(`Encountered error: ${err}`);
        });
    })
}

addValueInFirebaseForNonAdmin(addDoctor, arrnonadmin){

  var _this = this;
  arrnonadmin.map((item)=>{

    let doc = firebase.firestore().collection('Chatlist').doc(item.toString());

    doc.get().then(docSnapshot => {
    var arrData = []
    if(docSnapshot.data() === undefined){
       console.log("jflks jj empty")
    }else{
       let data = docSnapshot.data();
       if(docSnapshot.data().channels.length > 0){
           arrData = data.channels
        }
     }
    arrData.push(addDoctor)
    _this.addChannelInFirebase(item.toString(), arrData, true)
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
  })
}

updateValueInFirebaseForAdmin(arrnonadmin, newGroupChannelName) {

  this.setState({loader:true})
  var _this = this;
  this.state.arrAdmin.map((item)=>{

    let doc = firebase.firestore().collection('Chatlist').doc(item.toString());

        doc.get().then(docSnapshot => {
       _this.setState({loader:false})
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
             var arrChannel = docSnapshot.data().channels  
            if(docSnapshot.data().channels.length > 0){
                arrChannel.map((channel, index)=>{
                  if(channel.channelname === _this.state.channelName){
                  _this.updateChannelInFirebase(item.toString(), index, arrnonadmin, arrChannel, newGroupChannelName)
                }
              })
            }
        }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
  })
}

updateValueInFirebaseForNonAdmin(arrnonadmin, newGroupChannelName) {

  this.setState({loader:true})
  var _this = this;

  this.state.arrNonadmin.map((item)=>{

    let doc = firebase.firestore().collection('Chatlist').doc(item.toString());

        doc.get().then(docSnapshot => {
       _this.setState({loader:false})
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
             var arrChannel = docSnapshot.data().channels  
            if(docSnapshot.data().channels.length > 0){
                arrChannel.map((channel, index)=>{
                  if(channel.channelname === _this.state.channelName){
                      _this.updateChannelInFirebase(item.toString(), index, arrnonadmin, arrChannel, newGroupChannelName)
                }
              })
            }
        }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
  })
}

updateChannelInFirebase(phoneNum, key, arrnonadmin, arrChannel, newGroupChannelName){

    this.setState({loader:true})
    var _this = this;
    var arrData = [...arrChannel]
    var currentChannel = arrChannel[key]
    const keys = Object.keys(currentChannel)

      currentChannel['type']='group'
      if(keys.includes('nonadmin')) {
        currentChannel['nonadmin']= arrnonadmin
      }else{
        let nonAdmin = {'nonadmin':arrnonadmin}
        currentChannel.push(nonAdmin)
      }
      currentChannel['groupchannelname'] = newGroupChannelName
      arrData[key]= currentChannel
     
      chatlistRef.doc(phoneNum).set({
                    channels:arrData
                  }).then(async()=>{
          _this.setState({loader:false})
          if(key === parseInt(this.state.doctorPhoneNum)){
              _this.setState({loader:false, showModal:false, providerName:'', message:''})
          }else{
              _this.setState({loader:false, showModal:false,
                providerName:'', message:''})
          }
        })
}

leaveChatAction() {

    var arrnonadmin;
    let emptyNonAdmin;
    var nonAdmin = [...this.state.arrNonadmin]
    var index = nonAdmin.indexOf(parseInt(this.state.doctorPhoneNum))
    if(index !== -1){
      nonAdmin.splice(index, 1)
      var modifiedChannelName = this.state.groupChannelName
      const mySplitedString = this.state.doctorName.split(' ');
      if(this.state.groupChannelName.includes(mySplitedString[1])){
        modifiedChannelName = modifiedChannelName.replace(mySplitedString[1],'')
      }else if(this.state.groupChannelName.includes(mySplitedString[2])){
        modifiedChannelName = modifiedChannelName.replace(mySplitedString[1],'')
      }
      modifiedChannelName = modifiedChannelName.replace(",,",",")
      var lastChar = modifiedChannelName[modifiedChannelName.length -1];
      if(lastChar === ","){
        modifiedChannelName = modifiedChannelName.slice(0, -1);
      }
    }
    if(nonAdmin.length ===0){
      arrnonadmin=''
      emptyNonAdmin= true
    }else{
      arrnonadmin=nonAdmin
      emptyNonAdmin= false
    }
    this.setState({arrNonadmin:nonAdmin})

    this.updateValueInFirebaseForAdmin(arrnonadmin, modifiedChannelName)
    this.unsubscribeChannel(this.state.channelName)
    AsyncStorage.getItem('token').then((token)=>{
      this.removeChannelFromPush(this.state.channelName, token)
  })
    this.updateNonAdminValueInFirebase(arrnonadmin, emptyNonAdmin, modifiedChannelName)
    this.props.navigation.goBack()
}

removeChannelFromPush(channel, token) {
  this.setState({loader:true})
  var platformType = ''
  if(Platform.OS === 'ios'){
      platformType='apns'
  }else{
      platformType='gcm'
  }
  ChatEngineProvider._pubnub.push.removeChannels({
      channels: [channel],
      device: token,
      pushGateway: platformType, // apns or gcm
      }, (status) => {
      this.setState({loader:false})
      if (status.error) {
          console.log('operation failed w/ status: remove channel', status);
      } else {
          console.log('removeChannelFromPush done!');
      }
      });
}

openOptions(){
  this.ActionSheet.show()
}

async selectedImageAction(index){

  if(index === 0){

    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      })
      console.log("response of document....")
      console.log(res)
     
      if (res) {
        try {
          this.setState({loader:true})
          var uri = res.uri.replace(('%20'),'')
          const base64 = await RNFS.readFile(uri, "base64");
          this.fileUploadToFirebase(base64, res.name)

        } catch (read_file_err) {
          this.setState({loader:false})
          console.log("error reading file: ", read_file_err);
        }
      }      
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }else if(index === 1){
    ImagePicker.openPicker({
          cropping: false,
          includeBase64: true,
          mediaType: 'photo',
          compressImageQuality:0.1
        }).then(async image => {
          this.fileUploadToFirebase(image.data, image.filename )
        });
  }
  else if(index === 2){
    ImagePicker.openCamera({
            cropping: false,
            includeBase64: true,
            mediaType: 'photo',
            compressImageQuality:0.1
          }).then(image => {
            var rString = this.randomString(6, '0123456789');
            rString = "IMG_" + rString + '.JPG'
            this.fileUploadToFirebase(image.data, rString )
          });
  }
}

randomString(length, chars) {
  var result = '';
  for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}

async fileUploadToFirebase(base64, filename){

  this.setState({loader: true});

  let body = {
    "file": base64,
    "channel_name": this.state.channelName,
    "file_name": filename
  };

  try {
      fetch('https://us-central1-aepiphany-bdba7.cloudfunctions.net/charge/upload', {
          method: 'post',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
          },
          body: JSON.stringify(body)
      }) .then(response => response.json())
      .then(responseJson => {
         this.setState({loader: false});
          console.log("response firebase upload file")
          console.log(responseJson)
          
          if(responseJson.status){
            var msg_data;
          var rString = this.randomString(15, '0123456789abcdefghijklmnopqrstuvwxyz-');
          rString = rString + new Date()
        
          if(this.state.type === "group"){
             msg_data = {
               _id:  sha1(rString),
              text:'',
              fileName:responseJson.data.name,
                createdAt: new Date(),
                user: {
                  _id: this.props.navigation.state.params.doctorId,
                  name:this.state.senderName,
                  avatar:this.state.senderImage,
                  type:this.props.navigation.state.params.ownerType
                },
              };
          }else{
             msg_data = {
              _id:  sha1(rString),
              text:'',
              fileName:responseJson.data.name,
                createdAt: new Date(),
                user: {
                  _id: this.props.navigation.state.params.doctorId
                },
              };
          }
          let data = msg_data
          data = {...data, image: responseJson.data.mediaLink}
          this.onSend([data], responseJson.data.contentType)

          if(responseJson.data.contentType.includes('image') || 
             responseJson.data.contentType.includes('audio') || 
             responseJson.data.contentType.includes('video')) {

            this.state.arrAdmin.map((item, index)=>{
              this.saveImageFileInAttachments(responseJson.data.mediaLink, 
                responseJson.data.contentType, item, filename)

                if(index === this.state.arrAdmin.length - 1){
                  if(this.state.arrNonadmin.length > 0){
                    this.state.arrNonadmin.map((item)=>{
                      this.saveImageFileInAttachments(responseJson.data.mediaLink, 
                        responseJson.data.contentType, item, filename)
                    })
                  }
                }
            })
          }else{
            let doc = firebase.firestore().collection('Attachments').
            doc(this.props.navigation.state.params.doctorId).collection('files');

               doc.add({
                name:filename,
                mediaLink:responseJson.data.mediaLink,
                contentType:responseJson.data.contentType,
                senderId:parseInt(this.props.navigation.state.params.doctorId),
        }).then(async()=>{
        })
      }
    }
  }).catch(e => {
         this.setState({loader: false});
         console.log("error ...",e)
      }); 
     }catch (error) {
      // Error retrieving data
      console.log("error kjjl", error)
     }
 }

 saveImageFileInAttachments(mediaLink, contentType, id, filename){

  let doc = firebase.firestore().collection('Attachments').doc(id.toString()).collection('files');
       doc.add({
        name:filename,
        mediaLink:mediaLink,
        contentType:contentType,
        senderId:parseInt(this.props.navigation.state.params.doctorId),
  }).then(async()=>{
  })
 }

  providerSelectedAction(item) {
    this.setState({providerName:item, arrFilteredNames:[]})
  }

  searchForName(providerName) {
    this.setState({providerName:providerName})
    if(providerName.length > 0){      
       var filtered = []
       this.state.arrDoctorNames.map((value, index)=>{
        if(value.includes(providerName)){
          filtered.push(value)
        }
        if(index === this.state.arrDoctorNames.length - 1){
          this.setState({arrFilteredNames:filtered})
        }
      })
    }else{
      this.setState({arrFilteredNames:[]})
    }
  }

openFile(filename) {
    AsyncStorage.getItem(filename).then((value)=>{
       const source = {uri:value, cache:true};
       this.setState({attachImageUrl:source, showPdf:true})
    })
}

openAudioVideoFile(link) {

  Linking.canOpenURL
        (link).then(supported => {
            console.log("supported value - ")
            console.log(supported)
            if (supported) {
              Linking.openURL(link);
            } else {
              console.log("Don't know how to open URI: " + this.state.doctorDetail.website);
            }
          });
}

performActionAfterPdfLoaded(numberOfPages,filePath){
    if(this.state.fileNameonServer.length > 0){
      AsyncStorage.setItem(this.state.fileNameonServer,filePath)
    }
}

pressAvatarAction(user){
    
      var _this = this;
      if(user.type === 'doctor'){

        doctorRef.where('phone', '==', parseInt(user._id)).get()
            .then(snapshot => {
                _this.setState({loader:false})
                if (snapshot.empty) {
                    console.log('No matching documents.');                                      
                  }  else{
                      snapshot.forEach(doc => {
                          _this.props.navigation.navigate('ProfileDoctor', {otherDoctorProfile:doc.data()})
                        });
                      }
                    });
      }else if(user.type === 'patient'){

        userRef.where('phone', '==', parseInt(user._id)).get()
                    .then(snapshot => {
            _this.setState({loader:false})
                if (snapshot.empty) {
                    console.log('No matching documents.');                                          
                  }  else{
                    snapshot.forEach(doc => {
                    _this.props.navigation.navigate('ProfilePatient', {otherPatientProfile:doc.data()})
                  });
                }
              });
      }
}

downloadFile(uri, messagedetail){

  this.setState({loader:true})
  var uploadProgress = (response) => {
    var percentage = Math.floor((response.bytesWritten/response.contentLength) * 100);
  };

  RNFS.downloadFile({
    fromUrl: uri.currentMessage.image,
    toFile: RNFS.DocumentDirectoryPath + 'gallery',
    progress: uploadProgress
    })
    .promise
    .then((response) => {

      console.log("resposne for file download ..")
      console.log(response)
      this.setState({loader:false})
        if (response.statusCode == 200) {

          const source = {uri:uri.currentMessage.image,cache:true};
          this.setState({attachImageUrl:source,showPdf:true,
            fileNameonServer:messagedetail.currentMessage.fileName, loader:false})
            if(parseInt(uri.currentMessage.user._id) !== parseInt(this.props.navigation.state.params.doctorId)){

              let doc = firebase.firestore().collection('Attachments').
               doc(this.props.navigation.state.params.doctorId).collection('files');

               doc.add({
                  name:uri.currentMessage.fileName,
                  mediaLink:uri.currentMessage.image,
                  contentType:uri.currentMessage.type,
                  senderId:parseInt(uri.currentMessage.user._id)
                }).then(async()=>{
                })
              }            
        } else {
          alert('Error', 'Something went wrong while trying to download file');
        }
      }
    )
    .catch((download_err) => {
      console.log('error downloading file: ', download_err);
    });
}

_renderItem = ({item}) => {
    
  return(
    <TouchableOpacity style={{height:30
    }}onPress={()=> this.providerSelectedAction(item)}>
       <Text>
       {item}
       </Text>
    </TouchableOpacity>
  )
}

renderUIForDetail(){

      return(
          <View style={{flexDirection:'column'}}>
              <TextField 
                size = {13}
                weight = '200'
                width = '80%'
                image = {require('../../assets/name-icon.png')}
                placeholderText = "Name"
                height = {50}
                textWidth='90%'
                value = {this.state.providerName}
                onChangeText = {(providerName)=> this.searchForName(providerName)}
                keyboard = 'default'
              />
              <BottomBorderView 
              horizontal={0}
              top={0}
              />
            {
              this.state.arrFilteredNames.length > 0 ? (
                <View style={{height:100,
                  marginTop:10}}>
                      <FlatList
                          data = {this.state.arrFilteredNames}
                          extraData={this.state}
                          renderItem = {this._renderItem} />

                  </View>
              ) : (null)
            }
        </View>
      )
}

renderCustomActions = () => {

if(this.state.blockedChat){
  return (
    <View style={styles.blockCustomActionsContainer}>
       <Text>
         Chat is disabled
        </Text>          
    </View>
  );
}else{
  return (
    <View style={styles.customActionsContainer}>
      <TouchableOpacity onPress={()=>this.openOptions()}>
        <View style={styles.buttonContainer}>
            <Image  source={require('../../assets/attach_file.png')} />
        </View>
      </TouchableOpacity>
    </View>
  );
}
}

renderMessage = (msg) => {
  
  var renderBubble;
  if(msg.currentMessage.type){
    if(msg.currentMessage.type === 'application/pdf' || msg.currentMessage.type.includes('audio')
    || msg.currentMessage.type.includes('video')){
      renderBubble = this.renderPreview.bind(this, msg)
    }else{
      renderBubble = null
    }
  }else{
    renderBubble = null
  }
  const modified_msg = {
    ...msg,
    renderBubble
  }
  if(renderBubble !== null){
    return <Message {...modified_msg} />
  }else{
    return <Message {...msg} />

  }
}

renderPreview = (uri, bubbleProps) => {
  
  const modified_bubbleProps = {
    ...bubbleProps
  };
  var attachmentDownloaded = false
  const keys = Object.keys(bubbleProps.currentMessage)

   if(keys.includes('fileName')) {
    if(this.state.asyncStorageKeys.includes(bubbleProps.currentMessage.fileName)){
            attachmentDownloaded = true
          }     
    }
    return (
      <ChatBubble {...modified_bubbleProps}>
        <TouchableOpacity style = {{alignItems:'center', marginTop:-10}} onPress={() => {
          {
            attachmentDownloaded ? (
              bubbleProps.currentMessage.type.includes('audio') || 
              bubbleProps.currentMessage.type.includes('video') ? (
                this.openAudioVideoFile(bubbleProps.currentMessage.image)
              ) :
              this.openFile(bubbleProps.currentMessage.fileName)
            ) : (
              bubbleProps.currentMessage.type.includes('audio') || 
              bubbleProps.currentMessage.type.includes('video')? (
                this.openAudioVideoFile(bubbleProps.currentMessage.image)
              ) :
              this.downloadFile(uri, bubbleProps)
            )
          }  
        }}>
          {
            bubbleProps.currentMessage.type.includes('audio') ? (
              <Image style={{width:50, height:50}} 
                     source={require('../../assets/audio.png')}>
              </Image>
            ) : (
              bubbleProps.currentMessage.type.includes('video') ? (
                <Image style={{width:50, height:50}} 
                       source={require('../../assets/video.png')}>
                </Image>
              ) : (
                <Image source={require('../../assets/pdf.png')}>
                </Image>
              )
            )
          }
           {
              bubbleProps.currentMessage.type.includes('pdf') ? (
                <View style={styles.downloadButton}>
            {
              attachmentDownloaded ? (
                <Text style={[styles.link, { color: 'red' }]}>Open file</Text>
  
              ) : (
                <Text style={[styles.link, { color: 'white' }]}>Download</Text>
              )
            }
          </View>
              ) : (null)
           }
        </TouchableOpacity>
      </ChatBubble>
    );
}

  render() {
    
    const navigate = this.props.navigation.navigate;
    const userType = this.state.type === "patient" ? 'patient' : 'doctor' 

    return (
      <View style={{ flex: 1, backgroundColor:'white' }}>
      {
        this.state.showPdf ? (

          <Modal animationType="fade" transparent={true} visible= {true}>
                  <View style={{
                        height:'100%',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.60)',
                        width:DEVICE_WIDTH
                    }}>
                       <View style={{
                         marginTop:30, 
                         flexDirection:'column', 
                         backgroundColor:'white',
                         borderRadius:5,
                        height:DEVICE_HEIGHT-60,
                         width:DEVICE_WIDTH,
                         alignItems:'center'}}>
                            <TouchableOpacity style={{ 
                            position:'absolute', 
                            right:0,  
                            width:60,
                            height:70,
                            justifyContent:'center',
                            alignItems:'center',
                        }} 
                            onPress={()=> this.setState({showPdf:false})}>
                                        <Image source={require('../../assets/Close.png')} />
                         </TouchableOpacity>

              <View style={{marginTop:50}}>
                <Pdf
                    source={this.state.attachImageUrl}
                    onLoadComplete={(numberOfPages,filePath)=>{
                      this.performActionAfterPdfLoaded(numberOfPages,filePath)                       
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        console.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        console.log(error);
                    }}
                    style={styles.pdf}/>
               </View>
                         </View>
                </View>
            </Modal> 
        ) : (null)
      }
      {
        this.state.showModal ? (
            <KeyboardAvoidingView behavior={'padding'} enabled>

            <Modal animationType="fade" transparent={true} visible= {true}>
                  <View style={{alignItems:'center'}}>
                <View style={{
                        height:'100%',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.60)'
                    }}>
                      {this.state.loader ? <MyActivityIndicator /> : null}
    
                    <View style={{
                         marginTop:40, 
                         marginHorizontal:30, 
                         flexDirection:'column', 
                         backgroundColor:'white',
                         borderRadius:5,
                         width:DEVICE_WIDTH-60,
                         alignItems:'center'}}>
                        <Text style={{marginTop:20,
                        fontSize:16}}>
                            New Conversation
                        </Text>
                        <TouchableOpacity style={{ 
                            position:'absolute', 
                            right:15,  
                            marginTop:20}} 
                            onPress={()=> this.setState({showModal:false})}>
                                        <Image source={require('../../assets/Close.png')} />
                        </TouchableOpacity>
                        <BottomBorderView 
                            horizontal={10}
                            width='100%'
                            top={15}
                            />
                        <Text style={{fontSize:15,
                         marginTop:10 }}>
                            Add Provider
                         </Text>
                      {
                          this.renderUIForDetail()
                      }
                     {
                              <Button
                              top = {40}
                              radius = {20}
                              backgColor = '#F1424F'
                              height = {40}
                              textColor = 'white'
                              titleSize = {16}
                              title = 'Send message'
                              width='80%'
                              bottom = {20}
                              buttonAction = {()=>this.sendMessageAction()}
                          />
                     }
                    </View>
                </View>
                </View>
            </Modal> 
            </KeyboardAvoidingView>
       ) : (null)
    }
        {this.state.loader ? <MyActivityIndicator /> : null}
    {
      this.props.navigation.state.params.ownerType === "doctor" ? (

        this.state.isGroupAdmin ? (
          <Header title={this.state.type === 'group' ? (this.state.groupChannelName)
           : (this.state.otherUsername)} withBackButton=
          {() => this.backButtonAction()} type={userType} 
                  navigation={navigate} block={this.state.blockedChat}
                  ADDDOCTOR={() => this.setState({showModal:!this.state.showModal})}/>
        ) : (
          <Header title={this.state.type === 'group' ? (this.state.groupChannelName)
          : (this.state.otherUsername)} withBackButton=
          {() => this.backButtonAction()} type={userType} 
                  navigation={navigate} block={this.state.blockedChat}
              leave={() => this.leaveChatAction()}/>
        )
      ) : (
        <Header title={this.state.type === 'group' ? (this.state.groupChannelName)
        : (this.props.navigation.state.params.otherUsername)} withBackButton=
        {() => this.backButtonAction()} type={userType} 
                navigation={navigate}
            />
      )
    }     
     <ActionSheet
        ref={o => this.ActionSheet = o}
        title={'Choose option'}
        options={['Document', 'Gallery', 'Camera', 'Cancel']}
        cancelButtonIndex={3}
        destructiveButtonIndex={3}
        onPress={(index) => { this.selectedImageAction(index)}}
    />
      {
        this.state.type === "group" ? (
          <GiftedChat
          messages={this.state.messages}
          renderUsernameOnMessage={true}
          onSend={message => this.onSend(message)}
          onPressAvatar={user => this.pressAvatarAction(user)}
          user={{
            _id: this.props.navigation.state.params.doctorId,
            name:this.state.senderName,
            avatar:this.state.senderImage,
            type:this.props.navigation.state.params.ownerType
          }}
            renderActions={this.renderCustomActions}
            renderMessage={this.renderMessage.bind(this)}
        />
        ) : (
            <GiftedChat
            messages={this.state.messages}
            onSend={message => this.onSend(message, 'text')}
            renderAvatar={() => null}
            showAvatarForEveryMessage={true}
            user={{
              _id: this.props.navigation.state.params.doctorId
            }}
            renderActions={this.renderCustomActions}
            renderMessage={this.renderMessage.bind(this)}
          />         
        )
      }
      </View>
    );
  }
}

const styles = {
  customActionsContainer: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  blockCustomActionsContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor:'lightgray',
    width:'100%',
    height:50,
    alignItems:'center',
    justifyContent:'center'
  },
  buttonContainer: {
    padding: 10
  },
  link: {
    fontSize: 12
  },
  downloadButton: {
    padding: 5,
    alignItems: 'center',
  },
  pdf: {
    flex:1,
    width:Dimensions.get('window').width,
    height:Dimensions.get('window').height,
}
}