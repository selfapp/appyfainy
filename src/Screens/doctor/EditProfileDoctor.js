


import React,{Component} from 'react';
import {View, Text,TouchableOpacity, ScrollView,Image, 
    Platform, Keyboard,
      Dimensions} from 'react-native';
import BottomBorderView from '../../Components/BottomBorderView';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import MyActivityIndicator from '../../Components/activity_indicator';
import firebase from 'react-native-firebase';
import EditProfileTextField from '../../Components/EditProfileTextField';
import DatePicker from 'react-native-datepicker';
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../../Components/Header';

const doctorRef = firebase.firestore().collection('Doctors');
const chatlistRef = firebase.firestore().collection('Chatlist');
const DEVICE_WIDTH = Dimensions.get("window").width;
const days = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su']

export default class EditProfileDoctor extends Component {

    constructor(){
        super();
        this.state = {
            loader:false,
            amount:'',
            experience:'',
            profileImage:'',
            profileImageUri:'',
            location:'',
            name:'',
            lName:'',
            phone:'',
            expertise:[],
            speciality:'',
            website:'',
            doctorKey:'',
            arrSelectedDays:[],
            startTime:'',
            endTime:'',
            checked: true,
            imageName:'',
            imageBase64:'',
            phoneNum:''
        }
    }

    componentDidMount(){

        var _this = this;
        this.keyboardSub = Keyboard.addListener('keyboardWillShow', ()=> {
            this.scrollView.scrollToEnd({ animated: true })
          })
        AsyncStorage.getItem('phonenumber').then((phoneNum)=>{
            
            let phoneNumber = parseInt(phoneNum)
            _this.setState({loader:true, phoneNum:phoneNum})
            doctorRef.where('phone', '==', phoneNumber).get()
            .then(snapshot => {
             _this.setState({loader:true})
                if (snapshot.empty) {
                    console.log('No matching documents.');                                      
                  }  else{
                    snapshot.forEach(doc => {
                        let doctorDetails = doc.data()
                            if(doctorDetails.days){
                                if(doctorDetails.days.length > 0){
                                    _this.setState({arrSelectedDays:doctorDetails.days})
                                }
                            }
                            _this.setState({
                                name:doctorDetails.name,
                                speciality:doctorDetails.speciality,
                                location:doctorDetails.location,
                                experience:doctorDetails.experience.toString(),
                                website:doctorDetails.website,
                                expertise:doctorDetails.services,
                                amount:doctorDetails.amount,
                                phone:doctorDetails.phone,
                                profileImage:doctorDetails.image,
                                profileImageUri:doctorDetails.image,
                                startTime:doctorDetails.starttime,
                                endTime:doctorDetails.endtime,
                                loader:false
                            })
                });
            }
        });
    })
}

    componentWillUnmount() {
        this.keyboardSub.remove()
      }

    showActionSheet() {
        this.ActionSheet.show()
    }

    randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
      }

    takeImageAction(index){

        if(index === 0){
            ImagePicker.openCamera({
                cropping: false,
                includeBase64: true,
                mediaType: 'photo',
                compressImageQuality:0.1
              }).then(image => {
                let data = `data:${image.mime};base64,${image.data}`
                var rString = this.randomString(6, '0123456789');
                rString = "IMG_" + rString + '.JPG'
                this.setState({profileImage:data, imageBase64:image.data,
                    imageName:rString})
              });
        }else if(index === 1){
            ImagePicker.openPicker({
                cropping: false,
                includeBase64: true,
                mediaType: 'photo',
                compressImageQuality:0.1
              }).then(image => {
                
                let data = `data:${image.mime};base64,${image.data}`
                if(Platform.OS === 'ios'){
                    this.setState({imageName:image.filename})
                }else{
                    var rString = this.randomString(6, '0123456789');
                    rString = "IMG_" + rString + '.JPG'
                    this.setState({imageName:rString})
                }
                this.setState({profileImage:data, imageBase64:image.data})
              });
        }
    }

    async fileUploadToFirebase(){

        this.setState({loader: true});
      
        let body = {
          "file": this.state.imageBase64,
          "user_id": this.state.phoneNum,
          "file_name": this.state.imageName
        };
        try {
            fetch('https://us-central1-aepiphany-bdba7.cloudfunctions.net/charge/uploadProfilePicture', {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }) .then(response => response.json())
            .then(responseJson => {
               this.setState({loader: false});
                console.log("response firebase upload file")
                console.log(responseJson)

                this.state.arrSelectedDays.sort(function(a, b){return a-b});

                this.setState({loader:true})
                doctorRef.doc(this.state.phoneNum).set({
                    amount:this.state.amount,
                    name:this.state.name,
                    phone:this.state.phone,
                    services:this.state.expertise,
                    speciality:this.state.speciality,
                    location:this.state.location,
                    experience:this.state.experience,
                    website:this.state.website,
                    image:responseJson.data.mediaLink,
                    days:this.state.arrSelectedDays,
                    starttime:this.state.startTime,
                    endtime:this.state.endTime
                    }).then(async()=>{
                        this.setState({loader:false})
                        AsyncStorage.setItem('doctorname',this.state.name)

                        this.findDoctorChatlist(responseJson.data.mediaLink)
                        this.props.navigation.navigate('ProfileDoctor')
                    })
                if(responseJson.statusCode === 200){
                }
            })
            .catch(e => {
               this.setState({loader: false});
               console.log("error ...",e)
            }); 
           }catch (error) {
            // Error retrieving data
            console.log("error kjjl", error)
           }
       }

   async findDoctorChatlist(imageLink){

    var _this = this;

    let doc = firebase.firestore().collection('Chatlist').doc(this.state.phoneNum);

        doc.get().then(docSnapshot => {
        if(docSnapshot.data() === undefined){
           console.log("jflks jj empty")
        }else{
            if(docSnapshot.data().channels.length > 0){
               _this.findListOfPatient(docSnapshot.data().channels, imageLink)
            }
        }
        }, err => {
        console.log(`Encountered error: ${err}`);
        });
    }

    async findListOfPatient(channels, imageLink){

        var _this = this;

        channels.map((item, index)=>{

            if(item.type === 'patient'){
                var channelN = item.channelname
               channelN = channelN.replace('_','')
               channelN = channelN.replace(this.state.phoneNum, '')

               let doc = firebase.firestore().collection('Chatlist').doc(channelN);

                doc.get().then(docSnapshot => {
                if(docSnapshot.data() === undefined){
                   console.log("jflks jj empty")
                }else{
                    if(docSnapshot.data().channels.length > 0){
                        _this.updateImageInChatlistForPatient(docSnapshot.data().channels, imageLink, channelN)
                    }
                }
                }, err => {
                console.log(`Encountered error: ${err}`);
                });
            }
        })
    }

    async updateImageInChatlistForPatient(channels, imageLink, channelN){

        var updatedChannels = [...channels]
        channels.map((item, index)=>{

            if(item.type === 'patient'){
                var channelToChange = updatedChannels[index]
                channelToChange['image'] = imageLink
                updatedChannels[index] = channelToChange
                chatlistRef.doc(channelN).set({
                          channels:updatedChannels
                        }).then(async()=>{
                    })
            }
        })
    }

    backButtonAction(){
        this.props.navigation.goBack()
    }
    
    changeCheckState(index) {

        var newIndex = index
          if(this.state.arrSelectedDays.indexOf(newIndex) === -1) {
              var newArr = this.state.arrSelectedDays;
              newArr.push(newIndex)
              this.setState({arrSelectedDays:newArr})
        }else{
          const filteredItems = this.state.arrSelectedDays.filter(item => item !== newIndex)
          this.setState({arrSelectedDays:filteredItems})
        }
      }

    saveProfileAction(){

        if(this.state.arrSelectedDays.length === 0){
          alert("Please select at least one day.")
        }else if(this.state.startTime.length === 0){
            alert("Please select start time")
        }else if(this.state.endTime.length === 0){
            alert("Please select end time")
        }else if(this.state.name.length === 0){
            alert("Please enter name.")
        }
        else if(this.state.speciality.length === 0){
            alert("Please enter your speciality.")
        }else if(this.state.location.length === 0){
            alert("Please enter location.")
        }else if(this.state.experience.length === 0){
            alert("Please enter your experience.")
        }else if(this.state.website.length === 0){
            alert("Please enter website.")
        }else {

            this.setState({loader:true})
            if(this.state.imageName.length > 0){
                this.fileUploadToFirebase()
            }else{
                this.state.arrSelectedDays.sort(function(a, b){return a-b});

                doctorRef.doc(this.state.phoneNum).set({
                    amount:this.state.amount,
                    name:this.state.name,
                    phone:this.state.phone,
                    services:this.state.expertise,
                    speciality:this.state.speciality,
                    location:this.state.location,
                    experience:this.state.experience,
                    website:this.state.website,
                    image:this.state.profileImageUri,
                    days:this.state.arrSelectedDays,
                    starttime:this.state.startTime,
                    endtime:this.state.endTime
                    }).then(async()=>{
                        this.setState({loader:false})
                        AsyncStorage.setItem('doctorname',this.state.name)
                        this.props.navigation.navigate('ProfileDoctor')
                    })
            }
        }
    }

    renderOfficeHoursUI(){

        return(

            <View style={{
                marginHorizontal:10,
                marginTop:-80,
                width:DEVICE_WIDTH - 20,
                backgroundColor:'white',
                borderRadius:5,
                alignItems:'center',
                borderColor:'lightgray', 
                borderWidth:0.5,elevation: 5,
                shadowColor: '#999666', 
                shadowOffset: {width: 5, height: 5},
                shadowOpacity: 0.7, shadowRadius: 5,
            }}>
                <Image style={{
                marginTop:-40,
                height:80,
                width:80,
                borderRadius:40,
                borderColor:'#F64351',
                borderWidth:2,
            }} source={this.state.profileImage ? {uri:this.state.profileImage}  : 
                     require('../../assets/avatar.png')}>
            </Image>
               <TouchableOpacity style={{marginTop:15}} onPress={()=>this.showActionSheet()}>
                <Text>
                        Upload your photo
                </Text>
               </TouchableOpacity>
                
                <BottomBorderView 
                width='100%'
                top={10}
                />
                    <Text style={{width:DEVICE_WIDTH-40,
                    marginTop:10,
                    color:'#F64351' }}>
                            Office hours
                    </Text>
                
                <View style={{
                    height:80,
                    width:'100%',
                    flexDirection:'row',
                    justifyContent:'space-evenly',
                    alignItems:'stretch'
                }}>
                     {
                    days.map((value, index)=>{
                        return(
                            <View style={{
                                width:20, 
                                height:20, 
                                marginLeft:10, 
                                marginTop:10}}>
                                    <View style={{flexDirection:'row',
                                            justifyContent:'center', 
                                            }}>
                                        <Text style={{color:'gray', fontSize:13}}>
                                            {value}
                                        </Text>
                                    </View>
                                    <BottomBorderView 
                                        horizontal={0}
                                        top={10}
                                        />
                                    <TouchableOpacity style={{flexDirection:'row',
                                        justifyContent:'center',
                                        marginTop:10
                                        }} onPress={()=>this.changeCheckState(index+1)}>
                                            {
                                                this.state.arrSelectedDays ? (
                                                    this.state.arrSelectedDays.includes(index+1) ? (
                                                        <Image source={require("../../assets/check-mark.png")}>
                                                </Image>
                                                    ) : (
                                                        <Image source={require("../../assets/uncheck_green.png")}>
                                                </Image>
                                                    )
                                                ) : (
                                                    <Image source={require("../../assets/uncheck_green.png")}>
                                                    </Image>
                                                )
                                               
                                            }
                                    </TouchableOpacity>
                            </View>
                        )
                    })

                }
                </View>
                <View style={{ margin: 10,
                    width:'100%'
                        }}>
                            <View style={{ flexDirection: 'row', justifyContent:'space-between',
                                         marginHorizontal:20}}>
                                <Text style={{width: (DEVICE_WIDTH/2)-50,
                                }}>Start Time</Text>                                  
                                <Text style={{
                                    textAlign:'left',
                                    width: (DEVICE_WIDTH/2)-50
                                }}>End Time</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent:'space-between',
                             marginVertical:3,
                             marginHorizontal:20}}>
                                    <DatePicker
                                    style={{width: (DEVICE_WIDTH/2)-30 }}
                                    date={this.state.startTime}
                                    mode="time"
                                    placeholder="00:00 AM"
                                    format="hh:mm A"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    showIcon={false}
                                    onDateChange={(date) => {this.setState({startTime: date})}}
                                    /> 

                                    <DatePicker
                                        style={{width: (DEVICE_WIDTH/2)-50}}
                                        date={this.state.endTime}
                                        mode="time"
                                        placeholder="00:00 AM"
                                        format="hh:mm A"
                                        minDate={this.state.startTime}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        showIcon={false}
                                        onDateChange={(date) => {this.setState({endTime: date})}}
                                    />                       
                            </View>
                            
                    </View>

               
                <BottomBorderView 
                horizontal={40}
                width={DEVICE_WIDTH - 40}
                top={10}
                />
            </View>
        )
    }

    renderPersonalDetailUI(){

        return(
            <View style={{
                marginHorizontal:10,
                marginTop:10,
                width:DEVICE_WIDTH - 20,
                backgroundColor:'white',
                borderRadius:5,
                alignItems:'center',
                borderRadius:5,
                alignItems:'center',
                borderColor:'lightgray', 
                borderWidth:0.5,elevation: 5,
                shadowColor: '#999666', 
                shadowOffset: {width: 5, height: 5},
                shadowOpacity: 0.7, shadowRadius: 5,
            }}>

            <EditProfileTextField
                title={'Name'}
                value={this.state.name}
                image ={require('../../assets/name.png')}
                onChangeText={(value)=>this.setState({name:value})}
            />
            <EditProfileTextField
                title={'Speciality'}
                value={this.state.speciality}
                image ={require('../../assets/speciality.png')}
                onChangeText={(value)=>this.setState({speciality:value})}
            />
            <EditProfileTextField
                title={'Location'}
                value={this.state.location}
                image ={require('../../assets/big-map.png')}
                onChangeText={(value)=>this.setState({location:value})}
            />
            <EditProfileTextField
                title={'Experience'}
                value={this.state.experience}
                image ={require('../../assets/experience.png')}
                keyboard='decimal-pad'
                onChangeText={(value)=>this.setState({experience:value})}
            />
            <EditProfileTextField
                title={'Website'}
                value={this.state.website}
                image ={require('../../assets/website.png')}
                onChangeText={(value)=>this.setState({website:value})}
            />
            </View>   
        )
    }

    render(){
        const navigate = this.props.navigation.navigate;

        return(
            <View style={{flex:1, backgroundColor:'lightgray'}}>
                {this.state.loader ? <MyActivityIndicator /> : null}

                <Header title={'Edit Profile'} withBackButton=
          {() => this.backButtonAction()} type={'doctor'} 
              navigation={navigate}
              SAVE={() => this.saveProfileAction()}/>
                <BottomBorderView 
                height={1}
                bkgColor='lightgray'
                width='100%'
                top={0}
                />
                    <ScrollView 
                        style={{ flex: 1, backgroundColor:'white'}} 
                        ref={ref => this.scrollView = ref}
                        >
 
                            <ActionSheet
                                    ref={o => this.ActionSheet = o}
                                    title={'Choose option for image'}
                                    options={['Camera', 'Gallery', 'Cancel']}
                                    cancelButtonIndex={2}
                                    destructiveButtonIndex={2}
                                    onPress={(index) => { this.takeImageAction(index)}}
                                />
                            <View>
                                <View style={{
                                    width:'100%',
                                    height:150,
                                    backgroundColor:'#F64351',
                                    borderBottomLeftRadius:20,
                                    borderBottomRightRadius:20
                                }}>
                                </View>
                                {
                                    this.renderOfficeHoursUI()
                                }

                                <Text style={{
                                    marginLeft:20,
                                    marginTop:25,
                                    width:'100%',
                                    textAlign:'left'
                                }}>
                                    Personal Details
                                </Text>
                            {
                                this.renderPersonalDetailUI()
                            }
                    
                            </View>               
                     </ScrollView>
            </View>
        )
    }
}
