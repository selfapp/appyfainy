
import React, {Component} from 'react';
import {View, Text, Dimensions} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import ChatEngineProvider from "./lib/pubnub-chatengine";
import MyActivityIndicator from './Components/activity_indicator';
import { StackActions, NavigationActions } from 'react-navigation';

const DEVICE_HEIGHT = Dimensions.get("window").height;
const doctorRef = firebase.firestore().collection('Doctors');

export default class Splash extends Component{
    constructor(props){
        super(props);
        this.state = {
            loader:false
        }
    }

    componentWillMount(){
        var _this = this;
        firebase.database().goOnline()
        firebase.firestore().enableNetwork()
        var unsubscribe =   firebase.auth().onAuthStateChanged( async (user) => {
            _this.setState({loader:true})
           
            if (user) {

                let doc = firebase.firestore().collection('Users').doc(user.uid);

                doc.get().then(docSnapshot => {
                   let dataUser = docSnapshot.data();
            console.log("data user splash")
            console.log(dataUser)
                   if (dataUser != null && dataUser.phone != null) {

                        doctorRef.where('phone', '==', parseInt(dataUser.phone)).get()
                          .then(snapshot => {
                        _this.setState({loader:false})
                        if (snapshot.empty) {
                            AsyncStorage.setItem('patientid',user.uid)

                            var patDetailKey = [{'name':dataUser.name, 
                            'phonenum': dataUser.phone.toString(), 'image': dataUser.image}]
                            AsyncStorage.setItem('patientdetail',JSON.stringify(patDetailKey))
                            AsyncStorage.setItem('patientphonenum',dataUser.phone.toString())

                            if(dataUser.subscription){
                                _this.findDoctorDetails(dataUser.phone)
                            }else{
                                const keys = Object.keys(dataUser)
                                if(keys.includes('subscription_id')) {
                                    if(dataUser.subscription_id.length > 0){
                                        _this.findDoctorDetails(dataUser.phone)
                                    }else{
                                        const resetAction = StackActions.reset({
                                            index: 0,
                                            actions: [NavigationActions.navigate({ routeName: 'loginNavigationOption' })],
                                        });
                                        _this.props.navigation.dispatch(resetAction);
                                    }
                                }
                                else{
                                    console.log("Splash tos called ....")
                                    const resetAction = StackActions.reset({
                                        index: 0,
                                        actions: [NavigationActions.navigate({ routeName: 'TOS' })],
                                    });
                                    _this.props.navigation.dispatch(resetAction);
                                    }
                                }
                          }  else{
                                ChatEngineProvider.connect(
                                    dataUser.phone,
                                    dataUser.name
                                )
                                const resetAction = StackActions.reset({
                                    index: 0,
                                    actions: [NavigationActions.navigate({ routeName: 'TabNavigator' })],
                                });
                                _this.props.navigation.dispatch(resetAction);
                          }
                        })

                }else{
                    console.log("enter into doctor ////")
                    console.log(user)
                    _this.setState({loader:false})
                    AsyncStorage.getItem('phonenumber').then((phoneno)=>{
                        if(phoneno){
                            ChatEngineProvider.connect(
                                phoneno,
                                phoneno
                            )
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate({ routeName: 'TabNavigator' })],
                              });
                              _this.props.navigation.dispatch(resetAction);
                        }else{
                            // AsyncStorage.getItem('useruid').then((useruid)=>{
                            //     if(!useruid){
                                    console.log("doctor else login enter")
                                    const resetAction = StackActions.reset({
                                        index: 0,
                                        actions: [NavigationActions.navigate({ routeName: 'loginNavigationOption' })],
                                        });
                                    _this.props.navigation.dispatch(resetAction);
                            //     }

                            // })
                            
                        }
                    })
                }
                }, err => {
                console.log(`Encountered error: ${err}`);
                });
            } else {
                this.setState({loader:false})
                AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'loginNavigationOption' })],
                  });
                  this.props.navigation.dispatch(resetAction);
            }
        });
        // unsubscribe()
    }

    findDoctorDetails(phoneNum){

            var _this = this;    
             _this.setState({loader:true})
    
             let doc = firebase.firestore().collection('Chatlist').doc(phoneNum.toString());

            doc.get().then(docSnapshot => {
            console.log(`Received doc snapshot splash: `);
            console.log(docSnapshot)
           _this.setState({ loader:false})
            if(docSnapshot.data() === undefined){
               alert("Sorry! you can not register at this time. Because no doctor has sent you invitation.")
                    AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove);
                    _this.setState({loader:false})
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'loginNavigationOption' })],
                        });
                        _this.props.navigation.dispatch(resetAction);
            }else{
                if(docSnapshot.data().channels.length > 0){
                      _this.setValues(docSnapshot.data().channels, phoneNum)
                }
            }
            }, err => {
            console.log(`Encountered error: ${err}`);
            });    
    }

    setValues(data, phoneNum){
       
        var _this = this;
        var arrDoc = []
         data.map((item, index)=>{
             if(item.type === "patient"){
                arrDoc.push(item)
             }
             if(index === data.length - 1){
               
                 if(arrDoc.length > 0){
                     var docDetail = arrDoc[0]; 
                     var newKey = docDetail.channelname.replace(('_'),'')
                     var newKey1 = newKey.replace((phoneNum),'')
                     doctorRef.where('phone', '==', parseInt(newKey1)).get()
                        .then(snapshot => {
                           
                            if (snapshot.empty) {
                                console.log('No matching documents.');                                                  
                              }  else{
                                snapshot.forEach(doc => {
                                    var docDetailKey = [{'name':doc.data().name, 
                                    'phonenum': doc.data().phone, 'image': doc.data().image}]
                                    AsyncStorage.setItem('docdetail',JSON.stringify(docDetailKey))
                                    ChatEngineProvider.connect(
                                            phoneNum,
                                            phoneNum
                                        )
                                    const resetAction = StackActions.reset({
                                        index: 0,
                                        actions: [NavigationActions.navigate({ routeName: 'TabNavigatorPatinet' })],
                                        });
                                        _this.props.navigation.dispatch(resetAction);
                                    });
                              }
                    })
                 }
             }
         })
    }

    render(){
        return(
            <View style={{ flex: 1, 
                            backgroundColor:'white',
                            }}>
                 <Text style={{textAlign:'center', marginTop:DEVICE_HEIGHT/2+20,
                fontSize:16, fontWeight:'bold',
                width:'100%'}}>
                     Please wait ...
                </Text>
                {this.state.loader ? <MyActivityIndicator /> : null}
            </View>
        )
    }
}