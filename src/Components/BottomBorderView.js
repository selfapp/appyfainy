

import React from 'react';
import {View} from 'react-native';

export default BottomBorderView = (props) => {

    return(
        <View style={{

            marginHorizontal:props.horizontal,
            backgroundColor:props.bkgColor ? props.bkgColor : '#ECECEC',
            height:props.height ? props.height : 2,
            marginTop:props.top,
            width:props.width,
            marginBottom:props.bottom
        }}>
        </View>
    )
}