
import React from 'react';
import {TextInput, View, Image} from 'react-native';

export default TextField = (props) => {

    return(

        <View style={{
            width:props.width,
            flexDirection:'row',
            height:30,
            marginTop:15,
            alignItems:'center'
        }}>
            <Image
                style={{ height: 14, width: 16, marginRight:10}}
                source={props.image} resizeMode='contain'
                        />
                <TextInput style={{
                    fontSize:props.size,
                    fontWeight:props.weight,
                    height:props.height,
                    width:props.textWidth,
                    // backgroundColor:'red'
                }}  placeholder={props.placeholderText}
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    maxLength={props.length}
                    value={props.value}
                    keyboardType={props.keyboard}
                    onChangeText={props.onChangeText}
                >

                </TextInput>
        </View>
    )
}