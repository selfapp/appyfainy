
import React from 'react';
import {
    StyleSheet,
    Text, Keyboard,
    View,
    Image, TouchableOpacity,
    Platform,
    Dimensions
} from 'react-native';

const dim = Dimensions.get('window');
const isIphoneX = dim.height >= 812 ? true : false
const headerHeight = Platform.OS === 'ios' && isIphoneX ? 90 : 70

export default Header = (props) => {
    
    function renderLeftIcon() {
        if (props.withBackButton) {
            
            return (
                <TouchableOpacity style={styles.iconLeftStyle}
                                  onPress= 
                                    {props.withBackButton}
                                  >
                    <Image source={require('../assets/arrow_white.png')}/>
                </TouchableOpacity>
            )
        }
    }

    function renderRightIcon() {
      
        if (props.SAVE) {
            return (
               <TouchableOpacity style={styles.iconRightStyle} onPress={props.SAVE}>
                    <Text style={styles.title}>Save</Text>
                </TouchableOpacity>
            )
        } else if (props.ADD) {
            return (
                <TouchableOpacity style={[styles.iconRightStyle, {width: 85}]} onPress={props.ADD}>
                    <Image source={require('../assets/conversation.png')}/>
                </TouchableOpacity>
            )
        }else if (props.ADDDOCTOR) {
            return (
                <TouchableOpacity style={[styles.iconRightStyle, {width: 60}]} disabled={props.block}
                      onPress={props.ADDDOCTOR}>
                    <Image source={require('../assets/Groupnew.png')}/>
                </TouchableOpacity>
            )
        } else if (props.leave) {
            return (
                <TouchableOpacity style={styles.iconRightStyle} disabled={props.block} onPress={props.leave}>
                    <Text style={[styles.title, {fontSize:14}]}>Leave</Text>
                </TouchableOpacity>
            )
        } else if (props.EDIT) {
            return (
                    props.type === 'doctor' ? (
                        <TouchableOpacity style={styles.iconRightStyle} onPress={props.EDIT}>
                            <Image source={require('../assets/edit.png')}>
                            </Image>
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity style={styles.iconRightStyle} onPress={props.EDIT}>
                            <Image source={require('../assets/patient_edit.png')}>
                            </Image>
                        </TouchableOpacity>
                    )               
            )
        }
    }

    var headerBckgColor = ''
    if(props.type === 'doctor'){
        headerBckgColor = '#F64351'
    }
    else{
        headerBckgColor = '#1D808D'
    }

    return (
         
       
        <View style={[styles.header, {backgroundColor:headerBckgColor}]}>

            {renderLeftIcon()}
            <Text numberOfLines={1} style={styles.title}>
                {props.title}
            </Text>
            {renderRightIcon()}
        </View>


    )
};

const styles = StyleSheet.create({
    header: {
        // backgroundColor: '#2F97DE',
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: headerHeight
    },
    title: {
        height: 30,
        fontSize: 15,
        color: 'white',
        fontWeight:'bold',
        width:dim.width - 110,
        // marginTop:25,
        // backgroundColor:'yellow',
        textAlign:'center',
    },
    iconRightStyle: {
        position: 'absolute', right: 10, 
        width: 50, height: 40, alignItems: 'center', justifyContent: 'center'
    },
    iconLeftStyle: {
        position: 'absolute', left: 0, bottom: 5, zIndex: 2,
        width: 50, height: 40, alignItems: 'center', justifyContent: 'center'
    },

});