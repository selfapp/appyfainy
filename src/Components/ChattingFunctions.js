import React from 'react';

export default class ChattingFunctions {

    
    static fetchLastMessageForChannels(channels, phoneNumber, callback){

            var arrMess = []
            var arrMessNum = []
            var arrTime = []
            var index = 0
           
            const keys = Object.keys(channels)
            for (var key in channels) {
    
                index = index + 1
                var newKey1 = ''
                if(key.includes('group')){
                    newKey1 = key
                }else{
                    var newKey = key.replace(('_'),'')
                    newKey1 = newKey.replace((phoneNumber),'')
                }
           
            if(channels[key][0].message.text){
                var message = channels[key][0].message.text[0].text
                var time = channels[key][0].message.text[0].createdAt
                arrMess.push(message)
                arrMessNum.push(newKey1)
                arrTime.push(time)
            }          
            if(index === keys.length){
                callback(arrMess, arrTime,arrMessNum)
            }
        }
    }

    static mappingMessage(arrMess, arrTime, arrMessageNo, chatList, callback) {

        const newMessArr = [...arrMess]
        
        const newTimeArr = [...arrTime]
        chatList.map((item, index)=>{
           var newIndex = ''
           if(item.type === 'group'){
            newIndex = arrMessageNo.indexOf(item.channelname)
           }else{
            newIndex = arrMessageNo.indexOf(item.phone.toString())
           }
            newMessArr[index] = arrMess[newIndex]
            newTimeArr[index] = arrTime[newIndex]
        })
        callback(newMessArr, newTimeArr)
    }

    static createChannelName(userOne, UserTwo, callback) {

        var combinedChannelName = ''
        if(userOne > UserTwo){
        combinedChannelName = userOne + "_" + UserTwo
        }else{
        combinedChannelName = UserTwo + "_" + userOne
        }

        callback(combinedChannelName)
    } 
}