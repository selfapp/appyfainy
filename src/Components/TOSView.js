
import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

export default TOSView = (props) => {

    return(

        <View style={{
            marginHorizontal:'5%',
            marginTop:30,
        }}>

            <TouchableOpacity onPress={props.action}>
                <Text style={{
                    textDecorationLine:'underline',
                    fontSize:18,
                    color:'black',
                }}>{props.title}
                </Text>
            </TouchableOpacity>
            <Text style={{
                fontSize:14,
                color:'gray',
                marginTop:10
            }}>{props.detailText}
            </Text>
                
        </View>
    )
}