

import React from 'react';
import {TouchableOpacity, Image} from 'react-native';

export default BackButton = (props) => {

    return(
        <TouchableOpacity style={{
           marginLeft:20,
           width:70,
           height:40,
           justifyContent:'center',
        }} onPress={props.buttonAction}>
             <Image
                style={{ height: 20, width: 28}}
                source={props.image}//{require('../assets/arrow.png')} 
                resizeMode='contain'
                        />
        </TouchableOpacity>
    )
}