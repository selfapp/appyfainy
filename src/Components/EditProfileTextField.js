
import React from 'react';
import {TextInput, View, Image, Dimensions, StyleSheet, Text} from 'react-native';
import BottomBorderView from '../Components/BottomBorderView';

const DEVICE_WIDTH = Dimensions.get("window").width;


export default EditProfileTextField = (props) => {

    return(
        <View>
        <View style={styles.boxView}>
          <Image style={{
              marginLeft:10
          }} source={props.image}>
          </Image>
          <Text style={{
              fontSize:14,
              marginLeft:10
          }}>
             {props.title}
          </Text>
          </View>
          <TextInput style={{marginTop:10, fontSize:12,
                  color:'gray',marginLeft:20}}
                  autoCorrect={false}
                  underlineColorAndroid='transparent'
                  value={props.value}
                  keyboardType={props.keyboard}
                  onChangeText={props.onChangeText}
                  >
          </TextInput>
          <BottomBorderView
          horizontal={20}
          width={DEVICE_WIDTH-40}
          top={10}
          />
      </View>
    )
}

const styles = StyleSheet.create({

    boxView:{ marginHorizontal:10,
        // height:50,
        width:DEVICE_WIDTH - 40,
        alignItems:'center',
        marginTop:25,
        flexDirection:'row',}
       
})