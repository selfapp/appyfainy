

import React from 'react';
import {View, Image, Dimensions, StyleSheet, Text, TouchableOpacity} from 'react-native';
import BottomBorderView from '../Components/BottomBorderView';

const DEVICE_WIDTH = Dimensions.get("window").width;


export default ProfileTextField = (props) => {

    return(
        <View>
        <View style={styles.boxView}>
          <Image style={{
              marginLeft:10
          }} source={props.image}>
          </Image>
          <Text style={{
              fontSize:12,
              marginLeft:5
          }}>
             {props.title}
          </Text>
          </View>
          {
              props.makeTouchable ? (
                  <TouchableOpacity onPress={props.buttonAction}>
                        <Text style={{marginTop:5, fontSize:10,
                            color:props.color ? props.color : 'gray',
                            marginLeft:35,
                            textDecorationLine:props.underline ? props.underline : 'none'
                            }}>
                                
                            {props.value}
                        </Text>
                </TouchableOpacity>
              ) : (
                <Text style={{marginTop:5, fontSize:10,
                    color:props.color ? props.color : 'gray',
                    marginLeft:35,
                    textDecorationLine:props.underline ? props.underline : 'none'
                    }}>
                        
                    {props.value}
                </Text>
              )
          }
         
          <BottomBorderView
          horizontal={20}
          width={DEVICE_WIDTH-40}
          top={10}
          />
      </View>
    )
}

const styles = StyleSheet.create({

    boxView:{ marginHorizontal:10,
        width:DEVICE_WIDTH - 40,
        alignItems:'center',
        marginTop:25,
        flexDirection:'row'}
})