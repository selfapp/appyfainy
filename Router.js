
import React from 'react';
import { Image } from 'react-native';

import {createAppContainer, createStackNavigator, createBottomTabNavigator} from 'react-navigation';

import Splash from './src/Splash';
import Login from './src/Screens/login/Login';
import Otp from './src/Screens/login/Otp';
import Register from './src/Screens/login/Register';
import TOS from './src/Screens/login/TOS';

import Subscription from './src/Screens/login/Subscription';
import PaymentDetail from './src/Screens/login/PaymentDetail';
import ChatScreen from './src/Screens/doctor/ChatScreen';

import ChatPage from './src/Screens/doctor/ChatPage';

import ProfileDoctor from './src/Screens/doctor/ProfileDoctor';
import EditProfileDoctor from './src/Screens/doctor/EditProfileDoctor';

import SettingDoctor from './src/Screens/doctor/SettingDoctor';
import TransactionHistoryDoctor from './src/Screens/doctor/TransactionHistoryDoctor';

import ChatScreenPatient from './src/Screens/patient/ChatScreenPatient';
import ProfilePatient from './src/Screens/patient/ProfilePatient';
import EditProfilePatient from './src/Screens/patient/EditProfilePatient';
import PatientSetting from './src/Screens/patient/PatientSetting';
import TransactionHistory from './src/Screens/patient/TransactionHistory';
import PaymentSetting from './src/Screens/patient/PaymentSetting';

export const loginNavigationOption = createStackNavigator({

    Login:{
        screen:Login
    },
    Otp:{
        screen:Otp
    },Register:{
        screen:Register
    },TOS:{
        screen:TOS
    },
    Subscription:{
        screen:Subscription
    },
    PaymentDetail:{
        screen:PaymentDetail
    }
},{headerMode:'none'})

export const DoctorChatScreen = createStackNavigator({

    ChatScreen:{
        screen:ChatScreen
    },
    ChatPage:{
        screen:ChatPage
    },
     ProfileDoctor:{
        screen:ProfileDoctor
    },ProfilePatient:{
        screen:ProfilePatient
    }
},{headerMode:'none'})

export const DoctorProfile = createStackNavigator({

    ProfileDoctor:{
        screen:ProfileDoctor
    },
    EditProfileDoctor:{
        screen:EditProfileDoctor
    }
},{headerMode:'none'})

export const PatientProfile = createStackNavigator({

    ProfilePatient:{
        screen:ProfilePatient
    },
    EditProfilePatient:{
        screen:EditProfilePatient
    }
},{headerMode:'none'})

export const PatientSettings = createStackNavigator({

    PatientSetting:{
        screen:PatientSetting
    },
    TransactionHistory:{
        screen:TransactionHistory
    },
    PaymentSetting:{
        screen:PaymentSetting
    }
},{headerMode:'none'})

export const DoctorSettings = createStackNavigator({

    SettingDoctor:{
        screen:SettingDoctor
    },
    TransactionHistoryDoctor:{
        screen:TransactionHistoryDoctor
    }
},{headerMode:'none'})

export const PatientChatScreen = createStackNavigator({

    ChatScreenPatient:{
        screen:ChatScreenPatient
    },
    ChatPage:{
        screen:ChatPage
    },
    ProfileDoctor:{
       screen:ProfileDoctor
   },ProfilePatient:{
       screen:ProfilePatient
   }
},{headerMode:'none'})

export const TabNavigator = createBottomTabNavigator(
    {
        CHAT:{
            screen: DoctorChatScreen,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/message.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        PROFILE:{
            screen:DoctorProfile,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/user-tab.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        SETTING:{
            screen: DoctorSettings,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/setting-anticon.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
    },
    {
        initialRouteName: 'CHAT',        
        tabBarOptions: {
            activeTintColor: "#44A7AF",
            showLabel: false,
            inactiveTintColor: "gray",
            tabBarPosition: 'bottom',
            iconStyle:{
                width: 30,
                height: 30
            },
            lazy: true,
            indicatorStyle: '#fff',
        }
    }
);

export const TabNavigatorPatinet = createBottomTabNavigator(
    {
        CHAT:{
            screen: PatientChatScreen,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/message.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        PROFILE:{
            screen:PatientProfile,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/user-tab.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
        SETTING:{
            screen: PatientSettings,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('./src/assets/setting-anticon.png')}
                        style = {{tintColor:tintColor}}
                        />
                    )
                }
            }
        },
    },
    {
        initialRouteName: 'CHAT',        
        tabBarOptions: {
            activeTintColor: "#44A7AF",
            showLabel: false,
            inactiveTintColor: "gray",
            tabBarPosition: 'bottom',
            iconStyle:{
                width: 30,
                height: 30
            },
            lazy: true,
            indicatorStyle: '#fff',
        }
    }
);

export const appNavigationOptions = createStackNavigator({
   
    Splash:{
        screen:Splash
    },
    loginNavigationOption:{
        screen:loginNavigationOption
    },
    TabNavigator: {
        screen: TabNavigator
    },
    TabNavigatorPatinet: {
        screen: TabNavigatorPatinet
    },TOS:{
        screen:TOS
    }, Register:{
        screen:Register
    }, Subscription:{
        screen:Subscription
    }, PaymentDetail:{
        screen:PaymentDetail
    }
}, {headerMode:'none'})

export const AppContainer = createAppContainer(appNavigationOptions);
