/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {AppContainer} from './Router'
import {SafeAreaView} from 'react-native';
// import {StripeProvider} from 'react-stripe-elements';

export default class App extends Component {

  constructor(){

    super();
    console.disableYellowBox = true;
  }
  render() {
    return (
      // <StripeProvider apiKey="pk_test_yBnScPQDCl7LTzeRefixXP1g00Jfao1Jsl">
        // <SafeAreaView style={{flex:1}}>
          <AppContainer/>
        // </SafeAreaView>
      // </StripeProvider>
    );
  }
}


